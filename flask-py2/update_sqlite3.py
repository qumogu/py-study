# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: update_sqlite3.py
@datetime: 2020/7/1 18:54
"""

import sqlite3

if __name__ == "__main__":
    conn = sqlite3.connect("db/bruce.db")
    c = conn.cursor()
    sql = "update bs_uses set role='admin' where username='admin'"
    print(sql)
    res = c.execute(sql)
    print(res)
    conn.commit()
    conn.close()