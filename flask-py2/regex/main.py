# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: main.py
@datetime: 2020/6/23 12:55
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

from flask import Flask
from werkzeug.routing import BaseConverter

class RegexConverter(BaseConverter):
    def __init__(self, url_map, regex):
        # 调用父类初始化函数
        super(RegexConverter, self).__init__(url_map)
        # 保存正则表达式规则
        self.regex = regex

app = Flask(__name__)

app.url_map.converters["re"] = RegexConverter


@app.route("/")
@app.route("/index")
def index():
    return "ok"


#@app.route("/test02/<re('[a-zA-Z0-9_-]+[\.][a-z]{3,4}'):filename>")
@app.route("/test02/<re('[a-zA-Z0-9_-]+[\.](jpg|gif|png|jpeg)'):filename>")
def test(filename):
    if filename:
        return filename
    else:
        return u"无法获取文件名"

@app.route("/regex/<link_str>")
def get_str(link_str):
    return "str类型参数：%s" % link_str

@app.route("/regex/<int:num>")
def get_int(num):
    return "整形参数：%s" % num

@app.route("/regex/<path:link_path>")
def get_path(link_path):
    return "path类型参数：%s" % link_path

@app.route("/regex/<float:link_float>")
def get_float(link_float):
    return "浮点类型参数：%f" % link_float

@app.route("/sendsms/<re(r'1[345678]\d{9}'):phone>")
def sendsms(phone):
    return "给手机号：%s，发送短信" % phone

if __name__ == "__main__":
    app.run(debug=True)