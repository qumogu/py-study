# -*- coding: utf-8 -*-
"""
@author: Bruce
@contact: lanmin@itssoft.net
@file: main.py
@datetime: 2021/1/12 9:39
"""
from __future__ import unicode_literals

from flask import Flask, request, abort

app = Flask(__name__)

@app.route("/report/index")
def report_index():
    print("hello mgw")
    # username = request.cookies.get("mgw-username")
    username = request.headers.get("x-mgw-username")

    if username:
        return "hello , " + username
    else:
        return abort(401)

if __name__ == '__main__':
    app.run("0.0.0.0", 2125, True)