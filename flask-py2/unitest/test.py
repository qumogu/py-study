# -*- coding: utf-8 -*- 
"""
@author: Bruce
@contact: lanmin@itssoft.net
@file: test02.py
@datetime: 2020/11/20 14:28
"""
from __future__ import unicode_literals
from unittest import TestCase,main
from app import app
class FirstTest(TestCase):

    def setUp(self):
        print("测试开始")
        self.app = app
        self.client = self.app.test_client()

    def tearDown(self):
        print("测试结束")

    def test_first(self):

        resp = self.client.get('/')
        print(resp.text)
        self.assertEqual(resp,'index')

if __name__ == '__main__':
    main()
