# -*- coding: utf-8 -*- 
"""
@author: Bruce
@contact: lanmin@itssoft.net
@file: app.py.py
@datetime: 2020/11/20 14:23
"""
from __future__ import unicode_literals
from flask import Flask

app = Flask(__name__)

app.config["SECRET_KEY"] = "SDFASFD"

@app.route("/")
def test():

    return "index"

@app.route("/user/<name>")
def user(name):

    user = "你的名字：{}".format(name)

    return user

if __name__ == '__main__':
    app.run("0.0.0.0", 6123, debug=True)