# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: moban.py
@datetime: 2020/6/23 12:59
"""
from __future__ import unicode_literals
import sys
from redis.sentinel import Sentinel

from flask_session import Session
from flask import request, session

class Config(object):

    SECRET_KEY = "S9cK*LyDJ8@8Az"
    SESSION_TYPE = "redis"

    sentinel_list = [
        ("192.168.0.243", "26379"),
    ]

    mySentinel = Sentinel(sentinel_list)

    SESSION_REDIS = mySentinel.master_for('mymaster', db=9, password="Nash1234@")


from flask import Flask, request

app = Flask(__name__)
app.config.from_object(Config())
Session(app)


@app.route("/")
@app.route("/index")
def index():
    session["user"]= "test02"

    return "ok"

@app.route("/test02")
def test():
    msg = session.get("user","error")

    return msg





if __name__ == "__main__":
    app.run(debug=True,host="0.0.0.0", port=5001)