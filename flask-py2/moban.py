# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: moban.py
@datetime: 2020/6/23 12:59
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')


from flask import Flask, request

app = Flask(__name__)



@app.route("/")
@app.route("/index")
def index():

    return "ok"


if __name__ == "__main__":
    app.run(debug=True)