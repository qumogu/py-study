from flask import Flask
import os

app= Flask(__name__)

@app.route("/")
def index():
    print(os.environ.get("HOST"))
    return "ok"

if __name__ == '__main__':
    app.run("0.0.0.0", port=8000, debug=True)