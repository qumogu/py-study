# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: flask_upload.py
@datetime: 2020/6/29 14:42
"""
import sys

from flask import Flask, request, send_file, redirect, url_for, render_template, session, flash, jsonify
from werkzeug.routing import BaseConverter
from werkzeug.utils import secure_filename
from werkzeug.security import generate_password_hash, check_password_hash
from functools import wraps
import os
import datetime
from bson import ObjectId
import sqlite3
from PIL import Image


class Config(object):

    IMAGE_TPYE = ["jpg","JPG","png","PNG", "gif", "GIF", "JPEG", "jpeg"]
    IMG_UPLOAD_DIR = "uploads/imgs"
    IMG_THUMB_DIR = "static/thumb"
    IMG_NOPIC = "static/imgs/nopic.jpg"
    SQLITE_DB = "db"
    SQLITE_FILE = os.path.join(SQLITE_DB, "bruce.db")
    SECRET_KEY = "S9cK*LyDJ8@8Az"
    WEB_DOMAIN = "http://wimg.qumogu.com"


def init_dir():
    path_list = Config.IMG_UPLOAD_DIR.split("/")
    path = ""
    for i in path_list:
        path = os.path.join(path, i)
        print(path)
        if not os.path.exists(path):
            os.mkdir(path)
            print("创建目录{}成功".format(path))
    path_list = Config.SQLITE_DB.split("/")
    path = ""
    for i in path_list:
        path = os.path.join(path, i)
        print(path)
        if not os.path.exists(path):
            os.mkdir(path)
            print("创建目录{}成功".format(path))

def init_db():
    conn = sqlite3.connect(Config.SQLITE_FILE)
    c = conn.cursor()
    c.execute('''CREATE TABLE bs_users
           (id INTEGER PRIMARY KEY  AUTOINCREMENT ,
           username  CHAR(50) unique  NOT NULL,
           password  CHAR(100)    NOT NULL,
           role      CHAR(10),
           create_time datetime default (datetime('now', 'localtime')),
           login_ip  CHAR(20),
           last_login datetime default (datetime('now', 'localtime')) 
           );''')
    print("创建数据表 bs_users 成功。")
    conn.commit()
    c.execute('''CREATE TABLE bs_images
               (id INTEGER PRIMARY KEY  AUTOINCREMENT,
               uid int not null,
               filename  CHAR(50)  NOT NULL unique ,
               create_date  CHAR(20)    NOT NULL,
               size  INT  NOT NULL,
               visit_num INT default 1,
               create_time datetime default (datetime('now', 'localtime')),
               update_time datetime default (datetime('now', 'localtime'))
               );''')
    print("创建数据表 bs_images 成功。")
    conn.commit()
    sql = "INSERT INTO bs_users (username,password,role) VALUES ('admin', '{}', 'admin') ".format(
        generate_password_hash("admin123"))
    print("初始化管理员数据成功！")
    c.execute(sql)
    conn.commit()
    conn.close()

def str_now_time():
    return datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

def image_thumb(path, thumb_path):
    im = Image.open(path)
    width = im.size[0]
    height = im.size[1]
    if width >= height:
        new_size = (100, 70)
    else:
        new_size = (70, 100)
    im.thumbnail(new_size, Image.ANTIALIAS)
    im.save(thumb_path)

def check_login(func):
    @wraps(func)
    def decorator(*args, **kwargs):
        uid = session.get("uid")
        if uid:
            return func(*args, **kwargs)
        else:
            return redirect("/")

    return decorator

def check_admin(func):
    @wraps(func)
    def decorator(*args, **kwargs):
        user_type = session.get("utype")
        if user_type != "admin":
            return redirect("/")
        else:
            return func(*args, **kwargs)
    return decorator

class RegexConverter(BaseConverter):

    def __init__(self, url_map, regex):
        super(RegexConverter, self).__init__(url_map)
        self.regex = regex


class ImageConverter(BaseConverter):

    def __init__(self, url_map):
        super(ImageConverter, self).__init__(url_map)
        self.regex = '[a-zA-Z0-9_-]+[\.]({})'.format("|".join(Config.IMAGE_TPYE))


class ImageFile(object):

    def __init__(self, filename=None, id=None):
        self.tb_name = "bs_images"
        self.db_file = Config.SQLITE_FILE
        self.init_file(filename, id)

    def init_file(self, filename, id):
        self.id = None
        self.filename = None
        self.uid = None
        self.date = None
        self.visit_num = None
        if filename or id:
            if filename:
                sql = "select id,filename,uid,create_date,visit_num from {} where filename='{}' ".format(self.tb_name,
                                                                                                         filename)
            else:
                sql = "select id,filename,uid,create_date,visit_num from {} where id='{}' ".format(self.tb_name, id)
            res = self.execute(sql, "fetchone")
            if res:
                self.id = res[0]
                self.filename = res[1]
                self.uid = res[2]
                self.date = res[3]
                self.visit_num = int(res[4])

    def execute(self, sql, type="fetchall"):
        conn = sqlite3.connect(self.db_file)
        c = conn.cursor()
        res = c.execute(sql)
        conn.commit()
        if type == "fetchall":
            res = [i for i in res]
        elif type == "fetchone":
            res = c.fetchone()
        elif type == "insert_one":
            res = c.lastrowid
        else:
            res = conn.total_changes
        conn.close()
        print("{}:类型【{}】，语句：{}".format(str_now_time(), type, sql))
        return res

    def create(self,uid, filename, date, size):
        sql = "insert into {} (uid,filename,create_date,size) values ({},'{}','{}',{})".format(self.tb_name,
                                                                                uid, filename, date, size)
        res = self.execute(sql, "insert_one")
        if res:
            self.id = res
            self.filename = filename
            self.date = date
            self.visit_num = 0
            self.size = size
            return True
        return False

    # def get_user_imgages(self, uid, page=1, limit=20):
    #     if page > 1:
    #         startid = (page - 1) * limit
    #         sql = "select i.id,i.filename,i.create_time,i.visit_num,i.update_time,u.username from bs_images as i " \
    #               " join bs_users as u on i.uid= u.id where i.uid={} order by i.id desc limit {},{}".format(uid, startid, limit)
    #     else:
    #         sql = "select i.id,i.filename,i.create_time,i.visit_num,i.update_time,u.username from bs_images as i " \
    #               " join bs_users as u on i.uid= u.id where i.uid={} order by i.id desc limit {}".format(uid, limit)
    #     data = self.execute(sql)
    #     return data

    def get_user_imgages(self, uid, page=1, limit=20, date=None):
        startid = (page - 1) * limit
        if date:
            sql = "select i.id,i.filename,i.create_time,i.visit_num,i.update_time,u.username from bs_images as i " \
                  " join bs_users as u on i.uid= u.id where i.uid={} and i.create_date = '{}' order by i.id " \
                  "desc limit {},{}".format(uid, date, startid, limit)
        else:
            sql = "select i.id,i.filename,i.create_time,i.visit_num,i.update_time,u.username from bs_images as i " \
                  " join bs_users as u on i.uid= u.id where i.uid={}  order by i.id " \
                  "desc limit {},{}".format(uid, startid, limit)
        data = self.execute(sql)
        return data

    # def get_all_images(self, page=1, limit=20):
    #     if page > 1:
    #         startid = (page - 1) * limit
    #         sql = "select i.id,i.filename,i.create_time,i.visit_num,i.update_time,u.username " \
    #               " from bs_images as i left join bs_users as u on i.uid= u.id order by i.id desc limit {},{}".format(startid, limit)
    #     else:
    #         sql = "select i.id,i.filename,i.create_time,i.visit_num,i.update_time,u.username " \
    #               " from bs_images as i left join bs_users as u on i.uid= u.id order by i.id desc limit {}".format(limit)
    #     data = self.execute(sql)
    #     return data

    def get_all_images(self, page=1, limit=20, date=None):
        startid = (page - 1) * limit
        if date:
            sql = "select i.id,i.filename,i.create_time,i.visit_num,i.update_time,u.username " \
                  " from bs_images as i left join bs_users as u on i.uid= u.id where i.create_date = '{}' order " \
                  "by i.id desc limit {},{}".format(date, startid, limit)
        else:
            sql = "select i.id,i.filename,i.create_time,i.visit_num,i.update_time,u.username " \
                  " from bs_images as i left join bs_users as u on i.uid= u.id  order " \
                  "by i.id desc limit {},{}".format(startid, limit)
        data = self.execute(sql)
        return data

    def delete(self):
        if self.id:
            sql = "delete from {} where id={}".format(self.tb_name, self.id)
            res = self.execute(sql, "delete")
            if res:
                self.id = None
                self.filename = None
                self.size = 0
                self.visit_num = 0
                return True
        return False

    def del_images(self, ids):
        if not isinstance(ids,list):
            return 0
        if len(ids) > 1:
            ids = [str(i) for i in ids]
            str_ids = ",".join(ids)
            sql = "delete {} where id in ()".format(self.tb_name, str_ids)
        else:
            sql = "delete {} where id = {}".format(self.tb_name, ids[0])
        res = self.execute(sql, "delete")
        return res

    def incr_visit(self):

        if self.visit_num:
            num = self.visit_num + 1
            sql = "update {} set visit_num={}, update_time='{}' where id={}".format(self.tb_name,
                                                                                    num, str_now_time(), self.id)
            self.execute(sql, "update")
            self.visit_num = num



class User(object):

    def __init__(self, username=None, uid=None):
        self.tb_name = "bs_users"
        self.db_file = Config.SQLITE_FILE
        self.username = None
        self.uid = None
        self.role = None
        self.password = None
        self.init_user(username, uid)

    def init_user(self, username, uid):
        if username or uid:
            if username:
                sql = "select id,username,password,role from {} where username = '{}' ".format(self.tb_name, username)
            else:
                sql = "select id,username,password,role from {} where id = {} ".format(self.tb_name, uid)

            res = self.execute(sql, "fetchone")
            if res:
                self.uid = res[0]
                self.username = res[1]
                self.password = res[2]
                self.role = res[3]

    def execute(self, sql, type="fetchall"):
        conn = sqlite3.connect(self.db_file)
        c = conn.cursor()
        res = c.execute(sql)
        conn.commit()
        if type == "fetchall":
            res = [i for i in res]
        elif type == "fetchone":
            res = c.fetchone()
        else:
            res = conn.total_changes
        conn.close()
        print("{}:类型【{}】，语句：{}".format(str_now_time(), type, sql))

        return res

    def delete(self):
        if not self.role:
            return False
        sql = "delete from {} where id ={}".format(self.tb_name, self.uid)
        res = self.execute(sql,"delete")
        self.role = None
        self.password = None
        if res:
            return True
        else:
            return False

    def check_pwd(self, pwd):
        if not self.password:
            return False
        return check_password_hash(self.password, pwd)

    def check_username(self, username):
        sql = "select id from {} where username = '{}' ".format(self.tb_name, username)
        res = self.execute(sql)
        if res:
            return True
        else:
            return False

    def create(self,username, password, role="normal"):
        if self.check_username(username):
            return False
        password = generate_password_hash(password)
        sql = "insert into {} (username,password,role) values('{}','{}','{}') ".format(self.tb_name,
                                                                                          username, password, role)
        res = self.execute(sql, "insert_one")
        if res:
            self.username = username
            self.uid = res
            self.role = role
            self.password = password
            return True
        else:
            return False

    def updete(self, kwargs):
        if not self.role:
            return False
        keys = kwargs.keys()
        set_str = ""
        for i in keys:
            set_str = set_str + "{}='{}' ,".format(i, kwargs.get(i))
        set_str=set_str[:-1]
        sql = "update {} set {} where id ={} ".format(self.tb_name, set_str, self.uid)
        res = self.execute(sql, "update")
        if res:
            return True
        else:
            return False

    def update_pwd(self, old_pwd, new_pwd):
        if self.check_pwd(old_pwd):
            pwd = generate_password_hash(new_pwd)
            kv = {"password":pwd}
            res = self.updete(kv)
            if res:
                return True
        return False

    def get_alluser(self,page=1,limit=50):
        if self.role == "admin":
            sql = "select id,username,role,create_time,login_ip,last_login from {} order by id " \
                  "desc limit {}".format(self.tb_name, limit)
            return self.execute(sql)
        return None


app = Flask(__name__)
app.config.from_object(Config)

app.url_map.converters["re"] = RegexConverter
app.url_map.converters["image"] = ImageConverter


@app.route("/img/<image:filename>")
def get_img(filename):
    if ObjectId.is_valid(filename.split(".")[0]):
        img = ImageFile(filename)
        if img.id:
            path = os.path.join(Config.IMG_UPLOAD_DIR, img.date, filename)
            img.incr_visit()
        return send_file(path, cache_timeout=60 * 60 * 24 * 3)
    url = "/".join([Config.WEB_DOMAIN, Config.IMG_NOPIC])
    return redirect(url)

@app.route("/view_img/<image:filename>")
def view_img(filename):
    img_url = Config.WEB_DOMAIN + url_for("get_img", filename=filename)
    return render_template("view_img.html", img_url=img_url)

@app.route("/upload_img", methods=["GET", "POST"])
@check_login
def upload_img():
    if request.method == "GET":
        return render_template("upload_img.html")
    else:
        img = request.files.get("img_file")
        if img is None:
            return "无文件"
        filename = secure_filename(img.filename)
        if "." not in filename:
            return "不支持该类文件上传"
        img_ext = filename.rsplit(".", 1)[-1]
        if img_ext not in Config.IMAGE_TPYE:
            return "不支持该类图片上传"
        filename = ".".join([str(ObjectId()), img_ext])
        date = str(datetime.datetime.now().date())
        path = os.path.join(Config.IMG_UPLOAD_DIR, date)
        thumb_path = os.path.join(Config.IMG_THUMB_DIR, date)
        if not os.path.exists(path):
            os.mkdir(path)
        if not os.path.exists(thumb_path):
            os.mkdir(thumb_path)
        path = os.path.join(path, filename)
        thumb_path = os.path.join(thumb_path, filename)
        img.save(path)
        image_thumb(path, thumb_path)
        img_obj = ImageFile()
        uid = session.get("uid")
        img_obj.create(uid, filename, date, os.path.getsize(path))
        # url = url_for("view_img", filename=filename)
        # return redirect(url)
        img_url = Config.WEB_DOMAIN+"/img/" + filename
        thumb_url = Config.WEB_DOMAIN+"/"+thumb_path
        return render_template("upload_img.html",img_url= img_url, thumb_url=thumb_url)

@app.route("/upload_imgs", methods=["GET", "POST"])
@check_login
def upload_imgs():
    if request.method == "GET":
        return render_template("upload_imgs.html")
    else:
        img_urls = []
        imgs = request.files.getlist('img_files')
        if imgs is None:
            flash("没有选择任何文件")
            return render_template("upload_imgs.html", imgs=img_urls)
        if len(imgs) > 10:
            flash("单次只允许上传10张图片")
            return render_template("upload_imgs.html", imgs=img_urls)
        for img in imgs:
            filename = secure_filename(img.filename)
            print(filename)
            if "." not in filename:
                flash("{}不符合上传类型,去除上传".format(filename))
                continue
            img_ext = filename.rsplit(".", 1)[-1]
            if img_ext not in Config.IMAGE_TPYE:
                flash("{}不符合上传类型,去除上传".format(filename))
                continue
            filename = ".".join([str(ObjectId()), img_ext])
            date = str(datetime.datetime.now().date())
            path = os.path.join(Config.IMG_UPLOAD_DIR, date)
            thumb_path = os.path.join(Config.IMG_THUMB_DIR, date)
            if not os.path.exists(path):
                os.mkdir(path)
            if not os.path.exists(thumb_path):
                os.mkdir(thumb_path)
            path = os.path.join(path, filename)
            thumb_path = os.path.join(thumb_path, filename)
            img.save(path)
            image_thumb(path, thumb_path)
            img_obj = ImageFile()
            uid = session.get("uid")
            img_obj.create(uid, filename, date, os.path.getsize(path))
            img_urls.append((Config.WEB_DOMAIN+ "/" + thumb_path, Config.WEB_DOMAIN+"/img/" + filename))

        return render_template("upload_imgs.html", imgs= img_urls)

@app.route("/img/del/<int:id>")
@check_login
def delete_image(id):
    if id < 1:
        return "参数错误"
    uid = session.get("uid")
    utype = session.get("utype")
    img_obj = ImageFile(id=id)
    if not img_obj.id:
        return "文件不存在"
    if img_obj.uid == uid or utype =="admin":
        path = os.path.join(Config.IMG_UPLOAD_DIR, img_obj.date, img_obj.filename)
        img_obj.delete()
        if os.path.exists(path):
            os.remove(path)
            return "文件删除成功"
        else:
            return "文件不存在"
    else:
        return "没有权限"

@app.route("/")
@app.route("/index")
def index():
    uid = session.get("uid")
    if uid:
        return redirect("/home")
    else:
        login_url = url_for("login")
        reg_url = url_for("user_reg")
        return render_template("index.html", login_url=login_url, reg_url=reg_url)

@app.route("/login", methods=["GET", "POST"])
def login():
    if request.method == "GET":
        uid = session.get("uid")
        if uid:
            return redirect("/home")
        else:
            return render_template("login.html")
    else:
        username = request.form.get("username")
        password = request.form.get("passwd")
        if not all([username, password]):
            flash("用户名或密码为空，请重新填写")
            return render_template("login.html")
        user = User(username)
        if not user.uid:
            flash("用户名或密码错误，请重新填写")
            return render_template("login.html")
        if not user.check_pwd(password):
            flash("用户名或密码错误，请重新填写")
            return render_template("login.html")
        session["utype"] = user.role
        session["uid"] = user.uid
        session["username"] = user.username
        return redirect("/home")

@app.route("/home")
@check_login
def home():
    utype = session.get("utype")
    uid = session.get("uid")
    username = session.get("username")
    img_obj = ImageFile()
    view_url = Config.WEB_DOMAIN + "/view_img/"
    del_url = Config.WEB_DOMAIN + "/img/del/"
    mypic_url = None
    img_url = Config.WEB_DOMAIN + "/img/"
    thumb_base = "/".join([Config.WEB_DOMAIN, Config.IMG_THUMB_DIR])
    user = None

    user_id = request.args.get("user_id")
    page = request.args.get("page", 1)
    limit_num = request.args.get("limit", 20)
    date_time = request.args.get("date")
    print(date_time)
    if utype == "admin":
        mypic_url = "/home?user_id={}".format(uid)

        if user_id:
            print(user_id)
            user = User(uid=user_id)
            print(user.username)
            if user.uid:
                imgs = img_obj.get_user_imgages(user_id, page, limit_num, date_time)
            else:
                imgs = None
                user = None
        else:
            imgs = img_obj.get_all_images(page, limit_num, date_time)
    else:
        imgs = img_obj.get_user_imgages(uid, page, limit_num, date_time)

    data = {"username":username,
            "user":user,
            "mypic_url":mypic_url,
            "img_url":img_url,
            "thumb_base":thumb_base,
            "view_url":view_url,
            "del_url":del_url,
            "imgs":imgs}

    return render_template("home.html", data=data)

@app.route("/admin_user")
@check_admin
def admin_user():
    utype = session.get("utype")
    if not utype:
        return render_template("login.html")
    if utype == "normal":
        return redirect("/home")
    uid = session.get("uid")
    username = session.get("username")
    user = User(uid=uid)
    view_url = "/home?user_id="
    del_url = "/user/del/"
    data = {"username":username, "view_url":view_url, "del_url":del_url, "users":user.get_alluser()}

    return render_template("admin_user.html", data=data)

@app.route("/user/del/<int:uid>")
@check_admin
def delete_user(uid):
    if uid < 1:
        return "参数错误"

    user = User(uid=uid)
    if not user.uid:
        return "用户不存在"
    if user.delete():
        return "用户删除成功"
    else:
        return "用户删除失败"


@app.route("/user_reg", methods=["GET", "POST"])
def user_reg():
    if request.method == "GET":
        uid = session.get("uid")
        if uid:
            return redirect("/home")
        else:
            return render_template("user_reg.html")
    else:
        username = request.form.get("username")
        password = request.form.get("passwd")
        re_pwd = request.form.get("re_pwd")
        if not all([username, password, re_pwd]):
            flash("用户名或密码为空，请重新填写")
            return render_template("login.html")
        if re_pwd != password:
            flash("两次密码不一致，请重新填写")
            return render_template("login.html")
        user = User(username)
        if user.uid:
            flash("用户名已经注册，请重新填写")
            return render_template("login.html")
        user.create(username, password)
        if not user.uid:
            flash("用户注册失败，请再试一次")
            return render_template("login.html")
        return redirect("/login")

@app.route("/update_pwd", methods=["GET", "POST"])
@check_login
def update_pwd():
    if request.method == "GET":
        return render_template("update_pwd.html")
    else:
        old_pwd = request.form.get("old_pwd")
        new_pwd = request.form.get("new_pwd")
        re_pwd = request.form.get("re_pwd")

        if not all([old_pwd, new_pwd, re_pwd]):
            flash("资料不完整，请重新填写")
            return render_template("update_pwd.html")
        if re_pwd != new_pwd:
            flash("两次密码不一致，请重新填写")
            return render_template("update_pwd.html")
        uid = session.get("uid")
        user = User(uid=uid)
        if not user.uid:
            return redirect("/login")
        if not user.update_pwd(old_pwd, new_pwd):
            flash("原密码不正确，请重新填写")
            return render_template("update_pwd.html")
        else:
            session.clear()
            return redirect("/login")

@app.route("/login_out", methods=["GET", "POST"])
def login_out():
    utype = session.get("utype")
    if utype:
        session.clear()
    return redirect("/")

@app.route("/init_old_imgs")
@check_admin
def init_old_imgs():
    img_obj = ImageFile()
    imgs = img_obj.get_all_images()
    suc_imgs = []
    fial_imgs = []
    for img in imgs:
        path = os.path.join(Config.IMG_UPLOAD_DIR,img[2][:10], img[1])
        thumb_path = os.path.join(Config.IMG_THUMB_DIR, img[2][:10])
        if not os.path.exists(thumb_path):
            os.mkdir(thumb_path)
        thumb_path = os.path.join(thumb_path, img[1])
        try:
            image_thumb(path, thumb_path)
            suc_imgs.append(img[0])
        except Exception as e:
            print("缩略图失败")
            print(e)
            fial_imgs.append(img[0])

    data = {
        "total":len(imgs),
        "success_len":len(suc_imgs),
        "suc_ids":suc_imgs,
        "fial_len":len(fial_imgs),
        "fial_ids":fial_imgs
    }
    return jsonify(data)

@app.route("/test02")
def test():
    return "test02"


if __name__ == "__main__":
    if not os.path.exists(Config.SQLITE_FILE):
        init_dir()
        init_db()
    app.run(debug=True)
