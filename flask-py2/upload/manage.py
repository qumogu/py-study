# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: manage.py
@datetime: 2020/6/23 9:59
"""
from __future__ import unicode_literals
import sys

from qcloud_cos import CosConfig, CosS3Client

import logging
import sys
from bson import ObjectId
import os
from flask import Flask, render_template, request, redirect, make_response
from werkzeug.utils import secure_filename
from StringIO import StringIO
import datetime
import oss2

logging.basicConfig(level=logging.INFO, stream=sys.stdout)

secret_id = "AKIDNRVrxt4PlkRncAtL2arsIJAoOs7LIxHa"
secret_key = "5uOdZNYkhX0CLGB6wiFQLLBk5AMPH7g6"
region = "ap-guangzhou"
bb_bucket = "tuxi-1301633205"
token = None
scheme = "https"
img_base_url = "https://tuxi-1301633205.cos.ap-guangzhou.myqcloud.com"
dir_name = "test02"


image_type = ["jpg","png","gif","bmp","jepg"]

config = CosConfig(Region=region, SecretId=secret_id, SecretKey=secret_key, Token=token, Scheme=scheme)
client = CosS3Client(config)


accesskey_id = os.getenv("OSS_ACCESS_KEY_ID","LTAI4G6ojgSRNEpbZ5oca1YF")
accesskey_secret = os.getenv("OSS_ACCESS_KEY_SECRET", "qBshQqL8jFsCG5jjsXQKZUlPFl2DBy")
bucket_name = os.getenv("OSS_BUCKET", "lanmin")
endpoint = os.getenv("OSS_ENDPOINT", "oss-cn-shenzhen.aliyuncs.com")
oss_domin = "http://img.qumogu.com"

auth = oss2.Auth(accesskey_id,accesskey_secret)
oss_client = oss2.Bucket(auth, endpoint, bucket_name)

app = Flask(__name__, template_folder='templates')
app.config["UPLOAD_FOLDER"] = "up_files"

@app.route("/index")
def index():

    return render_template("upload_img.html")

@app.route("/upload")
def upload_test():
    return "upload_test"

@app.route("/upload_cos", methods=["POST"])
def upload_cos():
    file = request.files["image"]
    file_dir = request.form["dir"]

    print(file_dir)
    filename = secure_filename(file.filename)
    print(filename)
    newname = str(ObjectId())+"_"+filename
    year = datetime.datetime.now().strftime("%Y")
    month =datetime.datetime.now().strftime("%m")
    day = datetime.datetime.now().strftime("%d")
    date_dir =datetime.datetime.now().strftime("%Y%m%d")

    key = "/".join([file_dir, year, month, day, newname])
    print(key)
    #date_dir = date_dir.replace("/", "\\")
    save_file_dir = os.path.join(app.config["UPLOAD_FOLDER"], date_dir)
    print(save_file_dir)
    if not os.path.exists(save_file_dir):
        os.mkdir(save_file_dir)

    path = os.path.join(save_file_dir,newname)
    print(path)

    img_url = "/".join([img_base_url, key])
    print(img_url)
    file.save(path)
    try:
        res = client.put_object(
            Bucket=bb_bucket,
            Body=file.read(),
            Key=key,
            StorageClass="STANDARD",
            EnableMD5=False
        )
        file.close()
        print(res)
        return img_url
    except:
        file.close()
        return "上传失败"


@app.route("/upload_oss", methods=["POST"])
def upload_oss():
    file = request.files["image"]
    file_dir = request.form["dir"]

    print(file_dir)
    filename = secure_filename(file.filename)
    print(filename)
    ext = filename.rsplit(".", 1)[-1]
    if ext not in image_type:
        return "不支持此类文件上传"
    newname = str(ObjectId()) + "." + ext
    year = datetime.datetime.now().strftime("%Y")
    month =datetime.datetime.now().strftime("%m")
    day = datetime.datetime.now().strftime("%d")

    path = "/".join([file_dir, year, month, day, newname])
    print(path)
    img_url = "/".join([oss_domin, path])
    print(img_url)

    res = oss_client.put_object(path, file.read())
    file.close()
    if res.status == 200:
        return img_url
    else:
        return "上传失败"

    return "upload_oss"



@app.route("/upload_byte", methods=["POST"])
def upload_byte():
    with open("test02.jpg","wb") as fp:
        fp.write(request.data)

    return "test02"


if __name__ == "__main__":
    app.run(debug=True, port=5050)
