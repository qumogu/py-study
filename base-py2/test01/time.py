# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: time.py
@datetime: 2020/8/24 14:48
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')


from datetime import datetime, timedelta
from calendar import monthrange

now = datetime.now()
days = monthrange(now.year, now.month-1)
print(days)
next = now + timedelta(days=days[1])
print(next)
days = monthrange(next.year, next.month)
print(days)

def get_month(month):
    month = month % 12

    print month

get_month(-15)

past = datetime(2005,7,23,12,35,44,123456)

timspan = now -past



print(timspan)
print(timspan.days)
print(timspan.seconds)
print(timspan.resolution)