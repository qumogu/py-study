# -*- coding: utf-8 -*- 
"""
@author: Bruce
@contact: lanmin@itssoft.net
@file: dp_copy.py
@datetime: 2020/9/17 18:12
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

if __name__ == '__main__':
    # a = [["a", "b", "c"], [1, 2, 3], [2, 4, 8]]
    # b = []
    # for i in a:
    #     b.append(i)
    # a[0].append("d")
    # b[2].append(16)
    # print(a)
    # print(b)
    #
    # # 这个时候，a和b的值都将修改
    #
    # a = [["a", "b", "c"], [1, 2, 3], [2, 4, 8]]
    # b = []
    # for i in a:
    #     c = []
    #     for j in i:
    #         c.append(j)
    #
    #     b.append(c)
    #
    # a[0].append("d")
    # b[2].append(16)
    # print(a)
    # print(b)
    # # 这个时候，a和b相互独立的
    #
    # # 上面的代码可以简化为
    #
    # from copy import deepcopy
    #
    # a = [["a", "b", "c"], [1, 2, 3], [2, 4, 8]]
    # b = deepcopy(a)
    # a[0].append("d")
    # b[2].append(16)
    # print(a)
    # print(b)
    #
    # # 数字、字符串等数据类型，是有原子性的，可以直接复制
    # a = "hello world"
    # b = a
    # a = a.replace("o", "a")
    # print(a)
    # print(b)
    #
    # # 数组更新值
    # a = [["a", "b", "c"], [1, 2, 3], [2, 4, 8]]
    #
    # for i in a:
    #     for j in range(len(i)):
    #         i[j] += i[j]
    #
    # print(a)
    #
    # # 下面无法更新值
    # a = [["a", "b", "c"], [1, 2, 3], [2, 4, 8]]
    #
    # for i in a:
    #     for j in i:
    #         j += j
    #
    # print(a)
    #
    # # 字典更新值
    # a = [{"a": 1, "b": 2, "c": 4}, {"a": 3, "b": 9, "c": 9}, {"a": 4, "b": 8, "c": 16}]
    #
    # for i in a:
    #     i["a"] = i["a"] + i["b"]
    #     i["b"] = i["b"] + i["c"]
    #     i["d"] = i["c"] * 3
    #
    # print(a)

    class A(object):

        def __init__(self, a):
            self.a = a


    class B(object):

        def __init__(self, a):
            self.a =a


    test = [["a", "b", "c"], [1, 2, 3], [2, 4, 8]]

    tmp_a = A(test)
    tmp_b = B(test)

    test[0].append("d")
    tmp_b.a[2].append(16)
    print(tmp_a.a)
    print(tmp_b.a)

