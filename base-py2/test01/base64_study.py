# -*- coding: utf-8 -*-
"""
@author: Bruce
@contact: lanmin@itssoft.net
@file: base64_study.py
@datetime: 2021/3/5 15:22
"""
from __future__ import unicode_literals

import base64
import hashlib

a = "hello jack"

# a_byte = a.encode("utf8")
#
# b = base64.b64encode(a)
#
# print(type(b), b)
#
# c= base64.b64decode(b)
# print(type(c), c)
b = hashlib.md5(a).hexdigest()

print(b)