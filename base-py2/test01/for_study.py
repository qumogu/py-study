# -*- coding: utf-8 -*-
"""
@author: Bruce
@contact: lanmin@itssoft.net
@file: for_study.py
@datetime: 2021/2/24 11:40
"""
from __future__ import unicode_literals
import json
# a = [1,2,3]
#
# b = [i+1 for i in a]
#
# print(b)

# def test():
#     return "a", 1, "b"
#
# a,x,b = test()
# print(a,x,b)
#
# a = {
#     "name":"jack",
#     "age":28,
#     "extra":{
#         "height":180
#     }
# }
# keys= a.keys()
# print(keys)
# print (type(keys))
# print("1111111")
# # list1 = [1]
# # list2 = [2]
# list1 = []
# list2 = []
# list1.append(list2)
# list2.append(list1)
#
# print(list1, list2)
#
# print("222222")
import os

if __name__ == '__main__':
    # person = [{"name":"jack", "age":28}]
    # # # age = person.get("age", 10)
    # # age = os.environ.get("PERSONAGE", 10)
    # # print(type(age), age)
    #
    # for item in person:
    #     print(item)

    # a = "test_,_abc_,_123_,_"
    #
    # b = a.split("_,_")
    # print b
    # b = [i for i in b if i]
    # print b


    # a = {"name": "jack", "age": 23}
    #
    # a_str = json.dumps(a)
    # # if (isinstance(a_str, str)):
    # #     print("ok")
    # print(type(a_str))
    # # print(a_str)
    #
    # a_byte = a_str.encode()
    # print(type(a_byte))
    # print(a_byte)
    #
    # b = json.loads(a_byte)
    # print(type(b))
    # print(b.get("name"))

    # test_byte = "测试".encode("utf8")
    # print(type(test_byte))
    # print(test_byte)

    test_byte = "te_st".encode("utf8")
    print(type(test_byte))
    print(test_byte)
    t=test_byte.split("_")
    print(t)

