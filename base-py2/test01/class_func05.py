# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: class_func01.py
@datetime: 2020/8/20 17:44
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

class TestException(Exception):
    def __init__(self, err='缺少相关参数或参数不正确'):
        Exception.__init__(self, err)


def test(id):
    try:
        id = int(id)
        return id
    except:
        raise TestException

def test2(a, b):
    try:
        print(a + test(b))
    except:
        raise Exception

def test3(a, b):
    try:
        print(a + test(b))
    except Exception as e:
        raise e

if __name__ == '__main__':
    a = 10
    b = "a"
    try:
        test3(a,b)
    except Exception as e:
        if isinstance(e, TestException):
            print("测试成功")
            print(e)
        else:
            print("测试失败")
