# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: dict_test.py
@datetime: 2020/9/11 11:40
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')


fields_dict = {
    "id":102,
    "name":"jack",
    "desc":"jack love the world"
}

columns = [{"url":"www.baidu.com"},{"name":"lily"},{"friend":"lily"}]

fields = fields_dict.keys()

print(fields)

for column in columns:
    key = column.keys()[0]
    print(key)
    if key in fields:
        column.update({key:fields_dict.get(key)})

print(columns)

print(fields_dict['id'])