# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: class_func01.py
@datetime: 2020/8/20 17:44
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')


def test(**kwargs):
    print (kwargs.get("user_id"))

if __name__ == '__main__':
    api_params = {
        "user_id": "jack",
        "md5": "test02",
        "api_name": "test02",
        "version": 0
    }

    test(**api_params)