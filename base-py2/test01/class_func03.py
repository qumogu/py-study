# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: class_func01.py
@datetime: 2020/8/20 17:44
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')


class Test1(object):

    def get_msg(self):
        self.test_func()

    def test_func(self):
        print("00001111111")


if __name__ == '__main__':
    t = Test1()
    t.get_msg()