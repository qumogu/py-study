
# -*- coding: utf-8 -*-
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: re_study.py
@datetime: 2020/7/17 14:30
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

import re
import random,time,hmac,hashlib,base64,urllib

def check_url(url):
    if not re.match(r"http(s)?\:\/\/([a-zA-Z0-9]+)\.([a-zA-Z0-9]+)", url):
        return "{}是错误网址".format(url)
    else:
        return "{}是正常网址".format(url)


if __name__ == "__main__":

    # url = "htt:sdfsd"
    # print(check_url(url))
    # url = "http://sdfsd.123"
    # print(check_url(url))
    # url = "https://sdfsd.sdfsd.123.sdf"
    # print(check_url(url))
    random_nums = range(1000000)
    nonce = random.choice(random_nums)
    #nonce = random.randint(10000, 999999)
    cur_timestamp = int(time.time())

    code="735bd6a208f9d70762c1bc03ad67540b"

    data = {
        "Action": "GetUserAccessToken",
        "SecretId": "AKID40h7HBoB7vYv1qBmrkPaU7RYljQqFrBE",
        "userAuthCode": code,
        "Nonce": nonce,
        "Timestamp": cur_timestamp,
        "SignatureMethod": "HmacSHA256",
    }

    base = sorted(data.items(), key=lambda data: data[0])

    print(base)
    singna = []

    for i in base:
        singna.append(str(i[0]) + '=' + str(i[1]) + '&')
    result = 'GETopen.api.qcloud.com/v2/index.php?' + ''.join(singna).rstrip('&')
    secretkey = "1e05tRO6XnneEwQdbHhqkNnwRgeMnxEE"
    print(singna)
    print(result)
    sign = hmac.new(secretkey.encode("utf-8"),result, digestmod=hashlib.sha256).digest()
    sign_str = base64.b64encode(sign)
    print(sign_str)

    sign_uri = urllib.quote(sign_str)
    print(sign_uri)
