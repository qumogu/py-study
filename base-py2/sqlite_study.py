# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: sqlite_study.py
@datetime: 2020/6/29 16:41
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

import sqlite3
import os
from werkzeug.security import generate_password_hash

SQLITE_DB = "db"
SQLITE_NAME = "bruce.db"

def init_dir():

    path_list = SQLITE_DB.split("/")
    path = ""
    for i in path_list:
        path = os.path.join(path, i)
        print(path)
        if not os.path.exists(path):
            os.mkdir(path)
            print("创建目录{}成功".format(path))

def init_db():
    path = os.path.join(SQLITE_DB, SQLITE_NAME)
    conn = sqlite3.connect(path)
    c = conn.cursor()
    c.execute('''CREATE TABLE bs_users
           (id INTEGER PRIMARY KEY  AUTOINCREMENT ,
           username  CHAR(50) unique  NOT NULL,
           password  CHAR(100)    NOT NULL,
           role      CHAR(10),
           create_time datetime default (datetime('now', 'localtime')),
           login_ip  CHAR(20),
           last_login datetime default (datetime('now', 'localtime')) 
           );''')
    print "Table bs_user created successfully"
    conn.commit()
    c.execute('''CREATE TABLE bs_images
               (id INTEGER PRIMARY KEY  AUTOINCREMENT,
               uid int not null,
               filename  CHAR(50)  NOT NULL unique ,
               create_date  CHAR(20)    NOT NULL,
               size  INT  NOT NULL,
               visit_num INT default 1,
               create_time datetime default (datetime('now', 'localtime')),
               update_time datetime default (datetime('now', 'localtime'))
               );''')
    print "Table bs_images created successfully"
    conn.commit()
    conn.close()

def init_data():
    path = os.path.join(SQLITE_DB, SQLITE_NAME)
    conn = sqlite3.connect(path)
    c = conn.cursor()
    sql = "INSERT INTO bs_users (username,password,role) VALUES ('admin', '{}', 'admin') ".format(generate_password_hash("admin123"))
    print(sql)
    c.execute(sql)
    conn.commit()
    print "Records created successfully"
    conn.close()

def get_data():
    path = os.path.join(SQLITE_DB, SQLITE_NAME)
    conn = sqlite3.connect(path)
    c = conn.cursor()
    cursor = c.execute("select *from bs_users")
    print(cursor)
    for row in cursor:
        print(row)
    conn.close()

def execute(sql, type="fetchall"):
    print(sql)
    path = os.path.join(SQLITE_DB, SQLITE_NAME)
    conn = sqlite3.connect(path)
    c = conn.cursor()
    res = c.execute(sql)
    conn.commit()
    if type == "fetchall":
        res = [i for i in res]
    elif type == "fetchone":
        res = c.fetchone()
    else:
        res = conn.total_changes
    print(res)
    conn.close()
    return res


if __name__ == "__main__":
    # init_dir()
    # init_db()
    # init_data()
    # get_data()
    # sql = "select id,username,password,role from bs_users where id =1 limit 1"
    #sql = "insert into bs_users(username,password,role) values ('test3','test123','test02')"
    #sql = "select id,username from bs_users limit 1,2 "
    #execute(sql, "fetchall")
    #sql = "delete from bs_users where id = 5"
    # sql = "update bs_users set username = 'test4' ,password='test02' where id=4"
    # sql = "delete from bs_users where id in (2,3)"
    # res = execute(sql, "delete")
    sql = "select filename from bs_images"
    execute(sql)



