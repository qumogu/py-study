# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: pyqt5_study.py
@datetime: 2020/7/2 18:21
"""
import sys
import os
import hashlib
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QMessageBox
from PyQt5.Qt import QTextEdit, QLineEdit


class App(QWidget):

    def __init__(self):
        super().__init__()
        self.title = 'MD5校验'
        self.left = 400
        self.top = 300
        self.width = 800
        self.height = 600
        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        # create textbox
        self.textbox = QLineEdit(self)
        # self.textbox.setText('/Users/binyun007/Desktop/') #设置文本框的默认值
        self.textbox.setText(FileRecord.readpath())  # 读取文本框的默认值
        self.textbox.move(20, 20)
        self.textbox.resize(760, 400)

        # Create a button in the window
        self.button = QPushButton('保存', self)
        self.button.move(20, 480)

        # connect button to function on_click
        self.button.clicked.connect(self.on_click)
        self.show()

    def on_click(self):
        textboxValue = self.textbox.text()

        QMessageBox.question(self, "Message", '保存host',
                             QMessageBox.Ok, QMessageBox.Ok)
        """打印完毕之后设置文本框默认值为上一次使用后的"""
        FileRecord.writpath(textboxValue)
        # self.textbox.setText(textboxValue) #


# 保存、读取MD5记录
class FileRecord(object):
    #host_file = 'C:\\Windows\\System32\\drivers\\etc\\host'
    #host_file = 'C:\\Users\\adm\\Desktop\\my_demo\\test02.js'
    host_file = 'test'

    # 保存
    @classmethod
    def writpath(cls, value):
        try:
            with open(cls.host_file, 'wb') as f:
                print(value)
                value = value.encode('utf-8')
                f.write(value)
        except Exception:
            print("文件保存失败")

    # 读取
    @classmethod
    def readpath(cls):
        try:
            with open(cls.host_file, 'rb') as f:
                record = f.read()
                record = record.decode('utf-8')
                return record

        # 如果文件不存在创建
        except FileNotFoundError:
            print("文件打开失败")
            with open(cls.host_file, 'w') as f:
                return



if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    app.exit(app.exec_())