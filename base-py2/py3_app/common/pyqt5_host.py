# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: pyqt5_study.py
@datetime: 2020/7/2 18:21
"""
import sys
import os
import hashlib
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QMessageBox
from PyQt5.Qt import QLineEdit


class App(QWidget):

    def __init__(self):
        super().__init__()
        self.title = 'MD5校验'
        self.left = 800
        self.top = 600
        self.width = 320
        self.height = 200
        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        # create textbox
        self.textbox = QLineEdit(self)
        # self.textbox.setText('/Users/binyun007/Desktop/') #设置文本框的默认值
        self.textbox.setText(FileRecord.readpath())  # 读取文本框的默认值
        self.textbox.move(20, 20)
        self.textbox.resize(280, 40)

        # Create a button in the window
        self.button = QPushButton('校验', self)
        self.button.move(20, 80)

        # connect button to function on_click
        self.button.clicked.connect(self.on_click)
        self.show()

    def on_click(self):
        textboxValue = self.textbox.text()
        md5 = GetFileMd5(textboxValue)
        QMessageBox.question(self, "Message", 'MD5:' + md5,
                             QMessageBox.Ok, QMessageBox.Ok)
        """打印完毕之后设置文本框默认值为上一次使用后的"""
        FileRecord.writpath(textboxValue)
        # self.textbox.setText(textboxValue) #


# 保存、读取MD5记录
class FileRecord():

    # 保存
    def writpath(filepath):
        with open('md5.txt', 'w') as f:
            f.write(filepath)

    # 读取
    def readpath():
        try:
            with open('md5.txt', 'r') as f:
                record = f.readline()
                return record

        # 如果文件不存在创建
        except FileNotFoundError:
            with open('md5.txt', 'w') as f:
                return


# 校验MD5值
def GetFileMd5(filename):
    if not os.path.isfile(filename):
        return
    myHash = hashlib.md5()
    f = open(filename, 'rb')
    while True:
        b = f.read(8096)
        if not b:
            break
        myHash.update(b)
    f.close()
    return myHash.hexdigest()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    app.exit(app.exec_())