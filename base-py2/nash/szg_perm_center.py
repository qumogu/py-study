# -*- coding: utf-8 -*-
"""
@author: Bruce
@contact: lanmin@itssoft.net
@file: szg_perm_center.py.py
@datetime: 2021/1/6 11:01
"""
from __future__ import unicode_literals
import requests
from bson import ObjectId
import hashlib
import time
import random

paasId = "zhty"
paasToken = "IZUeFocyakSJSfJR4NkkAjTS1UhsXr6S"
app_code = "ZSSZG_2020"
api_host = "http://szgtest.gzonline.gov.cn/ebus/tgac"
transfer_uid_path = "/uum/ebus/personInfo/transferUserIdList"
get_menulist_path = "/govrbac/ebus/resources/getMenuList"
menu_code_prefix = "LDJSC"
menu_list_expire = 15*60

def sha256_signature(constr):
    """
    签名
    :param constr:
    :return:
    """
    print("constr:{}".format(constr))
    bytes_constr = constr.decode()
    sha256_signature = hashlib.sha256(bytes_constr).hexdigest()
    print("sha256_signature:{}".format(sha256_signature))
    return sha256_signature


def create_signature_headers():
    """
    创建网关签名hearder
    :return:
    """
    timestamp = str(time.time()).split(".")[0]
    nonce = str(random.random())
    signature = sha256_signature(timestamp + paasToken + nonce + timestamp)
    headers = {
        "x-tif-paasid": paasId,
        "x-tif-timestamp": timestamp,
        "x-tif-signature": signature,
        "x-tif-nonce": nonce,
        "Content-Type": "application/json",
        "Cache-Control": "no-cache"
    }
    print("create_signature_headers:", headers)
    return headers


def checkUserPermission(userid, oid, otype="gather"):
    """
    判断网关用户是否有权限访问资源
    """
    menulist = api_get_menu_list(userid)
    if menulist:
        if otype == "gather":
            if oid in menulist.get("gather_ids"):
                return True
        elif otype == "report":
            if oid in menulist.get("report_ids"):
                return True
    return False


def api_get_menu_list(sid):
    """
    从权限中心接口，获取用户有权访问的简报集IDS和简报页IDS
    """
    # 先转换用户ID,sid是从网关获取的userid, 转换成权限中心的userid
    uid = api_transfer_uid(sid)
    print("用户id转换数据：{}".format(uid))
    if not uid or uid == "null":
        return None
    body = {
        "appCode": app_code,
        "userId": uid
    }
    headers = create_signature_headers()
    url = api_host + get_menulist_path
    print("获取权限中心的用户菜单，请求参数body:{}, headers:{}, url:{}", body, headers, url)
    result = None
    try:
        r = requests.post(url, json=body, headers=headers)
        print("获取权限中心的用户菜单,返回信息：code:{}, text:{}".format(r.status_code, r.text))
    except Exception as e:
        print("权限中心:获取用户菜单API请求连接报错，错误:{}".format(e))
        return result

    if r.status_code == 200:
        try:
            jsondata = r.json()
        except Exception as e:
            print("权限中心:获取用户菜单API，json序列化失败，报错：{}".format(e))
            return result
        if jsondata.get('errcode') != 0:
            print("权限中心:获取用户菜单API，返回数据：errcode不为0")
            return result
        menusdata = jsondata.get("data")
        if not menusdata:
            print("权限中心:获取用户菜单API，返回数据：用户菜单信息为空")
            return result
        gather_ids = []
        report_ids = []
        for menu in menusdata:
            menu_code = menu.get("menuCode")
            if menu_code_prefix in menu_code:
                url = menu.get("menuUrl")
                url_list = url.split("/")
                if len(url_list)>1:
                    if url_list[-2] == "gather":
                        if "." in url_list[-1]:
                            id_list = url_list[-1].split(".")
                            gid = id_list[0]
                        else:
                            gid = url_list[-1]
                        if ObjectId.is_valid(gid):
                            gather_ids.append(gid)
                    elif url_list[-2] in ["banber", "embed"]:
                        if "." in url_list[-1]:
                            id_list = url_list[-1].split(".")
                            rid = id_list[0]
                        else:
                            rid = url_list[-1]
                        if ObjectId.is_valid(rid):
                            report_ids.append(rid)
        result = {
            "gather_ids": gather_ids,
            "report_ids": report_ids,
        }

    return result


def api_transfer_uid(sid):
    """
    从权限中心接口，转换用户ID
    """
    body = {
        "userIdList": [],
        "sourceIdList": [sid]
    }
    headers = create_signature_headers()
    url = api_host + transfer_uid_path
    print("获取权限中心的转换用户ID接口，请求参数body:{}, headers:{}, url:{}", body, headers, url)

    try:
        r = requests.post(url, json=body, headers=headers)
        print("获取权限中心：转换用户ID接口,返回信息：code:{}, text:{}".format(r.status_code, r.text))
    except Exception as e:
        print("权限中心：转换用户ID接口，API请求连接报错，错误:{}".format(e))
        return None

    if r.status_code == 200:
        try:
            jsondata = r.json()
        except Exception as e:
            print("权限中心：转换用户ID接口，json序列化失败，报错：{}".format(e))
            return None
        if jsondata.get('errcode') != 0:
            print("权限中心：转换用户ID接口，返回数据：errcode不为0")
            return None
        uidsdata = jsondata.get("data")
        if not uidsdata:
            print("权限中心：转换用户ID接口，返回数据：data为空")
            return None
        uid = uidsdata[0].get("id")
        if not uid:
            print("权限中心：转换用户ID接口，返回userid信息为空")
        return uid

if __name__ == '__main__':
    gid = "5fb642764f2d37000153e039"  #首页
    #gid = "5fb642764f2d37000153e03a"  #智慧党建
    #gid = "5fc5b3d638c86500015756c4"  #经济运行
    #gid = "5fc8b52dd3adbd000113285c"  #营商环境

    userid = "axppo0sk741smgsqh9ljcg"  #黄海林
    #userid = "7p2bx74p5al7p7oo3giu13"  #张锋
    #userid = "qiee4651wp1pkzullrevgz"  #陈琳

    result = checkUserPermission(userid, gid)
    print(result)






