# -*- coding: utf-8 -*-
"""
@author: bruce
@contact: lanmin@itssoft.net
@file: dir.py
@datetime: 2020/12/29 14:28
"""
from __future__ import unicode_literals

import os

def getdir():
    if os.path.exists("./test/html"):
        print("test01")
    else:
        print("err")

    if not os.path.exists("./test/html2"):
        return None
    print("test02")

if __name__ == '__main__':
    getdir()
