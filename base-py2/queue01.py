# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: queue01.py
@datetime: 2020/6/19 18:13
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

from queue import LifoQueue
from threading import Thread
import time_study
import random

q = LifoQueue(10) # 初始化队列最大值

def put():
    for i in range(20):
        q.put(i)
        print("队列插入值：%d" % i)


def get():
    for i in range(20):
        value = q.get()
        print("从队列中取出值%d" % value)
        q.task_done()


t_p = Thread(target=put,args=())
t_p.start()
t_g = Thread(target=get, args=())
t_g.start()
t_p.join()
t_g.join()
print("执行完成")