# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: upload.py
@datetime: 2020/6/29 14:17
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')
import mimetypes
import os

def get_mimetype():
    filename = "test02.jpg"
    res = mimetypes.guess_type(filename)[0]
    print(res)
    res = mimetypes.guess_extension("zip")
    print(res)
    res = mimetypes.guess_all_extensions(".gif")
    print(res)

IMG_UPLOAD_DIR = "uploads/imgs"

def init_dir():
    path_list = IMG_UPLOAD_DIR.split("/")
    path = ""
    for i in path_list:
        path = os.path.join(path, i)
        print(path)
        if not os.path.exists(path):
            os.mkdir(path)
            print("创建目录{}成功".format(path))

if __name__ == "__main__":
    a = [1,2,3,4]
    a = [str(i) for i in a]
    b = ",".join(a)
    print(b)