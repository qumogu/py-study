# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: queue01.py
@datetime: 2020/6/19 18:13
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

from queue import PriorityQueue
from threading import Thread
import time_study
import random

q = PriorityQueue(20)  # 初始化队列最大值

def put():
    for i in range(20):
        level = random.randint(1,10)
        q.put((level,i))
        print("队列插入值：%d，优先级：%d" % (i, level))


def get():
    for i in range(20):
        value = q.get()
        print("从队列中取出值{}".format(value[1]))
        q.task_done()
        time_study.sleep(1)


t_p = Thread(target=put,args=())
t_p.start()
time_study.sleep(2)
t_g = Thread(target=get, args=())
t_g.start()
t_p.join()
t_g.join()
print("执行完成")