# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: platform_01.py
@datetime: 2020/4/8 10:19
"""

import platform
import os



def main():
    print (platform.machine())  #系统平台
    print (platform.node())     #网络计算机名
    print (platform.system())   #系统信息
    print (platform.platform()) #系统版本
    print (platform.processor())  #cpu信息
    print (platform.version())

    print (platform.python_version())  #python的信息
    print (platform.python_build())
    data = os.environ
    #for k,v in data.items():
     #   print (k)
    print (data.get("OS"))


if __name__ == '__main__':
    main()