
# -*- coding: utf-8 -*-
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: re_study.py
@datetime: 2020/7/17 14:30
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

import re

SQL_FORBID_KEY_WORD = ["create", "drop", "delete", "truncate", "update", "alter", "set", "insert", "into", "commit",
                        "database", "table", "exec", "rollback", "trigger", "execute", "declare", "grant"]

def check_url(url):
    if not re.match(r"http(s)?\:\/\/([a-zA-Z0-9]+)\.([a-zA-Z0-9]+)", url):
        return "{}是错误网址".format(url)
    else:
        return "{}是正常网址".format(url)


def check_sql(sql):
    sql = str(sql).lower()

    #sql = sql.replace('"',"'")
    if ";" not in sql:
        sql = sql + ";"

    if not re.match(r"^select [\w\s\"\'\.\,\(\)\+\-\*\/\%\=\<\>\!\&\|\^\~]+;$", sql):
        print("{}\n是错误sql语句".format(sql))
    else:
        print("{}\n是正常sql语句".format(sql))

    # if not re.search(r"^select [\w\s\"\'\.\,\(\)\+\-\*\/\%\=\<\>\!\&\|\^\~]+", sql):
    #     print("{}\n是错误sql语句".format(sql))
    # else:
    #     print("{}\n是正常sql语句".format(sql))

    sql_keys = sql.split()
    for key in sql_keys:
        print(key)
        if key in SQL_FORBID_KEY_WORD:
            print("sql语句,包含非select查询关键字{}".format(key))

def check_source_sql(str_sql):

    if not str_sql:
        return False

    str_sql = str(str_sql).lower()
    if ";" not in str_sql:
        str_sql = str_sql + ";"
    if not re.match(r"select [\u4e00-\u9fa5\w\s\"\'\.\,\(\)\+\-\*\/\%\=\<\>\!\&\|\^\~]+;$", str_sql):
        return False

    sql_keys = str_sql.split()

    for key in sql_keys:
        if key in SQL_FORBID_KEY_WORD:
            print(key)
            return False

    return True


def test1(x, y):
    for i in range(x):
        print(i)
        if i == y:
            return False
    return True


class MyclassA():

    # def __str__(self):
    #     return "ok"

    def __init__(self):
        self.name = "test02"

if __name__ == "__main__":

    # url = "htt:sdfsd"
    # print(check_url(url))
    # url = "http://sdfsd.123"
    # print(check_url(url))
    # url = "https://sdfsd.sdfsd.123.sdf"
    # print(check_url(url))

    sql = u"seLect id * as ai_d '中文' from 'teststtad'"
    #zsql = 'seLect id,\"create_time\",t.username as "name", \'curdate(update_time),a+b,a-b,a*b,c/d,a%b,a&b,a|b,a^b,~a from drop_test as t join user as u on t.id = u.user_id where t.id >= 100 and u.id <= 100 and u.count != 0'
    #sql = "SELECT top 5 a.id,a.username,a.age,b.classname FROM dbo.student as a INNER JOIN dbo.stu_class as b ON a.class_id=b.id ORDER BY id desc "
    #sql ="SELECT a.id as aid,a.username,from_unixtime(a.int_time) as create_time,a.age*10000000/a.int_time as rate, b.age from test_user as a inner join test02 as b on a.username = b.username limit 5"
    #sql = "select *From test02;drop table test_delete;"
    #sql = MyclassA()

    print(sql)

    # if "set" in SQL_FORBID_KEY_WORD:
    #     print("ok")

    check_sql(sql)
    # print("*"*20)
    #
    # print(check_source_sql(sql))




