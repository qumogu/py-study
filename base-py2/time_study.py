# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: time_study.py
@datetime: 2020/7/14 18:56
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

import time

print(int(time.time()))

from bson import ObjectId

print(str(ObjectId()))