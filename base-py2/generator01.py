# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: generator.py
@datetime: 2020/6/18 16:10
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

def test_gene():
    print(123)
    value = yield 1
    yield "***"+value

def test_gene2(num):

    print(123)
    value = yield 1
    yield "***"+value


y1 = test_gene()

print(y1.next())
print(y1.send('aaa'))
print(y1.next())