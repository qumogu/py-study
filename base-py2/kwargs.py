# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: kwargs.py
@datetime: 2020/8/31 11:44
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')


def test(name, age, school, teacher="lucy", sclass="四（3）班", year='2020'):
    print("今年是{}年,{}，今年{}岁，在{}上学，是{}".format(year, name, age, school, sclass))


if __name__ == '__main__':
    testkws= {}
    testkws['name'] = "小明"
    testkws['sclass'] = "一（1）班"
    #testkws['teacher'] = "lily"
    testkws['school'] = "光明小学"
    testkws['age'] = 8


    test(**testkws)