# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: test_md.py
@datetime: 2020/7/29 17:24
"""
from __future__ import unicode_literals


class User(object):

    def __init__(self, username):
        self.username = username

    @property
    def password(self):
        raise AttributeError("密码不可以读")

    @password.setter
    def password(self, value):
        self.user_info = self.username + value

    @property
    def user_info(self):
        return "用户名:" + self.username

    @user_info.setter
    def user_info(self, value):
        self.username = value

import requests

if __name__ == "__main__":
    url = "https://www.banber.com/tuxi/images/uploads/2020-10-14/2020-10-14-11-18-44-845349_b638fb42cf49be3_9eabf4084048f1d3d96a02b67c46ebed.png"
    url2 = "https://www.banber.com/tuxi/images/uploads/2020-10-16/2020-10-16-17-21-31-200583_fa23c740ae6d7f87_bde7cf9049e122a728d29f2213bb2b67.gif?x-oss-process=image/resize,m_fixed,h_68,w_1391"
    r = requests.get(url, verify=False)
    file = "3.gif"
    print(r.status_code)
    with open(file, 'wb') as f:
        f.write(r.content)
