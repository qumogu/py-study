# -*- coding: utf-8 -*-
"""
@author: Bruce
@contact: lanmin@itssoft.net
@file: base.py
@datetime: 2021/2/22 16:51
"""
from __future__ import unicode_literals
from requests import ConnectTimeout
from requests import ConnectionError
import requests

class TestException(Exception):
    def __init__(self, err='test err'):
        Exception.__init__(self, err)

class Test():

    def get_info(self, info):
        print (info)
        print(msg_list)
        if info == "test err":
            raise TestException
        elif info == "err":
            raise Exception
        elif info == "time out":
            raise ConnectTimeout
        return info + ",  bruce is ok"

def move_msg(msg):
    print("______===_______")
    msg_list.remove(msg)


if __name__ == '__main__':
    msg_list = list()
    t = Test()
    try:
        msg = "time"
        msg_list.append(msg)
        info = t.get_info(msg)
        print(info)
        r = requests.get(url="https://www.banber222.com")
        # r = requests.get(url='http://dict.baidu.com/s', params={'wd': 'python'})
        print(r.status_code)

    except TestException as e:
        print("自定义错误信息:{}".format(e))
    except ConnectionError2 as e:
        print("连接超时错误错误信息:{}".format(e))
    except Exception as e:
        print("通用错误信息:{}".format(e))
    finally:
        move_msg(msg)
        print(msg_list)

    print("执行到最后了")



