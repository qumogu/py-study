# -*- coding: utf-8 -*-
"""
@author: Bruce
@contact: lanmin@itssoft.net
@file: enum.py
@datetime: 2021/1/11 19:23
"""
from __future__ import unicode_literals

from graphene import Enum

class Role(Enum):
    ADMIN = 1
    EDITOR = 2
    VIEWER = 3

if __name__ == '__main__':
    assert Role.get(1) != Role.ADMIN