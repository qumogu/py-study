# -*- coding: utf-8 -*-
"""
@author: Bruce
@contact: lanmin@itssoft.net
@file: base.py
@datetime: 2021/1/11 17:49
"""
from __future__ import unicode_literals

from graphene import Schema, String, ObjectType, Int
from graphql.language import ast
import json

class Query(ObjectType):
    hello = String(name=String(default_value="stranger"))
    test = String(name=String(default_value="test"), age=Int(default_value=22))
    add = Int(x=Int(), y=Int())
    goodbye = String()

    def resolve_hello(self, info, name):
        return "hello, "+name

    def resolve_test(self, info, name, age):
        ret = {
            "msg":"success",
            "func": "test",
            "data": name + ", " + str(age)
        }
        return json.dumps(ret)

    def resolve_add(self, info, x, y):
        return x+y

    def resolve_goodbye(self, info):
        print(info)
        return "goodbye"

if __name__ == '__main__':
    s = Schema(query=Query)
    q = "query{goodbye}"
    r1 = s.execute(q)
    print(r1.data['goodbye'])
    # r1 = s.execute("{add(x:20, y:30)}")
    # print(type(r1.data['add']), r1.data['add'])

    # result = s.execute("{hello}")

    # print(result)
    # print(result.data['hello'])
    #
    # r2 = s.execute(' {test(name:"jack", age:33) }')
    # print(r2.data['test'])
    # ret_dict = json.loads(r2.data['test'])
    # print(ret_dict.get("data"))

    # r2 = s.execute(' { hello(name:"jack") }')
    # print(r2.data['hello'])

