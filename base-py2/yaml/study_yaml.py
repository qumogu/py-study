# -*- coding: utf-8 -*- 
"""
@author: Bruce
@contact: lanmin@itssoft.net
@file: study_yaml.py
@datetime: 2020/11/10 16:58
"""
# from __future__ import unicode_literals
import yaml

if __name__ == '__main__':

    users = [{"username":"jack", "age":23}, {"username":"lily", "age":20}, {"username":"tom", "age":25}]

    data = {
        "web_name": "testa",
        "url": "http://www.testa.com",
        "users": users
    }

    filename = "test02.yaml"

    with open(filename, 'w') as f:
        yaml.dump(data, f)
    print("ok")