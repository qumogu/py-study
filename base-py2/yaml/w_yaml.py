# -*- coding: utf-8 -*- 
"""
@author: Bruce
@contact: lanmin@itssoft.net
@file: study_yaml.py
@datetime: 2020/11/10 16:58
"""
from __future__ import unicode_literals
import yaml

if __name__ == '__main__':
    filename = "ini.yaml"
    with open(filename,'rb') as f:
        data = f.read()
        test = yaml.load(data)

    if test:
        print (test['apps'][0]['app_name'])