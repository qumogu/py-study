# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: log.py
@datetime: 2020/4/8 10:53
"""

import logging

def get_logger():
    #     """
    #         设置全局logging 输出，并兼容docker日志输出
    #     :param level:
    #     :return:
    #     """
    logger = logging.getLogger()
    # logger = logging.getLogger('gunicorn.error')

    handler = logging.StreamHandler()
    formatter = logging.Formatter(
        '%(asctime)s %(filename)s [line:%(lineno)d] %(levelname)s %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)
    return logger

logger = get_logger()