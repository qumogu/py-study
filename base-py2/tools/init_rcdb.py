# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: init_rcdb.py.py
@datetime: 2020/5/9 14:52
"""

import logging


def get_logger():
    """
    设置全局logging 输出，并兼容docker日志输出
    :return:
    """
    log = logging.getLogger()

    handler = logging.StreamHandler()
    formatter = logging.Formatter('%(asctime)s %(filename)s [line:%(lineno)d] %(levelname)s %(message)s')
    handler.setFormatter(formatter)
    log.addHandler(handler)
    log.setLevel(logging.DEBUG)
    return log

logger = get_logger()
