# -*- coding: utf-8 -*- 
"""
@author: Bruce
@contact: lanmin@itssoft.net
@file: test02.py
@datetime: 2020/10/22 17:50
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')


"""
    def _publish(self, data):
        retry = True
        while True:
            try:
                if not retry:
                    self._redis_connect()
                return self.redis.publish(self.channel, pickle.dumps(data))
            except redis.exceptions.ConnectionError:
                if retry:
                    logger.error('Cannot publish to redis... retrying')
                    retry = False
                else:
                    logger.error('Cannot publish to redis... giving up')
                    break
"""

class School(object):

    def __init__(self, name, address):
        self.name = name
        self.address = address

    def get_info(self):
        print("{}的地址是{}".format(self.name, self.address))


class Student(object):

    def __init__(self, name, age):

        self.name = name+"_base"
        self.age = age

        self.get_name(name)

    def get_info(self):
        print("{}的年龄是{}岁".format(self.name, self.age))

    def get_name(self, name):
        self.name = name +"********_base"
        #print(self.name)



class SmallStudent(Student):

    def __init__(self, school, name="jack", age=0):
        self.school = school
        self.age = age
        self.init_num()
        self.get_name(name)
        print("SmallStudent init")
        super(SmallStudent, self).__init__(name=name, age=age)

    def init_num(self):
        self.num = self.age * 10

    def get_num(self):
        print(self.num)

    def get_name(self, name):
        self.name = name + "+++_base"
        #print(self.name)

    def test(self):
        self.init_num()
        print(self.num)


class MiddleStudent(SmallStudent):

    def __init__(self, school, name, age=20):
        self.school = school
        self.age = age
        self.init_num()
        super(MiddleStudent, self).__init__(school="测试", name=name, age=age)

    def init_num(self):
        self.num = self.age+5


if __name__ == '__main__':
    s = School("阳光小学", "南山区粤海街道科技中路9号")
    m = MiddleStudent(s, "may", 30)
    m2 = SmallStudent("南山小学", "lily", 30)
    m3 = Student("tom",15)

    m.get_num()
    m2.get_num()
    print("_______")
    print(m.name)
    print(m2.name)
    print(m3.name)
    print("_______")
    m.test()
    m2.test()
    # m.get_name()
    # m2.get_name()
    # print("_______")
    # print(m.school)
    # print(m.name)
    # print(m.age)
    # print("_______")
    # print(m2.school)
    # print(m2.name)
    # print(m2.age)
    # print("_______")
    # print(m3.name)
    # print(m3.age)