
# -*- coding: utf-8 -*-
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: re_study.py
@datetime: 2020/7/17 14:30
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

import re
import random,time,hmac,hashlib,base64,urllib

def check_host(host):
    if not re.match(r"^[a-zA-Z0-9_]{3,6}\.edu\.qumogu\.com$", host):
        print "{}是错误域名".format(host)
    else:
        print "{}是正常域名".format(host)


if __name__ == "__main__":

    host = "testa.edu.qumogu.com11"

    check_host(host)