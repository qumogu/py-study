# -*- coding: utf-8 -*-
"""
@author: Bruce
@contact: lanmin@itssoft.net
@file: err01.py
@datetime: 2021/3/3 11:48
"""
from __future__ import unicode_literals
import pdb

str_num = "9"
test_dict = {
    "name":"jack",
    "pwd":"jack123",
    "age":23
}
num = int(str_num)

test_dict.update({"age":35})
pdb.set_trace()
num = num + 1

print(str_num, num)