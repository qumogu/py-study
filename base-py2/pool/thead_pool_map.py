# -*- coding: utf-8 -*- 
"""
@author: Bruce
@contact: lanmin@itssoft.net
@file: requests_img.py
@datetime: 2020/9/24 10:08
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')
from bson import ObjectId
import requests
import time

from multiprocessing.pool import ThreadPool

def down_images(num,urls):
    files= []
    i = 1
    for url in urls:
        j = num*3+i
        filename = "imgs/" + str(j) + "_" + str(ObjectId()) + ".jpg"
        try:
            r = requests.get(url)
        except:
            print("第{}进程第{}张的图片{}连接失败，下载错误".format(num + 1, j, url))
            continue

        with open(filename, 'wb') as f:
            f.write(r.content)
        print("第{}进程第{}张的图片{}下载成功".format(num+1, j, url))
        files.append({str(j):filename})
        time.sleep(1)
        i += 1
    return files


if __name__ == '__main__':

    # "http://img.saihuitong.com/5242/img/2823869/17375b6e9ea.jpg",
    urls = ["http://img.saihuitong.com/5242/img/2823869/17375b6e9ea.jpg",
            "http://img.yyyyysaihuitong.com/5242/img/2823869/xxxx.jpg",
            "http://xiumi.saihuitong.com/xmi/ua/1kDHe/i/c6d12ac1eb0283b5e4f9df29e32ca547-sz_53085.jpg?x-oss-process=style/xmorient",
            "http://xiumi.saihuitong.com/xmi/ua/1kDHe/i/ca0cc58a83c5643be9ab71bf40a8458b-sz_106055.jpg?x-oss-process=style/xmorient",
            "http://xiumi.saihuitong.com/xmi/ua/1kDHe/i/c10a93fd53e2c36a40356e67819dde4c-sz_87536.jpg?x-oss-process=style/xmorient",
            "http://xiumi.saihuitong.com/xmi/ua/1kDHe/i/b092350fd27fcdb0c82ab535a5240021-sz_78611.jpg?x-oss-process=style/xmorient",
            "http://xiumi.saihuitong.com/xmi/ua/1kDHe/i/ce9705fc0e05af18097f0c8c69dc51ef-sz_78443.jpg?x-oss-process=style/xmorient",
            "http://xiumi.saihuitong.com/xmi/ua/1kDHe/i/19fcb4a2e4b8b02dcb7980840fe91c2d-sz_39962.jpg?x-oss-process=style/xmorient",
            "http://xiumi.saihuitong.com/xmi/ua/1kDHe/i/20b07c7e3078a5044f88635b14597d87-sz_2932563.jpg?x-oss-process=style/xmhigh",
            "http://xiumi.saihuitong.com/xmi/ua/1kDHe/i/725e0d0a139b1592af52255024094388-sz_356162.jpg?x-oss-process=style/xmorient",
            "http://xiumi.saihuitong.com/xmi/ua/1kDHe/i/806b77b5649a93245a0553020f10fe16-sz_740256.jpg?x-oss-process=style/xmorient",
            "http://xiumi.saihuitong.com/xmi/ua/1kDHe/i/7e5fd634d6287cc74a890d78d47de3b8-sz_97598.jpg?x-oss-process=style/xmorient",
            "http://img.saihuitong.com/5242/img/2823869/xxxx.jpg",]

    p_list = []
    start_time = time.time()
    p = ThreadPool(4)
    for i in range((len(urls)+2)/3):
        new_urls = urls[i*3:(i+1)*3]
        print(new_urls)

        p_list.append(p.apply_async(down_images, args=(i,new_urls)))
        #p_list.append(p)
        print("第{}个线程开启".format(i + 1))

    p.close()
    p.join()
    end_time = time.time()
    print("耗时{}".format(end_time - start_time))
    all_files=[]
    for i in p_list:
        all_files.extend(i.get())

    print(all_files)


