# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: date_study.py
@datetime: 2020/6/19 17:07
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

from datetime import datetime, timedelta
from dateutil import parser


start_str = "2020-06-09"
cur_time = datetime.strptime(start_str,'%Y-%m-%d')
print(type(cur_time))
print(cur_time)
aDay = timedelta(days=1)
# end_time = datetime.strptime("2020-01-10",'%Y-%m-%d')
end_time = datetime.now()

datetime.datetime.fromtimestamp(1592903010)
while True:
    cur_time = cur_time+aDay
    if cur_time > end_time:
        break
    print(cur_time.strftime('%Y-%m-%d'))

