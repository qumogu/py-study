# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: context01.py.py
@datetime: 2020/6/19 11:22
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

from contextlib import contextmanager

class Test(object):
    def __init__(self, name):
        self.name = name
        self.age = None

    def update_name(self, name):
        self.name

    def set_age(self,age):
        self.age = age

    def get_info(self):
        print("上下文管理测试基类，信息{}-{}".format(self.name,self.age))

    def hand_error(self):
        print("输出错误")



class TestWith(Test):

    @contextmanager
    def do_with(self):
        try:
            yield
            self.get_info()
        except Exception as e:
            self.hand_error()
            raise e


a = TestWith("testname")
with a.do_with():
    a.update_name("jack")
    a.set_age(25)