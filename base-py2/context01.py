# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: context01.py.py
@datetime: 2020/6/19 11:22
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

from contextlib import contextmanager

class Test(object):
    def __init__(self, name):
        self.name = name
        self.age = None

    def update_name(self, name):
        self.name = name

    def set_age(self,age):
        self.age = age

    def get_info(self):
        print("上下文管理测试基类，信息{}-{}".format(self.name,self.age))

    def hand_error(self):
        print("输出错误")

    def clear(self):
        self.age = None
        print("清除年龄数据")



class TestWith(Test):

    @contextmanager
    def do_with(self):
        try:
            yield
            self.get_info()
        except Exception as e:
            self.hand_error()
            raise e

class TestContext():

    def __init__(self,name,test_key,test_val):
        self.t = Test(name)
        self.tdict = {test_key:test_val}

    def __enter__(self):
        print("用户信息:{}".format(self.tdict.get("name","没有信息")))
        return self.t

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.t.clear()


a = TestWith("testname")
with a.do_with():
    a.update_name("jack")
    a.set_age(25)


with TestContext("lily", "name","jack") as tc:
    tc.set_age(25)
    tc.get_info()