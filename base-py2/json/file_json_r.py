# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: file_json_w.py
@datetime: 2020/6/19 17:42
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')
import json


with open("../log.txt", "r") as f:
    data = f.read()
    data = data.split("\n")[-1]
    json_data = json.loads(data)
    print(json_data["title"])
    