# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: file_json_w.py
@datetime: 2020/6/19 17:42
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')
import json

data = {"title":"测试","itemId":"1111ddff","num":[1,3,4,9]}
json_data = "今天的数据：\n"+json.dumps(data)

with open("../log.txt", "w") as f:
    f.write(json_data)
