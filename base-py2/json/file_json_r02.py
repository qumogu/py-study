# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: file_json_w.py
@datetime: 2020/6/19 17:42
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')
import json


with open("conf.json", "r") as f:
    data = f.read()
    # data = data.split("\n")[-1]
    config = json.loads(data)
    print(config.get("redis_info").get("Host"))

    with open("conf2.json", "wb") as f:
        f.write(data)
    