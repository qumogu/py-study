# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: dataset_tmp.py
@datetime: 2020/8/20 16:44
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

current_sort_value = [u'Longgang', u'Baoan', u'Futian', u'Luohu', u'Longhua']

# for i in current_sort_value:
#     print(i)

custom_index = 0

train_x_list = [[1, u'Baoan'], [1, u'Baoan'], [2, u'Futian'], [2, u'Futian'], [0, u'Longgang'], [0, u'Longgang'], [4, u'Longhua'], [4, u'Longhua'], [3, u'Luohu'], [3, u'Luohu']]

new_list = []
for i in train_x_list:
    if not i in new_list:
        new_list.append(i)

print(new_list)

for i in new_list:

    try:
        print()
        column_value = current_sort_value[int(i[int(custom_index)])]
        print(i[int(custom_index)], column_value)
        i[int(custom_index)] = column_value
    except Exception as e:
        print(e)

print(new_list)