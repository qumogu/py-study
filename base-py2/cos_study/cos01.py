# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: cos01.py
@datetime: 2020/6/22 10:47
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

from qcloud_cos import CosConfig, CosS3Client

import logging
import sys
from bson import ObjectId
logging.basicConfig(level=logging.INFO, stream=sys.stdout)

secret_id = "AKIDNRVrxt4PlkRncAtL2arsIJAoOs7LIxHa"
secret_key = "5uOdZNYkhX0CLGB6wiFQLLBk5AMPH7g6"
region = "ap-guangzhou"
bb_bucket = "tuxi-1301633205"
token = None
scheme = "https"
img_base_url = "https://tuxi-1301633205.cos.ap-guangzhou.myqcloud.com/"
dir_name = "test02"

config = CosConfig(Region=region, SecretId=secret_id, SecretKey=secret_key, Token=token, Scheme=scheme)
client = CosS3Client(config)

# 查询所有桶
# response = client.list_buckets()
# print(type(response))
# print(response)

# 创建新的桶 该方法返回值为 None。
# bucket_name = "bruce.db-1301633205"
# response = client.create_bucket(Bucket=bucket_name)
# print(type(response))
# print(response)

# 删除桶
# bucket_name = "bruce.db-1301633205"
# response = client.delete_bucket(Bucket=bucket_name)
# print(type(response))
# print(response)

# 上传文件
# with open('img/fj2.jpg', 'rb') as fp:
#     response = client.put_object(
#         Bucket=bb_bucket,
#         Body=fp,
#         Key="test02/brcue_fj1-02.jpg",
#         StorageClass="STANDARD",
#         EnableMD5=False
#     )
# print(type(response))
# print(response["ETag"])
# # a8952e69fb70756e504b3b7d26420a66
# print(response)

# 查询对象
# response = client.list_objects(
#     Bucket=bb_bucket
# )
# print(type(response))
# print(response["Contents"])
# print(response)

# 下载对象
# response = client.get_object(
#     Bucket=bb_bucket,
#     Key="brcue_fj3.jpg",
# )
# response['Body'].get_stream_to_file("test02.jpg")

# 查询ACL
# response = client.get_bucket_acl(
#     Bucket=bb_bucket
# )
# print(type(response))
# print(response)

# 查询自定义域名
# res = client.get_bucket_domain(
#     Bucket=bb_bucket
# )
# print(res)

def upload_file(file_path):
    file_ext = file_path.rsplit(".",1)[1]
    new_name = str(ObjectId())+"."+file_ext
    key = dir_name + "/" + new_name
    img_url = img_base_url + key
    with open(file_path,"rb") as fp:
        try:
            res = client.put_object(
                Bucket=bb_bucket,
                Body=fp,
                Key=key,
                StorageClass="STANDARD",
                EnableMD5=False
            )
            print(res)
            return img_url
        except:
            return None

def is_exist(file_path):
    res = client.object_exists(
        Bucket=bb_bucket,
        Key=file_path
    )
    return res

def get_file(file_path):
    if not is_exist(file_path):
        return None
    res = client.get_object(
        Bucket=bb_bucket,
        Key=file_path
    )
    print(res)

def delete_file(file_path):
    if not is_exist(file_path):
        print("文件不存在")
        return None
    res = client.delete_object(
        Bucket=bb_bucket,
        Key=file_path
    )
    print(res)

def delete_files(file_paths):
    del_obj = dict()
    del_obj["Quiet"] = "true"
    del_obj["Object"] = [{"Key": i} for i in file_paths]

    print(del_obj)

    res = client.delete_objects(
        Bucket=bb_bucket,
        Delete=del_obj
    )

    print(res)

def get_down_url(file_path):
    res = client.get_presigned_download_url(
        Bucket=bb_bucket,
        Key=file_path
    )

    print(res)

if __name__ == "__main__":

    # url = upload_file("img/rw.jpg")
    # if url:
    #     print(url)
    # else:
    #     print("上传失败")
    file_path = "test02/5ef0564be3f4e6288434e71c.jpg"
    # is_exist(file_path)
    # get_file(file_path)
    delete_files(['test02\\5ef16e4ee3f4e65a2842dda4_fj3.jpg', "test02\\5ef16f54e3f4e6634c0ff1c3_fj3.jpg"])
    # delete_file('test02/brcue_fj3-01.jpg')
    # get_down_url("test02/5ef04df4e3f4e64330d1f8ae.jpg")