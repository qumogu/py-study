# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: cos01.py
@datetime: 2020/6/22 10:47
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

from qcloud_cos import CosConfig, CosS3Client

import logging
import sys
from bson import ObjectId
logging.basicConfig(level=logging.INFO, stream=sys.stdout)

secret_id = "AKIDNRVrxt4PlkRncAtL2arsIJAoOs7LIxHa"
secret_key = "5uOdZNYkhX0CLGB6wiFQLLBk5AMPH7g6"
region = "ap-guangzhou"
bb_bucket = "tuxi-1301633205"
token = None
scheme = "https"
img_base_url = "https://tuxi-1301633205.cos.ap-guangzhou.myqcloud.com/"
dir_name = "test02"

config = CosConfig(Region=region, SecretId=secret_id, SecretKey=secret_key, Token=token, Scheme=scheme)
client = CosS3Client(config)

# 查询所有桶
# response = client.list_buckets()
# print(type(response))
# print(response)

# 创建新的桶 该方法返回值为 None。
# bucket_name = "bruce.db-1301633205"
# response = client.create_bucket(Bucket=bucket_name)
# print(type(response))
# print(response)

# 删除桶
# bucket_name = "bruce.db-1301633205"
# response = client.delete_bucket(Bucket=bucket_name)
# print(type(response))
# print(response)

# 上传文件
# with open('img/fj2.jpg', 'rb') as fp:
#     response = client.put_object(
#         Bucket=bb_bucket,
#         Body=fp,
#         Key="test02/brcue_fj1-02.jpg",
#         StorageClass="STANDARD",
#         EnableMD5=False
#     )
# print(type(response))
# print(response["ETag"])
# # a8952e69fb70756e504b3b7d26420a66
# print(response)

# 查询对象
response = client.list_objects(
    Bucket=bb_bucket,
    Prefix="test02"
)
print(type(response))
print(response["Contents"])
print(response)

# 下载对象
# response = client.get_object(
#     Bucket=bb_bucket,
#     Key="brcue_fj3.jpg",
# )
# response['Body'].get_stream_to_file("test02.jpg")

# 查询ACL
# response = client.get_bucket_acl(
#     Bucket=bb_bucket
# )
# print(type(response))
# print(response)

# 查询自定义域名
# res = client.get_bucket_domain(
#     Bucket=bb_bucket
# )
# print(res)

# def upload_file(file_path):
#     file_ext = file_path.rsplit(".",1)[1]
#     new_name = str(ObjectId())+"."+file_ext
#     key = dir_name + "/" + new_name
#     img_url = img_base_url + key
#     with open(file_path,"rb") as fp:
#         try:
#             res = client.put_object(
#                 Bucket=bb_bucket,
#                 Body=fp,
#                 Key=key,
#                 StorageClass="STANDARD",
#                 EnableMD5=False
#             )
#             print(res)
#             return img_url
#         except:
#             return None
#
#
# if __name__ == "__main__":
#
#     url = upload_file("img/rw.jpg")
#     if url:
#         print(url)
#     else:
#         print("上传失败")