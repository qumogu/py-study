# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: oss01.py
@datetime: 2020/6/22 12:41
"""
from __future__ import unicode_literals
import sys
import os
import oss2
from itertools import islice
from bson import ObjectId

# accesskey_id = os.getenv("OSS_ACCESS_KEY_ID","LTAI4GEdXsBJHp3n8qcN5ody")
# accesskey_secret = os.getenv("OSS_ACCESS_KEY_SECRET", "yeimJ1jI6IlMJVVDSkDNKXfkmCoT1e")

accesskey_id = os.getenv("OSS_ACCESS_KEY_ID","LTAI4G6ojgSRNEpbZ5oca1YF")
accesskey_secret = os.getenv("OSS_ACCESS_KEY_SECRET", "qBshQqL8jFsCG5jjsXQKZUlPFl2DBy")
bucket_name = os.getenv("OSS_BUCKET", "qumogu")
endpoint = os.getenv("OSS_ENDPOINT", "oss-cn-shenzhen.aliyuncs.com")
url_domin = "http://img.qumogu.com"

auth = oss2.Auth(accesskey_id,accesskey_secret)
client = oss2.Bucket(auth, endpoint, bucket_name)

upload_type = ["jpg","png","gif","bmp","pdf"]

def put_file(file_path):
    ext = file_path.rsplit(".",1)[-1]
    if ext not in upload_type:
        print("不支持此类文件上传")
    path = "test02/" + str(ObjectId()) + "." + ext
    res = client.put_object_from_file(path, file_path)
    # print(res.resp)
    print(res.headers)
    print(res.status)
    # print(res.request_id)
    # print(res.crc)
    # print(res.etag)

def get_file():
    pass

def delete_file(path):
    if not is_exists(path):
        return "文件不存在"
    res = client.delete_object(path)
    if res.status == 200:
        return "删除成功"
    else:
        return "删除失败"

def delete_small(filesize):
    delete_keys = []
    for b in oss2.ObjectIterator(client, prefix="test02"):
        if not b.is_prefix(): # 去除目录
            if b.size < filesize:
                client.delete_object(b.key)
                delete_keys.append(b.key)
    return delete_keys


def is_exists(path):
    return client.object_exists(path)

def get_list():
    for b in islice(oss2.ObjectIterator(client),10):
        print(b)
        print(b.key+" : "+ str(b.size))

# test02/5ef1a8a3e3f4e60cd0de614a.jpg
# test02/5ef1a961e3f4e62bf0232d63.jpg
# test02/5ef1a9b2e3f4e60adcc60ec1.jpg
# test02/5ef1a9efe3f4e67188197c31.jpg

if __name__ == "__main__":
    print("程序启动")
    put_file("img/fj3.jpg")
    get_list()

    # 判断文件是否存在
    # if is_exists("test02/5ef1a8a3e3f4e60cd0de614a.jpg"):
    #     print("文件存在")
    # else:
    #     print("文件不存在")

    # 删除单个文件
    # res = delete_file("test02/5ef1a9b2e3f4e60adcc60ec1.jpg")
    # print(res)

    # 删除小文件,小于1000 btyes
    # res = delete_small(1000)
    # print(res)
    #
    # get_list()
