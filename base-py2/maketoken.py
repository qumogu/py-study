# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: maketoken.py
@datetime: 2020/7/10 18:03
"""
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer

from bson import ObjectId
import time_study


def generate_confirmation_token(secret_key, key, data, expiration=86400):
    s = Serializer(secret_key, expires_in=expiration)
    return s.dumps({key: data})


def confirm_token(secret_key, key, data, token):
    s = Serializer(secret_key)
    try:
        d = s.loads(token)
    except Exception as e:
        print e
        return False
    if d.get(key) != data:
        return False

    return True

if __name__ == "__main__":
    token = generate_confirmation_token("test02","username","jack")
    print(token)

    res = confirm_token("test02","username", "jack", token)
    print(res)

    print(ObjectId())
    print(time_study.time())