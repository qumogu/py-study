# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: class_func01.py
@datetime: 2020/8/20 17:44
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')


from py_study.test01.class_func02 import Test1

class Test2(Test1):
    pass

def test_func():
    print("000022222")


if __name__ == '__main__':
    t = Test2()
    t.get_msg()