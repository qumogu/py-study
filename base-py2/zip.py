# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: zip.py
@datetime: 2020/6/18 17:30
"""
from __future__ import unicode_literals
import sys
import os

reload(sys)
sys.setdefaultencoding('utf-8')

a = ("a","c","e")
b = (1,5,9)
c = ("*","-","_")

for i in zip(a,c,b):
    print(i)

print(os.getenv('TEST'))