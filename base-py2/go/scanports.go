package main

import (
	"fmt"
	"net"
	"sort"
	"time"
)

var openports []int

func worker(ports chan int, result chan int){	
	ip := "dev.qumogu.com"
	for port := range ports {
		addr := fmt.Sprintf("%s:%d", ip, port)
		//fmt.Println("ip端口：", addr)
		conn, err := net.Dial("tcp", addr)
		if err != nil{
			//fmt.Printf("端口:%d，无法连通！\n", port)
			result <- 0
		}else{
			conn.Close()
			result <- port
		}
		
	}
}

func main(){
	fmt.Println("hello world")
	start := time.Now()
	ports := make(chan int ,1000)
	result := make(chan int)
	
	for i:=0;i<cap(ports);i++{
		go worker(ports, result)
	}
	go func(){
		for i:=20;i<10000;i++{
			ports <- i
		}
	}()

	for i:=20;i<10000;i++{
		port := <- result
		if port > 0 {
			openports = append(openports, port)
		}
	}
	close(ports)
	close(result)
	sort.Ints(openports)
	usetime := time.Since(start)
	fmt.Println("用时：", usetime)
	fmt.Println("扫描结束，开放端口为：", openports)

}