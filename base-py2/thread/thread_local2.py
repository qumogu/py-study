# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: thread_local.py
@datetime: 2020/4/26 14:01
"""

import time_study
from threading import Thread, local

class foo(object):
    pass

class fxx(local):
    pass

a = foo()

b = fxx()

def add1(i):
    a.name = i
    time_study.sleep(1)
    print a.name

def add2(i):
    b.name = i
    time_study.sleep(1)
    print b.name

def exce1():
    for i in range(10):
        th = Thread(target=add1, args=(i,))
        th.start()

def exce2():
    for i in range(10):
        th = Thread(target=add2, args=(i,))
        th.start()

def main():
    #exce1()
    exce2()

if __name__ == '__main__':
    main()