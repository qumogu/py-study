# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: thread_test01.py
@datetime: 2020/6/10 18:37
"""
# from __future__ import unicode_literals
# import sys
#
# reload(sys)
# sys.setdefaultencoding('utf-8')

import threading
import time_study


def action(arg):
    time_study.sleep(1)
    # 获取当前线程
    print("Current thread: {}".format(threading.currentThread().getName()))
    print("Threading No.:{}".format(arg))


for i in range(3):
    # 新建线程
    t = threading.Thread(target=action, name="xdq thread {}".format(i), args=(i, ))
    # 获取线程状态
    daemon_t = t.isDaemon()
    print("Daemon value: {}".format(daemon_t))
    # 开启线程

    t.daemon = True
    t.start()
    # 获取线程状态
    daemon_t = t.isDaemon()
    print("Daemon value: {}".format(daemon_t))
    # 获取活动线程列表
    enu = threading.enumerate()
    # 获取活动线程数量
    count = threading.active_count()
    print("Threading counts: {}".format(count))
    print("Enumerate thread: {}".format(len(enu)))
# 主线程
print("Main thread end!")