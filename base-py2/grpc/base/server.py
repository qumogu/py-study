# -*- coding: utf-8 -*-
"""
@author: Bruce
@contact: lanmin@itssoft.net
@file: base.py
@datetime: 2021/1/13 14:28
"""
from __future__ import unicode_literals

import json
import pickle
import urllib
import pymongo
import reco_pb2
import reco_pb2_grpc
import grpc
import time
from datetime import datetime, date
from concurrent.futures import ThreadPoolExecutor
from bson import ObjectId

def getReport(rid):
    mg_uri = "mongodb://{}:{}@192.168.0.243:24002/admin".format(urllib.quote("base-py2"), urllib.quote("Nash1234@"))
    mg_client = pymongo.MongoClient(mg_uri, connect=True)
    doc = mg_client["tuxi2"].report

    report = doc.find_one({"_id": ObjectId(rid)})
    if report:
        return report
    else:
        return None


class MongoDocEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        if isinstance(o, datetime):
            return o.strftime('%Y-%m-%d %H:%M:%S')
        elif isinstance(o, date):
            return o.strftime('%Y-%m-%d')
        else:
            return json.JSONEncoder.default(self, o)


class UserRecommendService(reco_pb2_grpc.UserRecommandServicer):

    def user_recommend(self, request, context):
        user_id = request.user_id
        channel_id = request.channel_id
        article_num = request.article_num
        time_stamp = request.time_stamp
        print(user_id, channel_id, time_stamp)
        response = reco_pb2.ArticleResponse()
        response.exposure = "exposure param"
        response.time_stamp = int(round(time.time()*1000))
        recommends = []
        for i in xrange(article_num):
            article = reco_pb2.Article()
            article.track.click = 'click param {}'.format(i+1)
            article.track.collect = 'collect param {}'.format(i + 1)
            article.track.share = 'share param {}'.format(i + 1)
            article.track.read = 'read param {}'.format(i + 1)
            article.article_id = i + 1
            recommends.append(article)
        response.recommends.extend(recommends)

        return response

    def hello_rpc(self, request, context):

        username = request.username
        print(time.strftime("%Y-%m-%dT%H:%M:%S",time.localtime(time.time())), "hello_rpc收到请求：", username)
        report =None
        if ObjectId.is_valid(username):
            report = getReport(username)

        if report:
            ret = {
                "status": 1,
                "data": report
            }
        else:
            ret = {
                "status":0
            }
        response = reco_pb2.HelloResponse()
        response.msg = json.dumps(ret, cls=MongoDocEncoder)

        return response

    def get_report(self, request, context):

        rid = request.rid
        print(time.strftime("%Y-%m-%dT%H:%M:%S",time.localtime(time.time())), "get_report receive ：", rid)
        report = None
        if ObjectId.is_valid(rid):
            report = getReport(rid)

        if report:
            ret = {
                "status": 1,
                "data": report
            }
        else:
            ret = {
                "status":0
            }
        response = reco_pb2.ReportResponse()
        response.report = pickle.dumps(ret)

        return response

def serve():
    print("-----服务启动-----")
    server = grpc.server(ThreadPoolExecutor(max_workers=10))
    reco_pb2_grpc.add_UserRecommandServicer_to_server(UserRecommendService(), server)
    server.add_insecure_port('127.0.0.1:8888')
    server.start()
    while True:
        time.sleep(10)
        print("-----休眠10s-----")


if __name__ == '__main__':
    serve()