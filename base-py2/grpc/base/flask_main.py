# -*- coding: utf-8 -*-
"""
@author: Bruce
@contact: lanmin@itssoft.net
@file: flask_main.py
@datetime: 2021/1/13 16:59
"""
from __future__ import unicode_literals

import json
import pickle
import time

from flask import Flask, current_app, request
import grpc
import reco_pb2
import reco_pb2_grpc
from datetime import datetime, date
from bson import ObjectId


app = Flask(__name__)

app.rpc_channel = grpc.insecure_channel("127.0.0.1:8888")

class MongoDocEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        if isinstance(o, datetime):
            return o.strftime('%Y-%m-%d %H:%M:%S')
        elif isinstance(o, date):
            return o.strftime('%Y-%m-%d')
        else:
            return json.JSONEncoder.default(self, o)

def feed_articles(stub,user_id,channel_id, num, int_now):
    user_request = reco_pb2.UserRequest()
    user_request.user_id = user_id
    user_request.channel_id = channel_id
    user_request.article_num = num
    user_request.time_stamp = int_now
    ret = stub.user_recommend(user_request)
    print('ret{}'.format(ret))
    return ret.recommends

@app.route("/index")
def index():
    return "flask grpc"

@app.route("/reco")
def user_reco():
    article_num = request.args.get("num")
    if article_num:
        int_now = int(round(time.time() * 1000))
        stub = reco_pb2_grpc.UserRecommandStub(current_app.rpc_channel)
        feeds = feed_articles(stub, user_id="10001", channel_id=5, num=int(article_num), int_now=int_now)
        articles = []
        for feed in feeds:
            articles.append({
                "acticle_id": feed.article_id,
                "click":feed.track.click
            })
        return json.dumps(articles)
    else:
        return "没有数量"

@app.route("/hello")
def hello_grpc():
    username = request.args.get("username")
    if not username:
        username = "bruce"
    stub = reco_pb2_grpc.UserRecommandStub(current_app.rpc_channel)
    hello = reco_pb2.HelloRequest()
    hello.username = username
    ret = stub.hello_rpc(hello)
    msg = ret.msg
    data = json.loads(msg)
    if data.get("status") == 1:
        report = data.get("data")
        print(type(report.get("lastModifyTime")), report.get("lastModifyTime"))
        return "简报标题：{}".format(report.get("reportName"))
    else:
        return "没有简报信息"


@app.route("/report")
def report_grpc():
    rid = request.args.get("rid")
    if not rid:
        rid = "5ff2ceef9aba2700101496d5"
    r = reco_pb2.ReportRequest()
    r.rid = rid
    stub = reco_pb2_grpc.UserRecommandStub(current_app.rpc_channel)
    ret = stub.get_report(r)
    report = pickle.loads(ret.report)
    if report.get("status") == 1:
        report2 = report.get("data")
        print(type(report2.get("lastModifyTime")), report2.get("lastModifyTime"))
        return "简报标题：{}".format(report2.get("reportName"))
    else:
        return "没有简报信息"



if __name__ == '__main__':
    app.run("0.0.0.0", 5000, debug=True)