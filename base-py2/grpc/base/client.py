# -*- coding: utf-8 -*-
"""
@author: Bruce
@contact: lanmin@itssoft.net
@file: client.py
@datetime: 2021/1/13 15:35
"""
from __future__ import unicode_literals

import time
import grpc
import reco_pb2
import reco_pb2_grpc

def feed_articles(stub):
    user_request = reco_pb2.UserRequest()
    user_request.user_id = "10001"
    user_request.channel_id = 5
    user_request.article_num = 10
    user_request.time_stamp = int(round(time.time()*1000))
    ret = stub.user_recommend(user_request)
    print('ret{}'.format(ret))


def run():
    with grpc.insecure_channel('127.0.0.1:8888') as channel:
        stub = reco_pb2_grpc.UserRecommandStub(channel)
        feed_articles(stub)

if __name__ == '__main__':
    run()