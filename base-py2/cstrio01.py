# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: cstrio01.py
@datetime: 2020/6/28 18:01
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

from cStringIO import StringIO
#from StringIO import StringIO

if __name__ == "__main__":

    st = "hello world!"
    f = StringIO()
    seek_num = f.tell()
    print(seek_num)
    out = f.getvalue()
    print(out)
    f.write(st)
    out = f.getvalue()
    print(out)
    seek_num = f.tell()
    print(seek_num)
    st2 = "I'm Jack"
    f.write(st2)
    out = f.getvalue()
    print(out)
    f.seek(0)
    f.write(st2)
    out = f.getvalue()
    print(out)
    seek_num = f.tell()
    print(seek_num)