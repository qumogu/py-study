# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: pd_study.py
@datetime: 2020/6/19 10:13
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

import  pandas as pd

data = {'a':[None,1,2,3],'b':[4,None,None,6],'c':[1,2,1,2],'d':[7,7,9,2]}

df = pd.DataFrame(data)

print(df)
print(df.isnull().sum())

#去除nan的列
data_without_NaN = df.dropna(axis=1)
print(data_without_NaN)


df_new = df.copy()

clos_with_missing = (col for col in df_new.columns if df_new[col].isnull().any())

for col in clos_with_missing:
    print(df_new[col].isnull())
    df_new[col+ "_NaN"] = df_new[col].isnull()
