# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: var_func.py
@datetime: 2020/8/13 12:55
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

import datetime,time
import hashlib
import json
import requests
from jsonpath import jsonpath


def hp_signature(generate_rule, generate_key, params_dict):

    list_str = generate_rule.split("+")
    res_str = ""
    for s in list_str:
        res_str = res_str + str(params_dict[s])
    params_dict[generate_key] = hashlib.sha256(res_str).hexdigest().upper()

    return params_dict


def make_signature(generate_rule, generate_key, params_dict):

    list_str = generate_rule.split("+")
    params = []
    for s in list_str:
        params.append(str(params_dict[s]))
    params = sorted(params)
    str_params = "".join(params)
    params_dict[generate_key] = hashlib.sha256(str_params.encode("utf8")).hexdigest()

    return params_dict


api_generate_funcs = {
    "signature":make_signature,
    "hp_signature":hp_signature,
}

def init_param_values(param_values):
    if 'timestamp' in param_values:
        index = param_values.index('timestamp')
        param_values[index] = str(int(time.time()))
    return param_values


def transform_params(json_dict):

    if isinstance(json_dict, dict):
        generate_funcs = json_dict.get("generate_funcs")
        generate_rules = json_dict.get("generate_rules")
        generate_keys = json_dict.get("generate_keys")
        param_keys = json_dict.get("param_keys")
        param_values = json_dict.get("param_values")
        delete_keys = json_dict.get("delete_keys")
        params_dict = json_dict.get("params_dict")

        param_values = init_param_values(param_values)

        ret_dict = {}

        for i in range(len(param_keys)):
            ret_dict[param_keys[i]] = param_values[i]
        if generate_funcs:
            for i in range(len(generate_funcs)):
                ret_dict = api_generate_funcs[generate_funcs[i]](generate_rules[i], generate_keys[i], ret_dict)
        if params_dict:
            for key in params_dict:
                params_dict[key] = ret_dict[params_dict[key]]
            return params_dict
        else:
            if delete_keys:
                for key in delete_keys:
                    ret_dict.pop(key)

            return ret_dict
    else:
        return None


def main():

    header_dict = {
        "generate_funcs": ["hp_signature"],
        "generate_rules": ["timestamp+token+nonce+timestamp"],
        "generate_keys": ["signature"],
        "param_keys": ['timestamp', 'token', 'nonce', 'paasid'],
        "param_values": ['timestamp', 'kLxLJcqASI5leYHRJC8OfiqqW2QiLVII', '123456789abcdefg', 'zdxm'],
        "delete_key": [],
        "params_dict": {
            "x-tif-paasid": "paasid",
            "x-tif-timestamp": "timestamp",
            "x-tif-signature": "signature",
            "x-tif-nonce": "nonce",
        }

    }

    get_param_dict = {}

    post_param_dict = {}

    post_dict = {
        "query": {
            "pageNo": 1
        }
    }




    header_json = json.dumps(header_dict)
    get_param_json = json.dumps(get_param_dict)
    post_param_json = json.dumps(post_param_dict)
    post_json = json.dumps(post_dict)


    if header_json and header_json != "{}":
        headers = transform_params(header_json)
    else:
        headers = None

    #print(headers)

    if get_param_json:
        search_params = transform_params(get_param_json)
    else:
        search_params = None

    #print(search_params)


    if post_param_json:
        post_params = transform_params(post_param_json)
    else:
        post_params = None

    #print(post_params)

    if post_json:
        post_dict = json.loads(post_json)
    else:
        post_dict = None

    url = "http://tyrztest.hp.gov.cn/ebus/gzshpqsjfwpt/v1/service/gjgzzdxmcxjk"
    request_type = "post"
    post_data_type = "json"

    if request_type == "get":
        r = requests.get(url, search_params, headers=headers)
    elif request_type == "post":
        # print(search_params)
        if search_params:
            url = url + "?"
            for k, v in search_params.items():
                url = url + "{}={}".format(k, v) + "&"
        print(url)
        if post_data_type == "json":
            headers['Content-Type'] = "application/json;charset=UTF-8"
            print(post_dict)
            r = requests.post(url, json=post_dict, headers=headers)
        else:
            headers['Content-Type'] = "application/x-www-form-urlencoded;charset=UTF-8"
            print(post_params)
            r = requests.post(url, data=post_params, headers=headers)
    print(r.content)
    print(r.text)

    # metadata={
    #     "meta_keys":["dust_unstand_proj_cnt", "id", "noise_unstand_proj_cnt", "stat_time"],
    #     "data_keys":["$..data[*].DUST_UNSTAND_PROJ_CNT", "$..data[*].ID", "$..data[*].NOISE_UNSTAND_PROJ_CNT",
    #                  "$..data[*].STAT_TIME"],
    #     "meta_types":[0, 0, 0, 2],
    # }
    #
    # if r.status_code == 200:
    #     result = r.json()
    #     print(result)
    #     data = {}
    #     key_length = len(keys1)
    #     for i in range(key_length):
    #         data[keys1[i]] = jsonpath(result, keys2[i])
    #
    #     # print(data)
    #     print("***" * 15)
    #     db_data = []
    #     for i in range(len(data[keys1[0]])):
    #         d = {"version": "aaabbbcccddd"}
    #         for j in range(key_length):
    #
    #             d[keys1[j]] = data[keys1[j]][i]
    #
    #         db_data.append(d)
    #     print(db_data)

    #     try:
    #         res = api_test.insert_many(db_data)
    #         print("一共插入数据{}条".format(len(res.inserted_ids)))
    #     except Exception as e:
    #         print("db error")
    #         print(e)
    # else:
    #     print("error")
    #
    # return "ok"
if __name__ == '__main__':

    main()



