# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: var_func.py
@datetime: 2020/8/13 12:55
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

import datetime,time
import hashlib
import json
import requests
from jsonpath import jsonpath


def hp_signature_bak(make_rules, make_param_names, params_dict):

    for i in range(len(make_rules)):
        list_str = make_rules[i].split("+")
        # print(list_str)
        res_str = ""
        for s in list_str:
            res_str = res_str + str(params_dict[s])
        # print(param_dict)
        # print(make_params_names[i])
        params_dict[make_param_names[i]] = hashlib.sha256(res_str).hexdigest().upper()

    return params_dict

def hp_signature(generate_rule, generate_key, params_dict):

    list_str = generate_rule.split("+")
    res_str = ""
    for s in list_str:
        res_str = res_str + str(params_dict[s])
    params_dict[generate_key] = hashlib.sha256(res_str).hexdigest().upper()

    return params_dict

def make_signature(generate_rule, generate_key, params_dict):

    list_str = generate_rule.split("+")
    param_list = []
    for s in list_str:
        param_list.append(str(params_dict[s]))
    params = sorted(param_list)
    str_params = "".join(params)

    #return hashlib.sha256(str_params).hexdigest()
    params_dict[generate_key] = hashlib.sha256(str_params).hexdigest()

    return params_dict

api_generate_funcs = {
    "signature":make_signature,
    "hp_signature":hp_signature,
}

def transform_params_bak(param_json):

    json_dict = json.loads(param_json)
    dynamic_func = json_dict.get("dynamic_func")
    make_param_names = json_dict.get("make_param_names")
    make_rules = json_dict.get("make_rules")
    param_keys = json_dict.get("param_keys")
    param_values = json_dict.get("param_values")
    params_dict = json_dict.get("params_dict")
    delete_keys = json_dict.get("delete_keys")

    if 'timestamp' in param_values:
        index = param_values.index('timestamp')
        param_values[index] = str(int(time.time()))
    ret_dict = {}
    for i in range(len(param_keys)):
        ret_dict[param_keys[i]] = param_values[i]
    if dynamic_func:
        ret_dict = api_generate_funcs[dynamic_func](make_rules, make_param_names, ret_dict)
    if params_dict:
        for key in params_dict:
            params_dict[key] = ret_dict[params_dict[key]]
        return params_dict
    else:
        if delete_keys:
            for key in delete_keys:
                ret_dict.pop(key)

        return ret_dict

def transform_params(param_json):

    json_dict = json.loads(param_json)

    generate_funcs = json_dict.get("generate_funcs")
    generate_rules = json_dict.get("generate_rules")
    generate_keys = json_dict.get("generate_keys")
    param_keys = json_dict.get("param_keys")
    param_values = json_dict.get("param_values")
    delete_keys = json_dict.get("delete_keys")
    params_dict = json_dict.get("params_dict")


    if 'timestamp' in param_values:
        index = param_values.index('timestamp')
        param_values[index] = str(int(time.time()))
    ret_dict = {}
    for i in range(len(param_keys)):
        ret_dict[param_keys[i]] = param_values[i]
    if generate_funcs:
        for i in range(len(generate_funcs)):
            ret_dict = api_generate_funcs[i](generate_rules[i], generate_keys[i], ret_dict)
    if params_dict:
        for key in params_dict:
            params_dict[key] = ret_dict[params_dict[key]]
        return params_dict
    else:
        if delete_keys:
            for key in delete_keys:
                ret_dict.pop(key)

        return ret_dict


def main():
    headers = {
        "x-tif-paasid": "paasid",
        "x-tif-timestamp": "timestamp",
        "x-tif-signature": "signature",
        "x-tif-nonce": "nonce",
    }
    header_dict = {
        "generate_funcs": ["hp_signature"],
        "generate_rules": ["timestamp+token+nonce+timestamp"],
        "generate_keys": ["signature"],
        "param_keys": ['timestamp', 'token', 'nonce', 'paasid'],
        "param_values": ['timestamp', 'kLxLJcqASI5leYHRJC8OfiqqW2QiLVII', '123456789abcdefg', 'zdxm'],
        "delete_key": [],
        "params_dict": {
            "x-tif-paasid": "paasid",
            "x-tif-timestamp": "timestamp",
            "x-tif-signature": "signature",
            "x-tif-nonce": "nonce",
        }

    }

    header_dict_simple = {
        "dynamic_func": "hp_signature",
        "make_rule": "timestamp+token+nonce+timestamp",
        "make_param_name": "signature",
        "param_keys": ['timestamp', 'token', 'nonce', 'paasid'],
        "param_values": ['timestamp', 'kLxLJcqASI5leYHRJC8OfiqqW2QiLVII', '123456789abcdefg', 'zdxm'],
        "delete_keys": [],
        "params_dict": {
            "x-tif-paasid": "paasid",
            "x-tif-timestamp": "timestamp",
            "x-tif-signature": "signature",
            "x-tif-nonce": "nonce",
        }

    }

    get_param_dict = {
        "generate_funcs": "signature",
        "generate_rules": "timestamp+token+appid",
        "generate_keys": "signature",
        "param_keys": ['timestamp', 'token', 'appid', 'beginTime', 'endTime'],
        "param_values": ['timestamp', 'kLxLJcqASI5leYHRJC8OfiqqW2QiLVII', '123456789abcdefg', '20200801', '20200807'],
        "delete_keys": ['token', 'appid'],
        "params_dict": {},
    }

    get_param_dict_simple = {
        "dynamic_func": "signature",
        "make_rule": "timestamp+token+appid",
        "make_param_name": "signature",
        "param_keys": ['timestamp', 'token', 'appid', 'beginTime', 'endTime'],
        "param_values": ['timestamp', 'kLxLJcqASI5leYHRJC8OfiqqW2QiLVII', '123456789abcdefg', '20200801', '20200807'],
        "delete_keys": ['token', 'appid'],
        "params_dict": {},
    }

    post_param_dict = {
        "generate_funcs": None,
        "generate_rules": None,
        "generate_keys": None,
        "param_keys": ['id', 'beginTime', 'endTime'],
        "param_values": ['1111', '20200801', '20200807'],
        "delete_key": [],
        "params_dict": {}
    }

    post_param_dict_simple = {
        "dynamic_func": None,
        "make_rule": None,
        "make_param_name": None,
        "param_keys": ['id', 'beginTime', 'endTime'],
        "param_values": ['1111', '20200801', '20200807'],
        "delete_key": [],
        "params_dict": {}
    }

    post_dict = {
        "query": {
            "pageNo": 1
        }
    }


    request_type = "post"
    post_data_type = "json"
    # post_data_type = "x-www-form-urlencoded"

    header_json = json.dumps(header_dict)
    get_param_json = json.dumps(get_param_dict)
    post_param_json = json.dumps(post_param_dict)
    post_json = json.dumps(post_dict)



    # url = "http://serviceanalysis.szairport.com/bigdata/dm/huawei/v1/sortiesAnalysis/query"

    # keys1 = ["data_time", "flight_count", "landing_count"]
    # keys2 = ["$..result[*].data_time", "$..result[*].flight_count", "$..result[*].landing_count"]
    # keys3 = [2, 0, 0]

    url = "http://tyrztest.hp.gov.cn/ebus/gzshpqsjfwpt/v1/service/gjgzzdxmcxjk"
    #url = "http://serviceanalysis.szairport.com/api/test"

    keys1 = ["dust_unstand_proj_cnt", "id", "noise_unstand_proj_cnt", "stat_time"]
    keys2 = ["$..data[*].DUST_UNSTAND_PROJ_CNT", "$..data[*].ID", "$..data[*].NOISE_UNSTAND_PROJ_CNT",
             "$..data[*].STAT_TIME"]
    keys3 = [0, 0, 0, 2]

    if header_json:
        headers = transform_params(header_json)
    else:
        headers = {}

    print(headers)

    if get_param_json:
        search_params = transform_params(get_param_json)
    else:
        search_params = {}

    print(search_params)


    if post_param_json:
        post_params = transform_params(post_param_json)
    else:
        post_params = {}

    print(post_params)
    return "0"

    if post_json:
        post_dict = json.loads(post_json)
    else:
        post_dict = {}

    if request_type == "get":
        r = requests.get(url, search_params, headers=headers)
    elif request_type == "post":
        # print(search_params)
        if search_params:
            url = url + "?"
            for k, v in search_params.items():
                url = url + "{}={}".format(k, v) + "&"
        print(url)
        if post_data_type == "json":
            headers['Content-Type'] = "application/json;charset=UTF-8"
            print(post_dict)
            r = requests.post(url, json=post_dict, headers=headers)
        else:
            headers['Content-Type'] = "application/x-www-form-urlencoded;charset=UTF-8"
            print(post_params)
            r = requests.post(url, data=post_params, headers=headers)

    if r.status_code == 200:
        result = r.json()
        print(result)
        # data = {}
        # key_length = len(keys1)
        # for i in range(key_length):
        #     data[keys1[i]] = jsonpath(result, keys2[i])
        #
        # # print(data)
        # print("***" * 15)
        # db_data = []
        # for i in range(len(data[keys1[0]])):
        #     d = {"version": "aaabbbcccddd"}
        #     for j in range(key_length):
        #
        #         d[keys1[j]] = data[keys1[j]][i]
        #
        #     db_data.append(d)
        # print(db_data)

    #     try:
    #         res = api_test.insert_many(db_data)
    #         print("一共插入数据{}条".format(len(res.inserted_ids)))
    #     except Exception as e:
    #         print("db error")
    #         print(e)
    # else:
    #     print("error")
    #
    # return "ok"


def main1():
    make_rule = "timestamp+token+appid"
    make_param_name = "signature"
    params_dict = {
        "timestamp": "1597316540",
        "token": "kLxLJcqASI5leYHRJC8OfiqqW2QiLVII",
        "appid": "123456789abcdefg",
    }
    print(make_signature(make_rule, make_param_name, params_dict))

if __name__ == '__main__':

    main()



