# -*- coding: utf-8 -*- 
"""
@author: Bruce
@contact: lanmin@itssoft.net
@file: hp-api.py
@datetime: 2020/12/10 14:59
"""
from __future__ import unicode_literals

import datetime,time
import hashlib
import json
import requests
import jsonpath

def hp_signature(timestamp, nonce):
    token = "kLxLJcqASI5leYHRJC8OfiqqW2QiLVII"
    res_str = timestamp + token + nonce + timestamp
    return hashlib.sha256(res_str).hexdigest().upper()


if __name__ == '__main__':
    timestamp = str(int(time.time()))
    passid = "zdxm"
    nonce = "123456789abcdefg"
    signature = hp_signature(timestamp, nonce)
    print(signature)


    headers = {
        "x-tif-paasid": passid,
        "x-tif-timestamp": timestamp,
        "x-tif-signature": signature,
        "x-tif-nonce": nonce,
        'Content-Type': 'application/json;charset=UTF-8'
    }

    post_json = {
        "query": {
            "pageNo": 1
        }
    }
    url = "http://tyrztest.hp.gov.cn/ebus/gzshpqsjfwpt/v1/service/gjgzzdxmcxjk"

    # print("url: {}, json_dict: {}, headers:{}".format(url, post_json, headers))
    #
    # url= "http://tyrztest.hp.gov.cn/ebus/gzshpqsjfwpt/v1/service/gjgzzdxmcxjk"
    # json_dict = {
    #                 'query': {
    #                     'pageNo': 1
    #                 }
    #             }
    # headers ={'x-tif-signature': '6385B6324DAAA8FE2A91FC46EE1A5C7FAB6BE7D96CA24F58744B85B8BD5A8535',
    #           'x-tif-nonce': '123456789abcdefg',
    #           'Content-Type': 'application/json;charset=UTF-8',
    #           'x-tif-paasid': 'zdxm',
    #           'x-tif-timestamp': '1607585881'}
    #
    # # sys.exit()

    # r = requests.post(url, json=post_json, headers=headers)
    # jsondata = r.json()
    jsondata = {
        "errcode": 0,
        "errmsg": "",
        "data": {
            "pageNo": 1,
            "pageSize": 5000,
            "data": [
                {
                    "DUST_UNSTAND_PROJ_CNT": "0",
                    "DUST_UNSTAND_PROJ_PERCENT": "0.0",
                    "ID": "69541",
                    "NOISE_UNSTAND_PROJ_CNT": "7",
                    "NOISE_UNSTAND_PROJ_PERCENT": "0.0287",
                    "PROJECT_CNT": "244",
                    "STAT_TIME": "2020-12-10 15:26:24"
                }
            ],
            "metadata": {}
        }
    }
    # print(jsondata)
    data = jsonpath.jsonpath(jsondata, "$.data.data[*].PROJECT_CNT")

    mid_dict = {
              'noise_unstand_proj_cnt': ['7'],
              'dust_unstand_proj_cnt': ['0'],
              'stat_time': ['2020-12-10 16:36:24'],
              'project_cnt': False,
              'id': ['69576']
        }

