# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: py_path.py
@datetime: 2020/8/13 10:16
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')


import jsonpath, json

from collections import OrderedDict
def main():
    d = {
        "error_code": 0,
        "stu_info": [
            {
                "id": 2059,
                "name": "小白",
                "sex": "男",
                "age": 28,
                "addr": "河南省济源市北海大道32号",
                "grade": "天蝎座",
                "phone": "18378309272",
                "gold": 10896,
                "info":{
                    "card":434345432,
                    "bank_name":'中国银行'
                }

            },
            {
                "id": 2067,
                "name": "小黑",
                "sex": "男",
                "age": 32,
                "addr": "河南省济源市南海大道15号",
                "grade": "水瓶座",
                "phone": "12345678915",
                "gold": 100
            }
        ]
    }
    test_d = {"test02":"test02","data":d}
    str_d = json.dumps(d)

    keys1= ["name","age"]
    keys2= ["$.stu_info[*].name","$.stu_info[*].age"]

    data = {}
    key_length = len(keys1)
    for i in range(key_length):
        data[keys1[i]] = jsonpath.jsonpath(d,keys2[i])

    print(data)

    students = jsonpath.jsonpath(d,'$.stu_info[*].name')
    print(students)

    # for stu in students:
    #     stu

if __name__ == '__main__':
    main()