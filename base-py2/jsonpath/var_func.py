# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: var_func.py
@datetime: 2020/8/13 12:55
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')


def test_multi(num,age):
    ret = 0
    for i in range(3):
        print(i*num)
        ret = ret + i*num

    ret = ret+age

    return ret


def test_add(num,age):
    ret = 0
    for i in range(3):
        print(i + num)
        ret = ret + i + num
    ret = ret + age
    return ret



if __name__ == '__main__':
    b= {"add":test_add,"multi":test_multi}
    a = "add"
    # if a == "test_add":
    #     c = test_add(10)
    c = b[a](2,100)
    print("ok %d" % c)
