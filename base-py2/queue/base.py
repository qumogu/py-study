# -*- coding: utf-8 -*-
"""
@author: Bruce
@contact: lanmin@itssoft.net
@file: base.py
@datetime: 2021/2/22 15:44
"""
from __future__ import unicode_literals

from Queue import PriorityQueue
from test import TestB
import time,random

class TestA(object):

    def __init__(self, name, queue):
        self.name = name
        self.queue = queue

    def getMsg(self):
        if self.queue.empty():
            return None
        print(self.queue.qsize())
        msg = self.queue.get()
        print(msg)
        # print(testmsg)
        self.mock(1)
        self.queue.task_done()
        return msg

    def mock(self, second):
        time.sleep(second)
        print("休眠了{}秒".format(second))

if __name__ == '__main__':
    testmsg = "测试消息"
    q = PriorityQueue()
    for i in range(0,5):
        level = random.randint(0,3)
        msg = (level, "this is the {} msg".format(level))
        print(msg)
        q.put(msg)
    t = TestA("testA", q)
    print("-----")
    while True:
        # if t.queue.empty():
        #     print("结束")
        #     break
        msg = t.getMsg()
        # q.task_done()
        if not msg:
            print("结束")
            break
