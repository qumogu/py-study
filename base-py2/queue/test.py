# -*- coding: utf-8 -*-
"""
@author: Bruce
@contact: lanmin@itssoft.net
@file: test.py
@datetime: 2021/2/22 15:44
"""
from __future__ import unicode_literals
import time

class TestB(object):

    def __init__(self, name, queue):
        self.name = name
        self.queue = queue

    def getMsg(self):
        msg = self.queue.get()
        print(msg)
        self.queue.task_done()
        # print(testmsg)
        self.mock(1)

        return msg

    def mock(self, second):

        time.sleep(second)
        print("休眠了{}秒".format(second))