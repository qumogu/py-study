# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: base64_study.py
@datetime: 2020/6/28 15:31
"""
#from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

from StringIO import StringIO
import base64

def study_base(st):
    print(st)
    f = StringIO()
    out1 = StringIO()
    out2 = StringIO()
    f.write(st)
    print(f.getvalue())
    print(f.read())
    f.seek(0)
    print(f.read())
    print(f.getvalue())
    base64.encode(f, out1)
    print(out1.getvalue())
    out1.seek(0)
    base64.decode(out1, out2)
    print(out2.getvalue())


def study_b64(st):
    print(st)
    # bst = st.encode("utf8")
    # print(bst)
    res = base64.b64encode(st)
    print(res)
    # print(res.decode("utf8"))
    res = base64.b64decode(res)
    print(res)
    #print(res.decode("utf8"))


if __name__ == "__main__":

    st = u"hello world! 你好，世界"
    #@print(st.encode("utf8"))
    study_base(st)
    print("*"*50)
    study_b64(st)
