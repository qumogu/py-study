# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: base64_study.py
@datetime: 2020/6/28 15:31
"""
from __future__ import unicode_literals
import sys

from StringIO import StringIO
import base64

def study_base(st):
    print(st)
    f = StringIO()
    out1 = StringIO()
    out2 = StringIO()
    f.write(st)
    f.seek(0)
    base64.encode(f, out1)
    print(out1.getvalue())
    out1.seek(0)
    base64.decode(out1, out2)
    print(out2.getvalue())

def study_b64(st):

    res = base64.b64encode(st)
    print(res)
    print(type(res))

    res = base64.b64decode(res)
    print(res)
    print(type(res))



if __name__ == "__main__":

    st = "hello world!sdfsdfasdfwerqwtrfagsdfgasfdas"


    study_b64(st)
