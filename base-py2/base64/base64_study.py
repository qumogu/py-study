# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: base64_study.py
@datetime: 2020/6/28 15:31
"""
from __future__ import unicode_literals
import sys

from StringIO import StringIO
import base64
import hashlib

def study_base(st):
    print(st)
    f = StringIO()
    out1 = StringIO()
    out2 = StringIO()
    f.write(st)
    f.seek(0)
    base64.encode(f, out1)
    print(out1.getvalue())
    out1.seek(0)
    base64.decode(out1, out2)
    print(out2.getvalue())

def study_b64(st):
    print(st)
    bst = st.encode("utf8")
    print(bst)
    res = base64.b64encode(bst)
    print(res)
    print(res.decode("utf8"))
    res = base64.b64decode(res)
    print(res)
    print(res.decode("utf8"))


def study_md5():
    word = "hello world"
    m1 = hashlib.sha256(word)
    print(m1.hexdigest())
    m2 = hashlib.md5(word)

    print(m2.hexdigest())


if __name__ == "__main__":

    # st = "hello world!"
    # print(st.encode("utf8"))
    # study_base(st)
    # print("*"*50)
    # study_b64(st)
    study_md5()
    # study_md5()
