# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: base64_study.py
@datetime: 2020/6/28 15:31
"""

from io import StringIO, BytesIO

import base64

import cStringIO

def study_base(st):
    #st = st.encode("utf8")
    print(st)
    f1 = StringIO()
    f2 = BytesIO()
    # out1 = BytesIO()
    # out2 = BytesIO()
    out1 = StringIO()
    out2 = StringIO()
    f1.write(st)
    f1.seek(0)
    out = f1.getvalue()
    print(out)
    f2.write(st.encode("utf8"))
    f2.seek(0)
    out = f2.getvalue()
    print(out)
    # base64.encode(f1, out1)
    # print(out1.getvalue())
    # out1.seek(0)
    # base64.decode(out1, out2)
    # print(out2.getvalue().decode("utf8"))

def study_b64(st):
    print(st)
    bst = st.encode("utf8")
    print(bst)
    res = base64.b64encode(bst)
    print(res)
    print(res.decode("utf8"))
    res = base64.b64decode(res)
    print(res)
    print(res.decode("utf8"))

def test_b64():
    st = "VsbG8gd2".encode("utf8")
    print(st)
    out = base64.b64decode(st)
    print(out)
    print(out.decode("utf8"))

def get_str(st):
    st_len = len(st)
    num = 4 - st_len % 4
    st = st + "="*num
    return st

if __name__ == "__main__":

    st = "hello world!3"
    #print(st.encode("utf8"))
    study_base(st)
    #print("*"*50)
    #study_b64(st)

    # test02 = get_str(st)
    # print(test02)
    # print(test02.replace("=",""))
