# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: base64_study.py
@datetime: 2020/6/28 15:31
"""



from io import BytesIO
from StringIO import StringIO

import base64

def str_base(st):
    print(st)
    f1 = StringIO()
    out1 = StringIO()
    out2 = StringIO()
    f1.write(st)
    f1.seek(0)
    out = f1.getvalue()
    print(out)
    base64.encode(f1, out1)
    print(out1.getvalue())
    out1.seek(0)
    base64.decode(out1, out2)
    print(out2.getvalue().decode("utf8"))

def bytes_base(st):
    st = st.encode("utf8")
    print(st)
    f2 = BytesIO()
    out1 = BytesIO()
    out2 = BytesIO()
    f2.write(st)
    f2.seek(0)
    out = f2.getvalue()
    print(out)
    out = f2.getvalue()
    print(out)
    base64.encode(f2, out1)
    print(out1.getvalue())
    out1.seek(0)
    base64.decode(out1, out2)
    print(out2.getvalue().decode("utf8"))


if __name__ == "__main__":

    st = "hello world!3"
    #print(st.encode("utf8"))
    str_base(st)
    print("--------------")
    bytes_base(st)
    #print("*"*50)
    #study_b64(st)

    # test02 = get_str(st)
    # print(test02)
    # print(test02.replace("=",""))
