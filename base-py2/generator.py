# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: generator.py
@datetime: 2020/6/18 16:10
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

a = [1,3,8,12,20,55]

b = ((i,"test_{}".format(i), i if i>15 else i*2) for i in a)

c = [(i,"test_{}".format(i), i if i>15 else i*2) for i in a]

print(b)
print(list(b))
print(c)
print(type(c))
print(b)
print(enumerate(c))

for index, item in enumerate(c):
    print(index)
    print(item)