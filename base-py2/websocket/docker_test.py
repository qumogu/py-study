# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: docker_test.py
@datetime: 2020/5/6 12:59
"""

from flask import Flask

app = Flask(__name__)

@app.route("/index")
@app.route("/")
def index():
    return "ok"

if __name__ == '__main__':
    app.run("0.0.0.0", 5050, debug=True)