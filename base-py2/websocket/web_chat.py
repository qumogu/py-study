# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: web_chat.py
@datetime: 2020/4/30 11:47
"""
from flask import Flask, request, render_template
from geventwebsocket.handler import WebSocketHandler
from geventwebsocket.websocket import WebSocket, WebSocketError
from gevent.pywsgi import WSGIServer

import datetime
import json

app = Flask(__name__)

user_socket_dict = {}
chat_sockets = []

@app.route("/")
def index():

    return render_template("chat.html")

@app.route("/chat/<username>")
def chat(username):
    #print(request.environ)
    user_socket = request.environ.get("wsgi.websocket")



    if not user_socket:
        return "请以websocket方式连接"

    user_socket_dict[username] = user_socket

    #print(user_socket_dict)

    while True:
        user_msg = user_socket.receive()
        print(user_msg)
        user_socket.send("{}说：{}".format(username, user_msg))

@app.route("/chat2")
def chat2():
    user_socket = request.environ.get("wsgi.websocket")
    if user_socket:
        chat_sockets.append(user_socket)

    while True:
        user_msg = user_socket.receive()
        chat_msg = "{}：{}".format(str(datetime.time()), user_msg)
        print(chat_msg)
        for i in chat_sockets:
            if i == user_socket:
                continue
            try:
                i.send(chat_msg)
            except:
                continue

@app.route("/test02")
def test():
    request.get_json()


if __name__ == '__main__':
    h_server = WSGIServer(("0.0.0.0", 5000), app, handler_class=WebSocketHandler)
    h_server.serve_forever()