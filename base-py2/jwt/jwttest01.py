# -*- coding: utf-8 -*-
"""
@author: Bruce
@contact: lanmin@itssoft.net
@file: jwttest01.py
@datetime: 2021/1/4 12:33
"""
from __future__ import unicode_literals

import jwt

JWTSecret = "dkjaHHFI&$%#;23dsfj"

def gen_token(playload, expiry, secret):
    _playload = {'exp':expiry}
    _playload.update(playload)
    token = jwt.encode(_playload, secret, algorithm='HS256')
    return token.decode()


def verify_token(token, secret):
    try:
        playload = jwt.decode(token, secret, algorithms=['HS256'])
    except:
        print("token解析失败，过期或者错误")
        playload =None

    return playload

if __name__ == '__main__':
    print("ok")

    token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImphY2siLCJleHAiOjE2MDk3NDE5NDcsImlzcyI6InF1bW9ndS5jb20ifQ.ctnHgMWfjCIE3dvfRGbOxlL3LR9qTwXhTPm6skgjA-M"
    p = verify_token(token, JWTSecret)
    print(p.get("username"))
    print(p)