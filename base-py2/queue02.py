# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: queue01.py
@datetime: 2020/6/19 18:13
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

from queue import Queue
from threading import Thread
import time_study
import random

q = Queue(5) # 初始化队列最大值

def put():
    for i in range(20):
        try:
            q.put(i, block=True, timeout=5)
            print("队列插入值：%d" % i)
        except Exception as e:
            print("队列插[%d]值超时" % i)

def get():
    for i in range(20):
        try:
            value = q.get(block=True, timeout=3)
            print("从队列中取出值%d" % value)
            q.task_done()
            time_study.sleep(random.randint(1, 6))
        except Exception as e:
            print("队列取值[%d]超时" % i)

t_p = Thread(target=put,args=())
t_p.start()
t_g = Thread(target=get, args=())
t_g.start()
t_p.join()
t_g.join()
print("执行完成")