# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: test_objectid.py
@datetime: 2020/3/31 13:11
"""

from bson import ObjectId


def main():
    oid = ObjectId()
    print(oid)
    print(ObjectId('foo-bar-quux'))

    content = "lorem"
    or_list = [{item: {"$regex": content, "$options": 'i'}} for item in ["email", "userName"]]
    search_info = {"$and": [{"$or": [{"user_status": True}, {"user_status": {"$exists": False}}]},
                            {"$or": or_list}]}

    print(search_info)
    my_disc ={'$and': [{'$or': [{'user_status': True}, {'user_status': {'$exists': False}}]},
                       {'$or': [{'email': {'$options': 'i', '$regex': 'lorem'}}, {'userName': {'$options': 'i', '$regex': 'lorem'}}]}
                       ]
              }


if __name__ == '__main__':
    main()