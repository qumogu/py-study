# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: bp_test.py
@datetime: 2020/3/30 17:23
"""

from flask import Flask
from users import users

app = Flask(__name__)
app.register_blueprint(users, url_prefix="/test02/users")

@app.route("/")
@app.route("/index")
def index():
    return "This is First Project"

if __name__ == '__main__':
    app.run(debug=True)