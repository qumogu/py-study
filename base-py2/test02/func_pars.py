# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: func_pars.py
@datetime: 2020/4/14 13:08
"""

class Test:
    def __init__(self, name, age, height=170, desc=None):
        self.name = name
        self.age = age
        self.height = height
        if desc:
            self.desc = desc
        else:
            self.desc="这家伙很懒，还没有填写介绍哦！"

    def introduce_self(self):
        print self.name + " 说: " + self.desc

    def get_height(self):
        print self.name + " 身高: " + str(self.height)



def main():
    t1 = Test('jack', 18, 150)
    t2 = Test("lily", 20,"dddd")
    t3 = Test(name="leo", desc="I'm very strong", age="45")
    t1.introduce_self()
    t2.introduce_self()
    t3.introduce_self()
    t1.get_height()
    t2.get_height()
    t3.get_height()



if __name__ == '__main__':
    main()