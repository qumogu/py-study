# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: log_test.py
@datetime: 2020/3/31 11:56
"""


import logging
import sys

reload(sys)
sys.setdefaultencoding("utf8")
# bruce.db-tmp
# from app.tools.log import logger
# logger.warning("*"*50)
# logger.warning(sql)


logging.basicConfig(level=logging.INFO, filename='pylog.txt')


def log_info():
    return "this is testing"


#logging.info(log_test())

class TestLog(object):

    PNAME = "LogTest"

    def __init__(self, name, age):
        self.name = name
        self.age = age


    @classmethod
    def get_clslog(cls):
        logging.info("Class:"+cls.PNAME+log_info())

    @staticmethod
    def get_cls_msg():
        logging.info(("*" * 60)+"\n get class static msg")

        return "static msg"

    def write_log(self):
        logging.info("{name}今年的年龄是{age}".format(name=self.name,age=self.age))


def main():
    tl = TestLog("jack", 28)
    msg = TestLog.get_cls_msg()
    print(msg)
    TestLog.get_clslog()
    print("-"*30)
    tl.write_log()
    print("t1写日志")



if __name__ == '__main__':
    main()
