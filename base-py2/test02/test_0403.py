# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: test_0403.py
@datetime: 2020/4/3 11:10
"""


def get_data(table_name, offset, limit, column_name_list, all=True):
    """获取数据"""
    table_name = table_name

    column_name = ','.join(['`{}`'.format(i) for i in column_name_list[0]])

    if isinstance(all, dict):
        value = all['value']
        update_key = all['update_key']
        sql = " select {} from `{}`  where {} > '{}' limit {}  offset {}".format(column_name,
                                                                                 table_name,
                                                                                 update_key,
                                                                                 value,
                                                                                 limit,
                                                                                 (int(offset) - 1) * int(limit),

                                                                                 )
    else:
        sql = " select {} from `{}` limit {}  offset {} ".format(column_name,
                                                                 table_name,
                                                                 limit,
                                                                 (int(offset) - 1) * int(limit))

    return sql
def get_test(column_name_list):
    column_name = ['`{}`'.format(i) for i in column_name_list]
    column_name = ','.join(column_name)
    return column_name

def main():
    table_name = 'users'
    offset = 10
    limit = 10
    column_name_list = ['username','password']
    all ={'value':'jack',
          'update_key':'username'}

    sql = get_data(table_name, offset, limit, column_name_list, all)
    #print (sql)
    sql = get_test(column_name_list)
    print (sql)

if __name__ == '__main__':
    main()