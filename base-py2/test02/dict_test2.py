# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: dict_test3.py
@datetime: 2020/5/21 18:49
"""
import re
a ={   "update_time": 100,
       "update_status": 2,
       "x_limit":{"xl": 5},
       "ids":  [{"_id":"5ec61c3d47c671000f738e9a", "$and":{"$not":"a"}},{"_id":"11111","name":{"$not":"jack"}}, {"_id":"222"}]
}


print(a)


for conditions in a.get('ids'):
    if '$and' in conditions.keys():
        print("11111111111111")

        for conditions_v in conditions.values():
            if isinstance(conditions_v, dict):
                print(conditions_v)
                if conditions_v.keys()[0] == '$not':
                    conditions_v['$not'] = re.compile(conditions_v['$not'])

                    print(conditions_v['$not'])
    else:
        print("2222222222222")
        # for and_condition in conditions['$and']:
        #
        #     for condition_v in and_condition.values():
        #         if isinstance(condition_v, dict):
        #             if condition_v.keys()[0] == '$not':
        #                 condition_v['$not'] = re.compile(condition_v['$not'])
        #                 print(condition_v['$not'])


print(a)

