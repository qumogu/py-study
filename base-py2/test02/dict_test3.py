# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: dict_test3.py
@datetime: 2020/5/21 18:49
"""
import re
a =  {

       "y_limit":  {
        "y":{
            "limit":3
        }
      },

       "update_time":  100,
       "update_status": 2,
       "x_limit":  {
            "xl":5
      },
       "ids":  [{"_id":"5ec61c3d47c671000f738e9a"}, {"_id":"11111"}, {"_id":"222"}]

    }

b = {"itemClass": "data", "version": 1,  "objectDetails": {"dataset": a}, "itemData": {}}
print(b)




#print(b["dataset"]["x_limit"]["xl"])

#change = {"dataset.y_limit.y.limit": 1000, "dataset.id.x.xl2": 5000, "data":[{"id","店"},{"11","罗湖店"}]}
change = {"data":[["id","店"],["11","罗湖店"]]}

for k, v in change.items():
    k_list = k.split('.')
    print(k_list)
    print(type(v))

    key = 'itemData' if k_list[0] == 'data' else 'objectDetails'
    print(key)
    connect = b[key]
    for key_i in range(len(k_list) - 1):
        try:
            print(k_list[key_i])
            print(connect.get(k_list[key_i]))

            if isinstance(connect[k_list[key_i]], dict):
                connect = connect[k_list[key_i]]

            else:
                connect[k_list[key_i]] = {}
                connect = connect[k_list[key_i]]
        except KeyError:
            connect[k_list[key_i]] = {}
            connect = connect[k_list[key_i]]
            print("----------error---------")

    print(k_list[-1])
    print(connect)
    connect[k_list[-1]] = v

print(b["itemData"]["data"][0])


