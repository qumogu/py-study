# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: list_test1.py
@datetime: 2020/5/25 18:48
"""

choice_columns = [{"columnType": 1, "originColumnIndex": 0, "originColumnName": "id", "replaceColumnIndex": 0,"replaceColumnName": "id"},
                  {"columnType": 1, "originColumnIndex": 1, "originColumnName": "订单", "replaceColumnIndex": 1,"replaceColumnName": "订单"},
                  {"columnType": 0, "originColumnIndex": 2, "originColumnName": "商品", "replaceColumnIndex": 2,"replaceColumnName": "商品"},
                  {"columnType": 0, "originColumnIndex": 3, "originColumnName": "销售店铺", "replaceColumnIndex": 4,"replaceColumnName": "销售店铺"},
                  {"columnType": 0, "originColumnIndex": 4, "originColumnName": "金额", "replaceColumnIndex": 5,"replaceColumnName": "金额"},
                  {"columnType": 0, "originColumnIndex": 5, "originColumnName": "销售员", "replaceColumnIndex": 3,"replaceColumnName": "销售员"}
                  ]


columns =[
        {
            "m_column_name" : "id",
            "m_column_id" : "5ec50dfa47c671000f738e84",
            "column_type" : 1,
            "column_id" : "5ec50dfa47c671000f738e84",
            "m_column_type" : "",
            "column_name" : "id"
        },
        {
            "m_column_name" : "订单",
            "m_column_id" : "5ec50dfa47c671000f738e85",
            "column_type" : 1,
            "column_id" : "5ec50dfa47c671000f738e85",
            "m_column_type" : "",
            "column_name" : "订单"
        },
        {
            "m_column_name" : "商品",
            "m_column_id" : "5ec50dfa47c671000f738e86",
            "column_type" : 0,
            "column_id" : "5ec50dfa47c671000f738e86",
            "m_column_type" : "",
            "column_name" : "商品"
        },
        {
            "m_column_name" : "销售店铺",
            "m_column_id" : "5ec50dfa47c671000f738e87",
            "column_type" : 0,
            "column_id" : "5ec50dfa47c671000f738e87",
            "m_column_type" : "",
            "column_name" : "销售店铺"
        }
    ]
new_columns_len = len(choice_columns)
columns_len = len(columns)
if new_columns_len > columns_len:
    new_columns = filter(lambda item: item["originColumnIndex"] >= columns_len, choice_columns)
    print(new_columns)
    new_columns.sort(key=lambda x: x["originColumnIndex"])
    print(new_columns)