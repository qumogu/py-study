# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: file_md5.py
@datetime: 2020/7/2 17:47
"""

from hashlib import md5
import os

def generate_file_md5value(fpath):
    '''以文件路径作为参数，返回对文件md5后的值
    '''
    m = md5()
    # 需要使用二进制格式读取文件内容
    a_file = open(fpath, 'rb')
    a_file.seek(128)
    m.update(a_file.read(8096))
    a_file.close()
    return m.hexdigest()

def generate_file_md5sumFile(fpath):
    fname = os.path.basename(fpath)
    fpath_md5 = "%s.md5" % fpath
    fout = open(fpath_md5, "w")
    fout.write("%s  %s\n" % (generate_file_md5value(fpath), fname.strip()))
    print "generate success, fpath:%s" % fpath_md5
    fout.flush()
    fout.close()

if __name__ == "__main__":
    fpath = "/data/python/wimages/uploads/imgs/2020-07-02/5efda99ca85f14c03df82bb2.jpg"
    # 测试一：以文件路径作为参数，获得md5后的字符串
    print generate_file_md5value(fpath)
    fpath = "/data/python/wimages/uploads/imgs/2020-07-02/5efd5e77fa6a413bad1fea9e.jpg"
    print generate_file_md5value(fpath)
    fpath = "/data/python/wimages/uploads/imgs/2020-07-02/5efd5e77fa6a413bad1fea9d.jpg"
    print generate_file_md5value(fpath)
    # 测试二：生成和linux命令：md5sum同样结果的.md5文件
    #generate_file_md5sumFile(fpath)
