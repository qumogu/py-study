# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: excel01.py
@datetime: 2020/9/8 11:16
"""
from __future__ import unicode_literals

import xlsxwriter
import time

def create_excel():

    t = time.time()
    timestamp = int(round(t * 1000))

    file_path = '{}.xlsx'.format(timestamp)
    # 以时间戳为名新建表格
    workbook = xlsxwriter.Workbook(file_path)
    sheet = workbook.add_worksheet('user_info')
    users = [
        {
            "id":"1001",
            "username":"jack",
            "school":"建安小学",
            "age":8,
            "class":"三（4）",
        },
        {
            "id": "1002",
            "username": "lily",
            "school": "建安小学",
            "age": 9,
            "class": "四（1）",
        },
        {
            "id": "1003",
            "username": "james",
            "school": "建安小学",
            "age": 7,
            "class": "二（2）",
        },
        {
            "id": "1004",
            "username": "tom",
            "school": "建安小学",
            "age": 10,
            "class": "五（6）",
        },
        {
            "id": "1005",
            "username": "jack dsafa dkjfakd ",
            "school": "建安小学",
            "age": 11,
            "class": "六（5）",
        },
    ]
    # 设置表格的行宽
    column_format = workbook.add_format({'font_color': 'red'})
    sheet.set_column(0, 0, 5)
    sheet.set_column(1, 1, 10)
    sheet.set_column(2, 2, 15)
    sheet.set_column(3, 3, 10)
    sheet.set_column(4, 4, 5)
    sheet.set_column(5, 5, 10)


    # 设置表格格式
    head_format = workbook.add_format({'align': 'centre', 'bold': True,"bg_color":'gray'})
    user_format = workbook.add_format({'align': 'centre'})
    click_format = workbook.add_format({'align': 'centre', 'font_color':'red'})

    # 写入首行信息
    head_line = [u'序号', u'ID', u'姓名', u'学校', u'年龄', u'班级']
    for col in xrange(0, len(head_line)):
        sheet.write(0, col, head_line[col], head_format)

    row_no = 1

    for item in users:
        try:
            user_info_list = list()
            user_info_list.append(row_no)
            user_info_list.append(item.get('id', None))
            user_info_list.append(item.get('username', None))
            user_info_list.append(item.get('school', None))
            user_info_list.append(item.get('age', None))
            user_info_list.append(item.get('class', None))

            for col in xrange(0, len(head_line)):
                if col == 4:
                    sheet.write(row_no, col, user_info_list[col],click_format)
                else:
                    sheet.write(row_no, col, user_info_list[col], user_format)
            row_no += 1
        except Exception as e:
            print(e)

    workbook.close()

if __name__ == '__main__':
    create_excel()