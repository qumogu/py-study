# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: lambda_study.py
@datetime: 2020/7/31 17:31
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')


def test(a):

    b=[("a",1),("a",1)]
    # b = [a(i) for i in b]
    b = tuple(b)
    print(b)
    # c = [("a",1),("b",1)]
    # d= ("a", a(b[0][1],c[0][1]))
    # print(d)

if __name__ == "__main__":

    a = lambda x, y: x+y
    b = lambda x:(x,1)
    test(a)
    # str_list = ["a","b","c"]
    # str_list = [b(i) for i in str_list]
    # print(str_list)

