# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: tmp2.py
@datetime: 2020/4/20 15:05
"""
# a = [{"name":"jack","serial":5},{"name":"lily","serial":2},{"name":"tom","serial":9},{"name":"leo","serial":5}]
#
# for i in a:
#     i["age"] = i["serial"]+10
#     i.pop("serial")
#
# print a
#---------------------------

# a = {"code":0,
#     "data":[{"name":"jack","serial":5},
#         {"name":"lily","serial":2},
#         {"name":"tom","serial":9},
#         {"name":"leo","serial":5}],
#      "msg":u"操作成功"
# }
#
# print a.get("code")


a = [{"grade":1,"mids":["dfdf", "babsdsd"]},
     {"grade":2,"mids":["xxxx", "babsdsd"]},
     {"grade":5,"mids":["xxxx", "babsdsd"]},
     {"grade":7,"mids":["xxxx", "babsdsd"]},
     {"grade":2,"mids":["xxxx", "babsdsd"]},
     {"grade":6,"mids":["xxxx", "babsdsd"]},
     {"grade":7,"mids":["xxxx", "babsdsd"]}]
grades = [i.get("grade") for i in a]
b = set(grades)

print(a)

print(b)