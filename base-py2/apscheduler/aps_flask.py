# -*- coding: utf-8 -*- 
"""
@author: Bruce
@contact: lanmin@itssoft.net
@file: aps_base.py
@datetime: 2020/11/10 16:44
"""
from __future__ import unicode_literals
from apscheduler.schedulers.blocking import BlockingScheduler

from apscheduler.jobstores.redis import RedisJobStore

from apscheduler.executors.pool import ProcessPoolExecutor, ThreadPoolExecutor
from datetime import datetime
import time
import logging

logging.basicConfig()

def test_task(name):
    print(name + ":"+datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    time.sleep(2)
    print(name+"------"+datetime.now().strftime("%Y-%m-%d %H:%M:%S"))


if __name__ == '__main__':
    jobstores = {
        'default': RedisJobStore(host="192.168.0.243",port=6379, password="Nash1234@",db=11),
    }
    executors = {
        'default': ThreadPoolExecutor(5),
    }

    job_defaults = {
        'coalesce': False,  # 默认情况下关闭新的作业
        'max_instances': 10  # 设置调度程序将同时运行的特定作业的最大实例数3
    }
    s1 = BlockingScheduler(jobstores=jobstores, executors=executors, job_defaults=job_defaults)
    # s1.remove_all_jobs(jobstore=jobstores)
    # print(s1.remove_job('jack',jobstore=jobstores))

    # job1 = s1.add_job(test_task, 'interval', seconds=5, args=['jack'], id='jack')
    # job2 = s1.add_job(test_task, 'interval', seconds=5, args=['lily'], id='lily')
    # job3 = s1.add_job(test_task, 'interval', seconds=5, args=['tom'], id='tom')
    # job4 = s1.add_job(test_task, 'interval', seconds=5, args=['may'], id='may')

    s1.start()
    # print("删除job1")
    # job1.remove()
