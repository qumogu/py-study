# -*- coding: utf-8 -*- 
"""
@author: Bruce
@contact: lanmin@itssoft.net
@file: aps_base.py
@datetime: 2020/11/10 16:44
"""
from __future__ import unicode_literals
from apscheduler.schedulers.blocking import BlockingScheduler
from apscheduler.jobstores.redis import RedisJobStore
from datetime import datetime
import time


def test_task():
    print(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    time.sleep(2)


if __name__ == '__main__':

    scheduler = BlockingScheduler()
    scheduler.add_job(test_task, 'interval', seconds=5)
    scheduler.start()