# -*- coding: utf-8 -*- 
"""
@author: Bruce
@contact: lanmin@itssoft.net
@file: aps_init.py
@datetime: 2020/11/11 10:56
"""
from __future__ import unicode_literals

from flask import Flask
from datetime import datetime
from apscheduler.schedulers.background import BackgroundScheduler

def test_task():
    print(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))


def create_app():

    app = Flask(__name__)

    app.scheduler = BackgroundScheduler()
    app.scheduler.add_job(test_task, 'interval', seconds=5)
    app.scheduler.start()

    return app
