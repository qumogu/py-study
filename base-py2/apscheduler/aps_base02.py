# -*- coding: utf-8 -*- 
"""
@author: Bruce
@contact: lanmin@itssoft.net
@file: aps_base.py
@datetime: 2020/11/10 16:44
"""
from __future__ import unicode_literals
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.schedulers.blocking import BlockingScheduler
from apscheduler.jobstores.redis import RedisJobStore
from apscheduler.jobstores.memory import MemoryJobStore
from apscheduler.jobstores.mongodb import MongoDBJobStore
from apscheduler.jobstores.rethinkdb import RethinkDBJobStore
from apscheduler.jobstores.sqlalchemy import SQLAlchemyJobStore
from apscheduler.jobstores.zookeeper import ZooKeeperJobStore
from apscheduler.executors.pool import ProcessPoolExecutor, ThreadPoolExecutor
from datetime import datetime
import time

def test_task():
    print(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    time.sleep(10)

def test_task2():
    print("222"+ datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    time.sleep(10)

def test_task3():
    print("333"+ datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    time.sleep(10)

if __name__ == '__main__':



    executors = {
        'default': ThreadPoolExecutor(10)
    }

    s1 = BackgroundScheduler(executors)

    s1.add_job(test_task, 'interval', seconds=3)
    s1.add_job(test_task2, 'interval', seconds=3)
    s1.add_job(test_task3, 'interval', seconds=3)
    s1.start()