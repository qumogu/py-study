# -*- coding: utf-8 -*- 
"""
@author: Bruce
@contact: lanmin@itssoft.net
@file: main.py
@datetime: 2020/11/10 20:14
"""
from __future__ import unicode_literals

from aps_init import create_app

from aps_views import DECE
app = create_app()

@app.route("/")
def index():

    return "ok"

if __name__ == '__main__':

    app.run("0.0.0.0", port=3030, debug=True)