# -*- coding: utf-8 -*- 
"""
@author: Bruce
@contact: lanmin@itssoft.net
@file: aps_base.py
@datetime: 2020/11/10 16:44
"""
from __future__ import unicode_literals
from apscheduler.schedulers.background import BackgroundScheduler
from flask import Flask, current_app
from flask_apscheduler import APScheduler

from apscheduler.jobstores.redis import RedisJobStore

from apscheduler.executors.pool import ProcessPoolExecutor, ThreadPoolExecutor
from datetime import datetime
import time

# def test_task2(name):
#     print(name + ":"+datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
#     time.sleep(2)
#     print(name+"------"+datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
#
# def test_task():
#     print(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))


app = Flask(__name__)

# app.config.from_object(SchedulerConfig())

# jobstores = {
#         'default': RedisJobStore(host="192.168.0.243",port=6379, password="Nash1234@",db=11),
# }
# executors = {
#     'default': ThreadPoolExecutor(5),
# }
#
# job_defaults = {
#     'coalesce': False,  # 默认情况下关闭新的作业
#     'max_instances': 10  # 设置调度程序将同时运行的特定作业的最大实例数3
# }
# app.scheduler = BackgroundScheduler(jobstores=jobstores, executors=executors, job_defaults=job_defaults)
#
# scheduler = APScheduler()
# scheduler.init_app(app)
# scheduler.start()

@app.route("/")
def index():
    # current_app.scheduler.start()
    # current_app.scheduler.add_job(test_task, 'interval', seconds=5, args=['jack'], id='jack')
    return "ok"


if __name__ == '__main__':

    app.run("0.0.0.0", port=4040, debug=True)