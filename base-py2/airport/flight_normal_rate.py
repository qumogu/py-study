# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: mysql_dev_01.py
@datetime: 2020/6/8 15:54
"""
from __future__ import unicode_literals

import pymysql
from tools import get_yesterday, get_roma_api

def handle_flight_normal_rate(api_data, conn, action):
    cursor = conn.cursor()
    print("获取游标成功")
    for i in api_data:
        staticdate = i.get("staticdate")
        statictype = i.get("statictype")
        if staticdate and statictype:
            if statictype == "始发放行正常率":
                table_name = "flight_normal_rate_start"
            else:
                table_name = "flight_normal_rate_all"

            if action == "create":
                sql = "insert into %s(staticdate, airport, plancount, normal, abnormal, normalrate) " \
                      "values('%s', '%s', %s, %s, %s, %s)" % (
                    table_name,
                    staticdate,
                    i.get("airport"),
                    i.get("plancount"),
                    i.get("normal"),
                    i.get("abnormal"),
                    i.get("normalrate")
                )
            else:
                sql = "update {} set plancount={}, normal={}, abnormal={}, normalrate={} where staticdate='{}' " \
                      "".format(table_name,
                                i.get("plancount"),
                                i.get("normal"),
                                i.get("abnormal"),
                                i.get("normalrate"),
                                staticdate)
            print(sql)
            data = cursor.execute(sql)
            conn.commit()
            print(data, cursor.rowcount, cursor.rownumber)
    cursor.close()
    print("游标关闭成功")

def flight_normal_rate(host, mysql_config, action='update'):
    api = "flightNormalRate"
    p_date = get_yesterday()
    params = {
        "p_date": p_date
    }
    print(p_date)
    api_data = get_roma_api(host, api, params)
    if api_data:
        try:
            conn = pymysql.connect(**mysql_config)
            print("创建数据连接成功")
            handle_flight_normal_rate(api_data, conn, action)
            conn.close()
            print("连接关闭成功")
        except Exception as e:
            print("失败")
            print(e)

