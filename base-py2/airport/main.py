# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: mysql_dev_01.py
@datetime: 2020/6/8 15:54
"""
from __future__ import unicode_literals
import sys
from flight_normal_rate import flight_normal_rate

api_map = {
    "1": flight_normal_rate,
}

if __name__ == '__main__':

    if len(sys.argv) < 4:
        sys.exit('启动命令缺失参数，示例如下：python file num dev/prod create/update')
    api_key = sys.argv[1]
    run_type = sys.argv[2]
    action = sys.argv[3]
    # print(api_key, run_type, action)

    if run_type == "dev":
        api_host = "http://www.testa.com"
        db_host = "air_mysql2.banber.com"
    elif run_type == "prod":
        api_host = "http://10.80.36.2:1080"
        db_host = "air_mysql2.banber.com"
    else:
        sys.exit("第二参数错误")

    mysql_config = {
        "host": db_host,
        "port": 15006,
        "user": "khfwglpt",
        "password": "Tencent@2020",
        "database": "nash_kettle"
    }
    print(api_host, db_host)
    if api_key not in api_map:
        sys.exit("没有这个接口")
    if action in ["create", "update"]:
        api_map[api_key](api_host, mysql_config, action)
    else:
        sys.exit("没有这个动作")
