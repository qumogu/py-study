# -*- coding: utf-8 -*- 
"""
@author: Bruce
@contact: lanmin@itssoft.net
@file: es_base01.py
@datetime: 2020/11/9 10:01
"""
from __future__ import unicode_literals
from elasticsearch import Elasticsearch

if __name__ == '__main__':

    es = Elasticsearch(hosts="192.168.0.243:9200")

    index_name = "delete_log"
    es_body = {
        "query":{
            "term":{
                "document_type": "report_gather",
            }
        }
    }

    ret = es.search(index=index_name, doc_type="report_gather", body=es_body)

    print(type(ret))
    print(len(ret['hits']['hits']))

    for i in ret['hits']['hits']:
        print (i)
        print (i.get('document_id'))
