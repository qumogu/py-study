# -*- coding: utf-8 -*- 
"""
@author: Bruce
@contact: lanmin@itssoft.net
@file: es_base01.py
@datetime: 2020/11/9 10:01
"""
from __future__ import unicode_literals
from elasticsearch import Elasticsearch
import json

import requests

def get_info(es_url):
    r = requests.get(es_url)
    if r.status_code == 200:
        res = r.text
        if res:
            data_dict = json.loads(res)
            return data_dict

    return None

if __name__ == '__main__':

    es_url = "http://192.168.0.243:9200"

    print(get_info(es_url))
    index_name = "bruce"

    index_url = "{}/{}".format(es_url, index_name)

    ret = requests.put(index_url).text
    print(ret)


    # es = Elasticsearch(hosts=es_url)
    #
    # index_name = "delete_log"
    # es_body = {
    #     "query":{
    #         "term":{
    #             "document_type": "report_gather",
    #         }
    #     }
    # }
    #
    # ret = es.search(index=index_name, doc_type="report_gather", body=es_body)
    #
    # print(type(ret))
    # print(len(ret['hits']['hits']))
    #
    # for i in ret['hits']['hits']:
    #     print (i)
    #     print (i.get('document_id'))
