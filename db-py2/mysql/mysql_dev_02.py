# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: mysql_dev_01.py
@datetime: 2020/6/8 15:54
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

import pymysql

mysql_config = {
    "host": "dev.qumogu.com",
    "port": 3336,
    "user": "root",
    "password": "Qumogu@1604",
    "database": "study"
}
try:
    conn = pymysql.connect(**mysql_config)
    print("创建数据连接成功")

    cursor = conn.cursor()
    print("获取游标成功")

    #sql = "INSERT INTO teacher(name, age, worktime) VALUES(%s, %s, %s)"
    #cursor.execute(sql,("李四",40,"15年"))
    #sql = "select 1 from student where name='test02'  limit 100"
    sql = "select * from student where name like '%t%t'  limit 100"
    cursor.execute(sql)
    print("执行sql成功")

    data = cursor.fetchall()

    for i in data:
        print i[1]

    if data:
        print(type(data))
        print("数据长度：{}".format(len(data)))
    else:
        print(type(data))
        print("数据长度：{}".format(len(data)))
        print("无结果")

    # conn.commit()
    # print("提交数据成功")

    cursor.close()
    print("游标关闭成功")

    conn.close()
    print("连接关闭成功")


except Exception as e:
    print("失败")
    print(e)