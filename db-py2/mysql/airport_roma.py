# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: mysql_dev_01.py
@datetime: 2020/6/8 15:54
"""
from __future__ import unicode_literals
import requests
import pymysql
import sys

from datetime import datetime, timedelta

def get_yesterday():
    now = datetime.now()
    a_day = timedelta(days=-1)
    new = now + a_day
    return new.strftime('%Y%m%d')

mysql_config = {
    "host": "air_mysql2.banber.com",
    "port": 15006,
    "user": "khfwglpt",
    "password": "Tencent@2020",
    "database": "nash_kettle"
}

def get_roma_api(host, api, params):
    url = "{}/bigdata/dm/huawei/v1/{}/query".format(host, api)
    headers = {
        "X-HW-ID": "airport_customer_service_management_platform",
        # "X-HW-APPKEY": "gzibGX9wRhdlkWBnwlk1HQ==",
        "X-HW-APPKEY": "kzGYfRItLZqxOvmVmfcYfQ==",
    }

    try:
        r =requests.get(url, params=params, headers=headers)
    except:
        print ("网络无法连通")
        return "http error"
    print(r.headers)
    print(r.text)
    print(r.status_code)
    result = {}
    if r.status_code == 200:
        try:
            jsondata = r.json()
            print(jsondata['retJSON'])
            if jsondata:
                result = jsondata['retJSON'].get("result")
        except:
            print("生成请求返回数据,返回数据为非json数据")
    print(result)

    return result

def flight_normal_rate(api_data, conn, op_type='update'):
    cursor = conn.cursor()
    print("获取游标成功")

    for i in api_data:
        staticdate = i.get("staticdate")
        statictype = i.get("statictype")
        if staticdate and statictype:
            if statictype == "始发放行正常率":
                table_name = "flight_normal_rate_start"
            else:
                table_name = "flight_normal_rate_all"
            if op_type == "create":
                sql = "insert into %s(staticdate, airport, plancount, normal, abnormal, normalrate) " \
                      "values('%s', '%s', %s, %s, %s, %s)" % (
                    table_name,
                    staticdate,
                    i.get("airport"),
                    i.get("plancount"),
                    i.get("normal"),
                    i.get("abnormal"),
                    i.get("normalrate")
                )
            else:
                sql = "update {} set plancount={}, normal={}, abnormal={}, normalrate={} where staticdate='{}' " \
                      "".format(table_name,
                                i.get("plancount"),
                                i.get("normal"),
                                i.get("abnormal"),
                                i.get("normalrate"),
                                staticdate)
            print(sql)
            data = cursor.execute(sql)
            conn.commit()
            print(data, cursor.rowcount, cursor.rownumber)
    cursor.close()
    print("游标关闭成功")

if __name__ == '__main__':
    api = "flightNormalRate"
    p_date = get_yesterday()
    print(p_date)
    params = {
        "p_date": p_date
    }
    host = "http://www.testa.com"
    if len(sys.argv) > 1:
        host = "http://10.80.36.2:1080"
    api_data = get_roma_api(host, api, params)

    if api_data:
        try:
            conn = pymysql.connect(**mysql_config)
            print("创建数据连接成功")
            # flight_normal_rate(api_data, cursor, 'create')
            flight_normal_rate(api_data, conn)
            conn.close()
            print("连接关闭成功")
        except Exception as e:
            print("失败")
            print(e)