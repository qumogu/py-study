# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: mysql_dev_01.py
@datetime: 2020/6/8 15:54
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

import pymysql
import threading
import time_s

def insert_data(i):
    mysql_config = {
        "host": "dev.qumogu.com",
        "port": 3336,
        "user": "root",
        "password": "Qumogu@1604",
        "database": "per_test"
    }

    conn = pymysql.connect(**mysql_config)

    cursor = conn.cursor(cursor=pymysql.cursors.DictCursor)

    for j in range(i*1000,(i+1)*1000):
        sql ="insert into test_insert(name) values (%s)"
        params = []
        for x in range(100):
            params.append([str(j)+ "-" + str(x)])

        cursor.executemany(sql, params)
        conn.commit()

    cursor.close()
    conn.close()


def main():



    ts = []
    for i in range(100):
        t = threading.Thread(target=insert_data, name="thread-"+str(i), args=(i, ))
        ts.append(t)

    start_time = time_study.time()
    for item in ts:
        item.start()
        print(item.name+"已经启动")

    for item in ts:
        item.join()
        print(item.name + "已经完毕")

    end_time = time_study.time()

    print("系统耗时：%.4f" %(end_time-start_time))






if __name__ == "__main__":
    main()