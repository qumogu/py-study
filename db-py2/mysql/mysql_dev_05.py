# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: mysql_dev_01.py
@datetime: 2020/6/8 15:54
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

import pymysql
import threading
import time_study

def get_dbnames():
    mysql_config = {
        "host": "dev.qumogu.com",
        "port": 3336,
        "user": "root",
        "password": "Qumogu@1604",
        "database": "study"
    }

    conn = pymysql.connect(**mysql_config)

    # cursor = conn.cursor(cursor=pymysql.cursors.DictCursor)
    cursor = conn.cursor()

    # sql = 'select *From testmyisam order by id desc limit 5'
    # sql = "delete from test1 where id = 5"
    sql = "update test1 set gender='F' where gender = 'M'"

    cursor.execute(sql)
    conn.commit()
    data = cursor.rowcount
    print(data, cursor.rownumber)

    cursor.close()
    conn.close()


def main():

    start_time = time_study.time()

    get_dbnames()
    end_time = time_study.time()

    print("系统耗时：%.4f" %(end_time-start_time))


if __name__ == "__main__":
    main()