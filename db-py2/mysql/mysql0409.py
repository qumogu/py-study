# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: mysql0409.py
@datetime: 2020/4/9 11:58
"""

from pymysql import connect, InternalError, cursors
from pymysql.err import OperationalError
from pymysql.err import ProgrammingError

class MysqlManager(object):
    """
    mysql操作基类
    """

    def __init__(self, host, port, username, password, db, charset):
        self.host = host
        self.port = port
        self.username = username
        self.password = password
        self.db = db
        self.charset = charset
        self.error = None
        self.client = self.connect()
        self.cursor = self.client.cursor() if self.client else None

    def get_cursor(self):
        self.cursor = self.client.cursor() if self.client else None

    def del_cursor(self):
        if self.cursor:
            try:
                self.cursor.close()
            except Exception:
                pass

    def connect(self):
        """
        连接数据库
        :return:
        """
        client = None
        try:
            client = connect(host=self.host,
                             port=self.port,
                             user=self.username,
                             passwd=self.password,
                             db=self.db,
                             charset=self.charset,
                             cursorclass=cursors.SSCursor)
        except Exception as e:
            if isinstance(e, OperationalError):
                if e[0] == 1045:
                    # 用户名或密码错误密码
                    self.error = "用户名或密码错误密码"
                elif e[0] == 2003:
                    # 主机或端口错误
                    self.error = "主机或端口错误"
                elif e[0] in [1044, 1049]:
                    # 数据库不存在或没有访问权限
                    self.error = "数据库不存在或没有访问权限"
                    self.error = "数据库不存在或没有访问权限"
            elif isinstance(e, InternalError):
                if e[0] == 1049:
                    # 数据库不存在或没有访问权限
                    self.error = "数据库不存在或没有访问权限"
            client = None
        finally:
            return client