# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: mysql_dev_01.py
@datetime: 2020/6/8 15:54
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

import pymysql
from datetime import datetime
from datetime import timedelta
from datetime import date
from datetime import time
from decimal import Decimal
from cx_Oracle import LOB
from uuid import UUID

def get_data_type(data):
    """通过数据列表，判断数据类型"""
    result = list()
    has_none_field = False
    none_fields = []
    item = data[0]
    item_len = len(item)

    # 判断第一行数据的类型
    for i in range(item_len):
        index = item[i]

        if index is None:
            has_none_field = True
            none_fields.append(i)
            result.append(0)
        else:
            result.append(get_field_type(index))

    if not has_none_field:
        return result

    # 如果第一行null的，则从后续数据中判断
    for item in data[1:]:
        for i in none_fields:
            if item[i] is not None:
                result[i]= get_field_type(item[i])
                none_fields.remove(i)
        if not none_fields:
            print(item)
            break

    return result


def get_field_type(value):
    """通过值判断类型"""

    if isinstance(value, Decimal):
        data_type = 1
    elif isinstance(value, datetime):
        data_type = 2
    elif isinstance(value, timedelta):
        data_type = 2
    elif isinstance(value, date):
        data_type = 2
    elif isinstance(value, time):
        data_type = 2
    elif isinstance(value, unicode):
        data_type = 0
    elif isinstance(value, int):
        data_type = 1
    elif isinstance(value, long):
        data_type = 1
    elif isinstance(value, bool):
        data_type = 1
    elif isinstance(value, UUID):
        data_type = 0
    elif isinstance(value, LOB):
        data_type = 0
    elif isinstance(value, str):
        data_type = 0
    elif isinstance(value, float):
        data_type = 1
    else:
        data_type = 0

    return data_type

mysql_config = {
    "host": "air_mysql.banber.com",
    "port": 15006,
    "user": "root",
    "password": "Nash1234@",
    "database": "kettle"
}
try:
    conn = pymysql.connect(**mysql_config)
    print("创建数据连接成功")

    cursor = conn.cursor()
    print("获取游标成功")

    #sql = "INSERT INTO teacher(name, age, worktime) VALUES(%s, %s, %s)"
    #cursor.execute(sql,("李四",40,"15年"))
    #sql = "select 1 from student where name='test02'  limit 100"
    #sql = "select * from student where gender='M' order by name limit 10"
    sql = "select id,username,create_time,address from test_user limit 5"
    cursor.execute(sql)
    print("执行sql成功")

    data = cursor.fetchall()
    #data = cursor.fetchone()

    for item in data:
        print(item)

    type_list = get_data_type(data)

    print(type_list)

    cursor.close()
    print("游标关闭成功")

    conn.close()
    print("连接关闭成功")

except Exception as e:
    print("失败")
    print(e)