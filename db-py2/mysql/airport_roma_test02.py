# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: mysql_dev_01.py
@datetime: 2020/6/8 15:54
"""
from __future__ import unicode_literals
import requests

def get_roma_api(host, api, params):
    url = "{}/bigdata/dm/huawei/v1/{}/query".format(host, api)
    headers = {
        "X-HW-ID": "airport_customer_service_management_platform",
        "X-HW-APPKEY": "kzGYfRItLZqxOvmVmfcYfQ==",
    }
    print("url")
    result = {}
    try:
        r =requests.get(url, params=params, headers=headers)
    except:
        print ("网络无法连通")
        return result
    print(r.headers)
    print(r.text)
    print(r.status_code)

    if r.status_code == 200:
        try:
            jsondata = r.json()
            print(jsondata['retJSON'])
            if jsondata:
                result = jsondata['retJSON'].get("result")
        except:
            print("生成请求返回数据,返回数据为非json数据")
    print(result)
    return result


if __name__ == '__main__':
    api = "flightNormalRate"
    p_date = "20201207"
    params = {
        "p_date":p_date
    }
    # host = "http://10.80.36.2:1080"
    host = "http://www.testa.com"
    get_roma_api(host, api, params)

