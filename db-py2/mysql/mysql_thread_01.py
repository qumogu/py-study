# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: mysql_dev_01.py
@datetime: 2020/6/8 15:54
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

import pymysql
import threading
import time_study
import random

def get_ram_char(num):
    up_char = "ABCDEFGHIJKLMANOPQRSTUVWXYZ"
    char = "abcdefghijklmnopqrstuvwxyz"
    res = up_char[random.randint(0,25)]
    i = 0
    while i < num-1:
        res = res + char[random.randint(0,25)]
        i += 1

    return res


class TestMysql(threading.Thread):

    def __init__(self, tid, tname, mysql_conn):
        threading.Thread.__init__(self)
        self.id = tid
        self.name = tname
        self.conn = mysql_conn
        self.cursor = self.conn.cursor(cursor=pymysql.cursors.DictCursor)

    def run(self):
        print("开启线程:" + self.name)
        for j in range(self.id * 1000, (self.id + 1) * 1000):
            sql = "insert into testmyisam(name) values (%s)"
            params = []
            for x in range(100):
                params.append([str(j) + "-" + get_ram_char(6) + "-" + str(x)])
            self.cursor.executemany(sql, params)
            self.conn.commit()

    def __del__(self):
        self.cursor.close()
        self.conn.close()
        print("关闭线程:" + self.name)


def main():
    mysql_config = {
        "host": "dev.qumogu.com",
        "port": 3336,
        "user": "root",
        "password": "Qumogu@1604",
        "database": "per_test"
    }
    ts = []
    for i in range(100):
        conn = pymysql.connect(**mysql_config)
        t = TestMysql(i, "TestMyisam[" + str(i) + "]", conn)
        ts.append(t)
    print("线程池启动")
    start_time = time_study.time()
    for item in ts:
        item.start()

    for item in ts:
        item.join()

    end_time = time_study.time()
    print("线程池关闭")
    print("系统耗时：%.4f" %(end_time-start_time))


if __name__ == "__main__":
    main()