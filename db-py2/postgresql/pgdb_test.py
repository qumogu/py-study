# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: pgdb_test.py
@datetime: 2020/5/8 14:06
"""

import psycopg2
from psycopg2.extras import RealDictCursor, DictCursor, NamedTupleCursor
#from psycopg2 import

# host = "10.253.247.246"
# port = 11345
# database = "devdb"
# username = "ns_readonly"
# password = "1Rsep46A"
# schema = "adqdevapp"

host = "192.168.0.243"
port = 5432
database = "bruce_test"
username = "postgres"
password = "lan1983"
schema = "test_schema"

table_name = "test_schema.test_info"


def main():

    try:
        conn = psycopg2.connect(host=host,
                            port=port,
                            database=database,
                            user=username,
                            password=password,
                                cursor_factory=RealDictCursor)
        print("连接成功")
    except Exception as e:
        print(e.message)

    try:
        cursor = conn.cursor()

        sql = "select id,title from {} limit 10 ".format(table_name)

        print sql

        cursor.execute(sql)

        # data = cursor.fetchall()
        data = cursor.fetchmany(2)
        cols = cursor.description

        for i in cols:
            print(i)

        for row in data:
            print(row.get("title"))
    except Exception as e:
        print(e.message)

    cursor.close()
    conn.close()

if __name__ == '__main__':
    main()