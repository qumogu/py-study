# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: pgsql_0508.py
@datetime: 2020/4/10 11:31
"""

import time_study

import psycopg2

class PgsqlManager(object):

    def __init__(self, **kwargs):
        self.host = kwargs.get("host", "localhost")
        self.port = kwargs.get("port", "5432")
        self.database = kwargs.get("database", "postgres")
        self.db_schema = kwargs.get("schema", "public")
        self.username = kwargs.get("username")
        self.password = kwargs.get("password")
        self.client = self.connect()
        self.cursor = self.client.cursor() if self.client else None

    def __del__(self):
        if self.cursor:
            try:
                self.cursor.close()
                print("删除游标")
            except Exception:
                pass

        if self.client:
            try:
                self.client.close()
                print("删除连接")
            except Exception:
                pass

    def get_cursor(self):
        self.cursor = self.client.cursor() if self.client else None

    def del_cursor(self):
        if self.cursor:
            try:
                self.cursor.close()
            except Exception:
                pass
            self.cursor = None

    def connect(self):
        conn = None
        try:
            conn = psycopg2.connect(host=self.host,
                                    port=self.port,
                                    database=self.database,
                                    user=self.username,
                                    password=self.password)

            print("数据库连接成功")
        except Exception as e:
            print(e)

        self.cursor = conn.cursor() if conn else None
        return conn

    def get_tabale_data(self, table_name, offset=1, limit=20):
        sql = "select *from {} limit {} ;".format(table_name, limit)
        self.cursor.execute(sql)
        data = self.cursor.fetchall()

        return data

    def get_tables(self, schema_name=None):
        if schema_name:
            sql = "select schemaname,tablename as name from pg_tables where schemaname = '{}';".format(schema_name)
        else:
            sql = "select schemaname,tablename as name from pg_tables WHERE tablename NOT LIKE 'pg%' AND tablename NOT LIKE 'sql_%';"
        # cur = self.client.cursor()
        # cur.execute(sql)
        self.cursor.execute(sql)
        """
        print ("开始休眠1")
        for i in range(0, 1):
            time.sleep(1)
            print i
        """
        data = self.cursor.fetchall()
        #cur.close()
        #self.client.close()
        return data

    def get_table_data(self, table_name):
        sql = "select *from {} limit 10".format(table_name)
        print(sql)

        self.cursor.execute(sql)
        return self.cursor.fetchall()

    def exec_sql(self, sql):
        self.cursor.execute(sql)
        return self.cursor.fetchall()


def main():
    host = "10.253.247.246"
    port = 11345
    database = "devdb"
    username = "ns_readonly"
    password = "1Rsep46A"
    schema = "adqdevapp"
    test_tbname = "adqdevapp.nas56_enter_person_city_area"
    time1 = time_study.time()
    mypgsql = PgsqlManager(host="192.168.0.134", port="5432", database="bruce_test",
                           username="postgres", password="lan1983")

    #mypgsql2 = PgsqlManager(host=host, port=port, db=database, username=username, password=password, schema=schema)
    data = 0
    try:
        #data =mypgsql.get_tabale_data("test_schema.test_info")
        #table_names = mypgsql.get_tables("public")
        #table_names = [".".join(i) for i in table_names if i]

        #tb_data = mypgsql2.get_table_data("info")
        tb_data = None
        tb_names = mypgsql.get_tables("test_schema")
        print(tb_names)
        #table_names = ["|-|".join(i) for i in tb_names if i]
        #print(table_names)


        tb_data = mypgsql.get_table_data("test_info")

        print(tb_data)

    except Exception as e:
        print (e)

    time2 = time_study.time()

    use_time = time2 - time1

    print data
    print ("查询耗时：%.4f" % use_time)

    #print mypgsql2.get_table_data("public.info")

"""
    
    time.sleep(5)



    mypgsql2.del_cursor()
    print "游标关闭"


    if mypgsql2.cursor :
        print "游标对象在"
    else:
        print "游标已清空"

    time.sleep(10)
    mypgsql2.get_cursor()
    print mypgsql2.get_tables("test_schema")

    mypgsql2.client.close()
    print "连接关闭"

    if mypgsql2.client :
        print "连接对象在"
    else:
        print "连接已清空"

    print "--   延迟10秒   ---"
    for i in range(0, 2):
        time.sleep(5)
        print i*5
    del mypgsql
    print "删除一个连接"
    for i in range(0, 2):
        time.sleep(5)
        print i * 5
    print "---结束--"
"""

if __name__ == '__main__':
    main()