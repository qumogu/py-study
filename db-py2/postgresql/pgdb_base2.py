# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: pgdb_test.py
@datetime: 2020/5/8 14:06
"""

import psycopg2
from psycopg2.extras import RealDictCursor, DictCursor, NamedTupleCursor

host = "192.168.0.243"
port = 5432
database = "bruce_test"
username = "postgres"
password = "lan1983"
schema = "base-py2"

def get_jsonbs(conn):

    cursor = conn.cursor()
    sql = "select id,info from base-py2.study_jsonb"
    cursor.execute(sql)
    data = cursor.fetchall()

    for row in data:
        print(row.get("info").get("customer"))
    cursor.close()

def get_jsonb(conn):

    cursor = conn.cursor()
    sql = "select id,info from base-py2.study_jsonb"
    cursor.execute(sql)
    data = cursor.fetchone()
    info = data.get("info")
    print(type(info))
    print(info.get("items").get("product"))
    cursor.close()


def create_table(conn):
    cursor = conn.cursor()
    sql = '''CREATE TABLE "base-py2"."study_jsonb" (
          "id" serial4,
          "info" jsonb,
          PRIMARY KEY ("id")
        );
        '''
    cursor.execute(sql)
    conn.commit()
    cursor.close()
    print("创建表成功")


def insert_json(conn):

    cursor = conn.cursor()
    sql = "insert into base-py2.study_jsonb (info) values(%s)"
    msgs = [['{"customer": "Lily Bush", "items": {"product": "Diaper","qty": 24}}'],
            ['{"customer": "Josh William", "items": {"product": "Toy Car","qty": 1}}'],
            ['{"customer": "Mary Clark", "items": {"product": "Toy Train","qty": 2}}'],
            ['{"customer": "Jack Clark", "items": {"product": "Foods","qty": 10}}']]
    cursor.executemany(sql, msgs)
    conn.commit()
    cursor.close()
    print("插入数据成功")


def where_jsonb(conn):
    cursor = conn.cursor()
    sql = "select id,info from base-py2.study_jsonb where info -> 'items' ->> 'product' = 'Foods'"
    cursor.execute(sql)
    data = cursor.fetchone()
    if data:
        info = data.get("info")
        print(type(info))
        print(info.get("items").get("product"))
    cursor.close()

def group_jsonb(conn):
    cursor = conn.cursor()
    sql = '''
    SELECT MIN(CAST(info -> 'items' ->> 'qty' AS INTEGER)) ,
       MAX (
          CAST (
             info -> 'items' ->> 'qty' AS INTEGER
          )
       ),
       SUM (
          CAST (
             info -> 'items' ->> 'qty' AS INTEGER
          )
       ),
       AVG (
          CAST (
             info -> 'items' ->> 'qty' AS INTEGER
          )
       )
       FROM base-py2.study_jsonb;
    '''
    cursor.execute(sql)
    data = cursor.fetchall()
    for row in data:
        print(row)
    cursor.close()

def main():

    try:
        conn = psycopg2.connect(host=host,
                            port=port,
                            database=database,
                            user=username,
                            password=password,
                            cursor_factory=RealDictCursor)
        print("连接成功")
    except Exception as e:
        print(e.message)

    # create_table(conn)
    # insert_json(conn)
    # get_jsonbs(conn)
    # where_jsonb(conn)
    group_jsonb(conn)

    conn.close()

if __name__ == '__main__':
    main()



# def insert_json(conn,t_name):
#
#     cursor = conn.cursor()
#     sql = "insert into {} (name, age,msg) values(%s, %s, %s)".format(t_name)
#     name = "lily01"
#     age = 21
#     msg = '{ "customer": "Lily Bush", "items": {"product": "Diaper","qty": 24}}'
#     cursor.execute(sql,(name,age,msg))
#     conn.commit()
#     cursor.close()
#     print("插入数据成功")


# def get_json(conn):
#
#     cursor = conn.cursor()
#     sql = "select id,msg from {} where id=3".format(table_name)
#     cursor.execute(sql)
#     data = cursor.fetchall()
#     # data = cursor.fetchmany(2)
#     # cols = cursor.description
#     #
#     # for i in cols:
#     #     print(i)
#     for row in data:
#         print(row.get("msg").get("sorce"))
#     cursor.close()