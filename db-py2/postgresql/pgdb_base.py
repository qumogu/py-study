# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: pgdb_test.py
@datetime: 2020/5/8 14:06
"""

import psycopg2
from psycopg2.extras import RealDictCursor, DictCursor, NamedTupleCursor
#from psycopg2 import

# host = "10.253.247.246"
# port = 11345
# database = "devdb"
# username = "ns_readonly"
# password = "1Rsep46A"
# schema = "adqdevapp"

host = "192.168.0.243"
port = 5432
database = "bruce_test"
username = "postgres"
password = "lan1983"
schema = "base-py2"

table_name = "base-py2.user"

def get_jsons(conn):

    cursor = conn.cursor()
    sql = "select id,info from base-py2.study_json"
    cursor.execute(sql)
    data = cursor.fetchall()

    for row in data:
        print(row.get("info").get("customer"))
    cursor.close()

def get_json(conn):

    cursor = conn.cursor()
    sql = "select id,info from base-py2.study_json"
    cursor.execute(sql)
    data = cursor.fetchone()
    info = data.get("info")
    print(type(info))
    print(info.get("items").get("product"))
    cursor.close()


def create_table(conn):
    cursor = conn.cursor()
    sql = '''CREATE TABLE "base-py2"."study_json" (
          "id" serial4,
          "info" json,
          PRIMARY KEY ("id")
        );
        '''
    cursor.execute(sql)
    conn.commit()
    cursor.close()
    print("创建表成功")


def insert_json(conn):

    cursor = conn.cursor()
    sql = "insert into base-py2.study_json (info) values(%s)"
    msgs = [['{"customer": "Lily Bush", "items": {"product": "Diaper","qty": 24}}'],
            ['{"customer": "Josh William", "items": {"product": "Toy Car","qty": 1}}'],
            ['{"customer": "Mary Clark", "items": {"product": "Toy Train","qty": 2}}'],
            ['{"customer": "Jack Clark", "items": {"product": "Foods","qty": 10}}']]
    cursor.executemany(sql, msgs)
    conn.commit()
    cursor.close()
    print("插入数据成功")

def main():

    try:
        conn = psycopg2.connect(host=host,
                            port=port,
                            database=database,
                            user=username,
                            password=password,
                                cursor_factory=DictCursor)
        print("连接成功")
    except Exception as e:
        print(e.message)


    # get_json(conn)

    # create_table(conn)
    # insert_json(conn)
    get_json(conn)

    conn.close()

if __name__ == '__main__':
    main()



# def insert_json(conn,t_name):
#
#     cursor = conn.cursor()
#     sql = "insert into {} (name, age,msg) values(%s, %s, %s)".format(t_name)
#     name = "lily01"
#     age = 21
#     msg = '{ "customer": "Lily Bush", "items": {"product": "Diaper","qty": 24}}'
#     cursor.execute(sql,(name,age,msg))
#     conn.commit()
#     cursor.close()
#     print("插入数据成功")


# def get_json(conn):
#
#     cursor = conn.cursor()
#     sql = "select id,msg from {} where id=3".format(table_name)
#     cursor.execute(sql)
#     data = cursor.fetchall()
#     # data = cursor.fetchmany(2)
#     # cols = cursor.description
#     #
#     # for i in cols:
#     #     print(i)
#     for row in data:
#         print(row.get("msg").get("sorce"))
#     cursor.close()