# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: pgsql_0508.py
@datetime: 2020/4/10 11:31
"""

import time_study

import psycopg2

class PgsqlManager(object):

    def __init__(self, **kwargs):
        self.host = kwargs.get("host", "localhost")
        self.port = kwargs.get("port", "5432")
        self.database = kwargs.get("database", "postgres")
        self.schema = kwargs.get("schema", "public")
        self.username = kwargs.get("username")
        self.password = kwargs.get("password")
        self.client = self.connect()
        self.cursor = self.client.cursor() if self.client else None

    def __del__(self):
        if self.cursor:
            try:
                self.cursor.close()
                print("删除游标")
            except Exception:
                pass

        if self.client:
            try:
                self.client.close()
                print("删除连接")
            except Exception:
                pass

    def get_cursor(self):
        self.cursor = self.client.cursor() if self.client else None

    def del_cursor(self):
        if self.cursor:
            try:
                self.cursor.close()
            except Exception:
                pass
            self.cursor = None

    def connect(self):
        conn = None
        try:
            conn = psycopg2.connect(host=self.host,
                                    port=self.port,
                                    database=self.database,
                                    user=self.username,
                                    password=self.password)

            print("数据库连接成功")
        except Exception as e:
            print(e)

        self.cursor = conn.cursor() if conn else None
        return conn

    def get_tabale_data(self, table_name, offset=1, limit=20):
        sql = "select *from {} limit {} ;".format(table_name, limit)
        self.cursor.execute(sql)
        data = self.cursor.fetchall()
        return data

    def get_table_views(self):
        sql = """ SELECT schemaname, tablename AS NAME
                                  FROM pg_tables
                                  WHERE tablename NOT LIKE 'pg%' AND tablename NOT LIKE 'sql_%' AND  schemaname = '{}'
                                  UNION
                                  SELECT schemaname, viewname AS NAME
                                  FROM pg_views
                                  WHERE  schemaname = '{}';
                                """.format(self.schema, self.schema)
        self.cursor.execute(sql)
        data = self.cursor.fetchall()

        return data

    def get_tables(self):
        sql = """ SELECT schemaname, tablename AS NAME FROM pg_tables
                                  WHERE tablename NOT LIKE 'pg%' AND tablename NOT LIKE 'sql_%'; """
        self.cursor.execute(sql)
        data = self.cursor.fetchall()

        return data

    def get_views(self):
        sql = """SELECT schemaname, viewname AS NAME
                  FROM pg_views WHERE  viewname NOT LIKE 'pg%' AND viewname NOT LIKE 'sql_%';"""

        self.cursor.execute(sql)
        data = self.cursor.fetchall()

        return data

    def get_table_data(self, table_name):
        sql = "select *from {} limit 10".format(table_name)
        self.cursor.execute(sql)
        return self.cursor.fetchall()

    def exec_sql(self, sql):
        self.cursor.execute(sql)
        return self.cursor.fetchall()


def main():
    host = "10.253.247.246"
    port = 11345
    database = "devdb"
    username = "ns_readonly"
    password = "1Rsep46A"
    schema = "adqdevapp"
    test_tbname = "nas56_enter_person_city_area"

    time1 = time_study.time()

    mypgsql2 = PgsqlManager(host=host, port=port, database=database, username=username, password=password, schema=schema)

    try:


        tb_names = mypgsql2.get_tables()
        print(tb_names)
        table_names = mypgsql2.get_views()
        print(table_names)
        tb_data = None
        tb_data = mypgsql2.get_table_data(test_tbname)
        print(tb_data)

    except Exception as e:
        print (e)

    time2 = time_study.time()
    use_time = time2 - time1

    print ("查询耗时：%.4f" % use_time)

if __name__ == '__main__':
    main()