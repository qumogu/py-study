# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: nash_aggregate1.py
@datetime: 2020/5/22 18:39
"""

import pymongo
from bson import ObjectId
import datetime
from dateutil import parser



def main():
    # 连接本地测试环境，192.168.0.134
    client = pymongo.MongoClient(host="192.168.0.155", port=24002, username="base-py2", password="Nash1234@",
                                 connect=False)
    #doc = client["tuxi2"].content
    doc = client["tuxi2"].item

    group= {"$group":{"_id":"$itemClass","count":{"$sum":1}}}
    # project = {"$project":{"ver":"$itemVer", "his":"$historyVersionDetails","create_date":{"$substr":["$createTime",0,10]}}}

    sort = {"$sort":{"_id":1}}

    cursor = doc.aggregate([group,sort])
    j = 0
    for i in cursor:
        print (i)
        j += 1


    print("一共%d条记录" % j)




if __name__ == '__main__':
    main()