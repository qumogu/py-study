# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: nash_aggregate1.py
@datetime: 2020/5/22 18:39
"""

import pymongo
from bson import ObjectId
import random

def get_random_name(num):

    txt = "abcdefghijklmnopqrstuvwxyz0123456789_"
    chars = []
    for i in range(num):
        r_num = random.randint(0,36)
        chars.append(txt[r_num])
    name = "".join(chars)
    return name

grades = ["一","二","三","四","五","六"]
schools =["建安小学","光明小学","红旗小学","宝安小学","灵芝小学"]


def main():
    # 连接本地测试环境，192.168.0.134
    client = pymongo.MongoClient(host="192.168.0.134", port=24002, username="base-py2", password="Nash1234@",
                                 connect=False)
    #doc = client["tuxi2"].content
    brcue = client["tuxi2"].bruce

    result = brcue.remove()
    print(result)

    students = []
    for i in range(2000):
        student = {
            "username": get_random_name(random.randint(4,10)),
            "stu_id": "10001{}".format(str(i)),
            "homework": [random.randint(3,10) for i in range(10)],
            "mid_exam": random.randint(60,100),
            "final_exam": random.randint(60,100),
            "addition": random.randint(1,6),
            "school": schools[random.randint(0, 4)],
            "grade": grades[random.randint(0, 5)],
            "class": str(random.randint(1, 8)),
            "description": "我是第{}个学生，我们班级是最棒的！".format(str(i))
        }
        students.append(student)

    result = brcue.insert_many(students)
    print(len(result.inserted_ids))

    # cursor = brcue.find({})
    # cursor = brcue.find({"username":{"$regex": "b", "$options": "i"}})
    # # s_id = student["_id"]
    # # print(str(s_id))
    # i = 0
    # for student in cursor:
    #     s_id = student["_id"]
    #     result = brcue.update_one({"_id":s_id}, {"$push":{"friends":"a"+str(i)}})
    #
    #     # result = brcue.update_one({"_id": s_id}, {"$unset": {"friends": 1}})
    #     print(result.modified_count)
    #     student = brcue.find_one({"_id":s_id}, {"username": 1, "friends":1})
    #     print(student)
    #     i += 1


if __name__ == '__main__':
    main()