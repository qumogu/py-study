# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: nash_aggregate1.py
@datetime: 2020/5/22 18:39
"""

import pymongo
from datetime import datetime, timedelta

from bson import ObjectId

import time
import xlsxwriter

def create_file_path(file_type='excel'):
    """
    生成导出文件的路径
    :param file_type:excel和其他
    :return:
    """
    basic_path = 'files/'
    t = time.time()
    timestamp = int(round(t * 1000))

    # 用户导出是xlsx文件
    if file_type == 'excel':
        file_path = '{}/{}.xlsx'.format(basic_path, timestamp)
    else:
        file_path = '{}/{}.{}'.format(basic_path, timestamp, file_type)

    return file_path


def create_data_file(column_info, data):

    file_path = create_file_path()
    # 以时间戳为名新建表格
    print('创建导出的信息excel文件：{}'.format(file_path))
    workbook = xlsxwriter.Workbook(file_path)
    sheet = workbook.add_worksheet('data')

    # 设置表格的行宽
    head_line = list()
    column_ids = column_info.keys()
    for i in range(0, len(column_ids)):
        sheet.set_column(i, i, 10)
        head_line.append(column_info.get(column_ids[i]))

    # 设置表格格式
    head_format = workbook.add_format({'align': 'centre', 'bold': True, "bg_color": 'gray'})
    normal_format = workbook.add_format({'align': 'centre'})

    # 写入首行信息
    for col in xrange(0, len(head_line)):
        sheet.write(0, col, head_line[col], head_format)

    # 写入数据细腻些
    row_no = 1
    for item in data:
        for col in xrange(0, len(head_line)):
            sheet.write(row_no, col, item.get(column_ids[col]), normal_format)
        row_no += 1
    sheet.write(row_no, 0, "", normal_format)
    row_no += 1
    msg1 = "导出数据，一共导出{}行数据".format(row_no).decode('utf-8')
    print(msg1)
    sheet.write(row_no, 1, msg1, normal_format)
    row_no += 1
    msg2 = "导出时间：{}".format(datetime.now()).decode('utf-8')
    sheet.write(row_no, 1, msg2, normal_format)
    workbook.close()

    return file_path

def main():
    # 连接本地测试环境，192.168.0.134  5f6c65e7e3f4e6892c0d7a14.txt
    client = pymongo.MongoClient(host="192.168.0.243", port=24002, username="base-py2", password="Nash1234@",
                                 connect=False)
    data_set_id = "6034686d0cf07b000f742876"  #603468350cf07b000f742871
    howdata2 = client["howdata"].howdata2
    dataset2 = client["tuxi2"].data_set2

    ds = dataset2.find_one({"_id":ObjectId(data_set_id)}, {"columns":1, "data_version":1})
    print(ds)

    column_info = dict()
    for col in ds.get("columns"):
        # print(col.get("m_column_id"), col.get("column_name"))
        column_info.update({str(col.get("m_column_id")): col.get("column_name")})
    print(column_info)

    cursor = howdata2.find({"version":ds.get("data_version")}, {'_id':0, "version":0})

    create_data_file(column_info, cursor)


if __name__ == '__main__':
    main()