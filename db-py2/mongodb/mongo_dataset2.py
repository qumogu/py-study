# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: nash_aggregate1.py
@datetime: 2020/5/22 18:39
"""
from __future__ import unicode_literals
import pymongo
from datetime import datetime, timedelta
import urllib
from bson import ObjectId
import pickle


def main():
    # 连接本地测试环境，192.168.0.134  5f6c65e7e3f4e6892c0d7a14.txt
    # client = pymongo.MongoClient(host="192.168.0.243", port=24002, username="base-py2", password="Nash234@",
    #                              connect=True)
    mg_uri = "mongodb://{}:{}@192.168.0.243:24002/admin".format(urllib.quote("base-py2"), urllib.quote("Nash1234@"))
    mg_client = pymongo.MongoClient(mg_uri, connect=True)

    doc = mg_client["tuxi2"].data_set2
    print("ok")

    ds = doc.find_one({"report_gather_id": "5ea295ea4491d80021e6be9d"})

    # print(ds)

    report_items = ds.get("report_items")
    print(len(report_items))
    new_report_items = []
    if report_items:
        item_ids = []
        for report_item in report_items:
            item_id = report_item.get("item_id")
            if item_id not in item_ids:
                item_ids.append(item_id)
                new_report_items.append(report_item)

    print(len(new_report_items))


if __name__ == '__main__':
    main()