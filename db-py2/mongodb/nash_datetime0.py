# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: nash_aggregate1.py
@datetime: 2020/5/22 18:39
"""

import pymongo
from bson import ObjectId
import datetime
from dateutil import parser




def main():
    # 连接本地测试环境，192.168.0.134
    client = pymongo.MongoClient(host="192.168.0.155", port=24002, username="base-py2", password="Nash1234@",
                                 connect=False)
    #doc = client["tuxi2"].content
    doc = client["tuxi2"].item

    # select_time = parser.parse("2020-06-16")
    #now_time = datetime.datetime.now()
    now_time = parser.parse("2020-06-16")
    print(now_time)
    change = datetime.timedelta(hours=-3)
    #change = datetime.timedelta(minutes=-15)
    #change = datetime.timedelta(seconds=-15)
    print(change)
    print(type(change))
    select_time = now_time+change


    print(select_time)
    print(type(select_time))
    cursor = doc.find({{"$dateToString":{"format":"%Y-%m-%d","date":"$createTime"}}},{"_id":0,"createTime":1,"itemVer":1})
    j = 0
    for i in cursor:
        print (i)
        j += 1

    print("一共%d条记录" % j)



if __name__ == '__main__':
    main()