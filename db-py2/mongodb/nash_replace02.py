# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: nash_aggregate1.py
@datetime: 2020/5/22 18:39
"""

import pymongo
from datetime import datetime, timedelta



def main():
    # 连接本地测试环境，192.168.0.134
    client = pymongo.MongoClient(host="192.168.0.243", port=24002, username="base-py2", password="Nash1234@",
                                 connect=False)
    #doc = client["tuxi2"].content
    doc = client["tuxi2"].item

    start_str = "2020-06-09"
    cur_time = datetime.strptime(start_str, '%Y-%m-%d')
    print(type(cur_time))
    print(cur_time)
    aDay = timedelta(days=1)
    # end_time = datetime.strptime("2020-01-10",'%Y-%m-%d')
    end_time = datetime.now()

    while True:
        cur_time = cur_time + aDay
        next_time = cur_time + aDay
        if cur_time > end_time:
            break
        cursor = doc.find({"createTime":{"$gte":cur_time,"$lt":next_time}}, {"itemVer":1})

        item_ids = []

        for i in cursor:
            print (i)
            item_ids.append(str(i["_id"]))
        str_cur = cur_time.strftime('%Y-%m-%d')
        print("%s一共%d条记录" % (str_cur, len(item_ids)))
        print("-*-"*30)




if __name__ == '__main__':
    main()