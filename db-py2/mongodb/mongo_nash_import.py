# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: nash_aggregate1.py
@datetime: 2020/5/22 18:39
"""

import pymongo
from datetime import datetime, timedelta

from bson import ObjectId
import pickle

def main():
    # 连接本地测试环境，192.168.0.134  5f6c65e7e3f4e6892c0d7a14.txt
    client = pymongo.MongoClient(host="192.168.0.243", port=24002, username="base-py2", password="Nash1234@",
                                 connect=False)

    doc = client["howdata"].test

    filename = "db/5f6c65e7e3f4e6892c0d7a14.txt"
    with open(filename, 'rb') as f:
        data = pickle.load(f)

    print("读取数据成功" )
    if data:
        for i in data:
            print(i.pop("_id"))

    doc.insert_many(data)
    print("导入数据成功")


if __name__ == '__main__':
    main()