# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: nash_aggregate1.py
@datetime: 2020/5/22 18:39
"""

import pymongo
from bson import ObjectId
import random
import sys
reload(sys)
sys.setdefaultencoding("utf-8")

def main():
    client = pymongo.MongoClient(host="192.168.0.134", port=24002, username="base-py2", password="Nash1234@",
                                    connect=False)
    doc = client["tuxi2"].bruce
    print(doc.find().count())
    match1 = {"$match":{"final_exam":{"$gte":90}}}
    match2 = {"$match":{"class_num":"三（1）班"}}
    project = {"$project":{"sid":"$stu_id","school_num":"$school", "grade_num":"$grade","class_num":"$class", "mexam":"$mid_exam"}}
    group = {"$group":{"_id":{"school_num":"$school_num","grade_num":"$grade_num","class_num":"$class_num"},
                       'stu_count':{"$sum":1},'mid_exam_total':{"$sum":"$mexam"},'mid_exam_avg':{"$avg":"$mexam"}
                       }}
    # 单个group字段
    # group = {"$group": {"_id": "$school_num",
    #                     'stu_count': {"$sum": 1}, 'mid_exam_total': {"$sum": "$mexam"},
    #                     'mid_exam_avg': {"$avg": "$mexam"}
    #                     }}
    # "mid_exam_min":{"$min":"$mid_exam"}, "mid_exam_max":{"$max":"$mid_exam"}
    sort = {"$sort":{"stu_count":-1}}
    addfield ={"$addFields":{}}
    aggs = [match1, project, group, sort]
    cursor = doc.aggregate(aggs)
    #print cursor
    j = 0

    for i in cursor:
        print(i)
        j += 1

    print(j)

if __name__ == '__main__':
    main()