# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: nash_aggregate1.py
@datetime: 2020/5/22 18:39
"""

import pymongo
from datetime import datetime, timedelta

from bson import ObjectId
import pickle

def main():
    # 连接本地测试环境，192.168.0.134  5f6c65e7e3f4e6892c0d7a14.txt
    client = pymongo.MongoClient(host="192.168.0.243", port=24002, username="base-py2", password="Nash1234@",
                                 connect=False)

    doc = client["howdata"].howdata2

    cursor = doc.find({"version":"b1d74423296ce7d59e4645f40f6d7df9"})

    items = []

    for i in cursor:
        #print (i)
        items.append(i)

    print("一共%d条记录" % (len(items)))
    print("-*-"*30)

    filename = "db/" + str(ObjectId()) + ".txt"
    with open(filename, 'wb') as f:
        pickle.dump(items, f)

    print("数据备份成功" )


if __name__ == '__main__':
    main()