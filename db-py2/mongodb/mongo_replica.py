# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: nash_aggregate1.py
@datetime: 2020/5/22 18:39
"""
from __future__ import unicode_literals
import pymongo
from datetime import datetime, timedelta
import urllib
from bson import ObjectId
import pickle


#mongodb://mongouser:******@10.0.0.14:27017,10.0.0.16:27017,10.0.0.42:27017/admin?authSource=admin&replicaSet=cmgo-ca0i1dpp_0
#10.0.0.16:27017
def main():
    # 连接本地测试环境，192.168.0.134  5f6c65e7e3f4e6892c0d7a14.txt
    # client = pymongo.MongoClient(host="192.168.0.243", port=24002, username="base-py2", password="Nash234@",
    #                              connect=True)
    mongodb_type = "replicaset"
    # mongodb_type = "single"
    MONGODB_HOSTS = "42.194.173.62:27017,119.29.166.51:27017"
    MONGODB_USER = "base-py2"
    MONGODB_PWD = "Nash1234@"
    MONGODB_REPLICA_SET = "mongo_clus"

    if mongodb_type == "single":
        mg_uri = "mongodb://{}:{}@192.168.0.243:24002/admin".format(urllib.quote("base-py2"), urllib.quote("Nash1234@"))

    else:
        # mg_hosts = MONGODB_HOSTS.split(",")
        # mg_ports = MONGODB_PORTS.split(",")
        mg_uri = "mongodb://{}:{}@{}/?replicaset={}".format(urllib.quote(MONGODB_USER), urllib.quote(MONGODB_PWD), MONGODB_HOSTS, MONGODB_REPLICA_SET)

    mg_client = pymongo.MongoClient(mg_uri, connect=False)

    doc = mg_client["test02"].test
    print("ok")

    # import random
    # num = random.randint(0,10000)
    # user = {
    #     "user":"user_" + str(num),
    #     "age": num
    # }
    # res=doc.insert_one(user)
    # print(res.inserted_id)

    cursor = doc.find()

    items = []

    for i in cursor:
        print (i)
        items.append(i)

    print("一共%d条记录" % (len(items)))
    print("-*-"*30)

    # filename = "db/" + str(ObjectId()) + ".txt"
    # with open(filename, 'wb') as f:
    #     pickle.dump(items, f)
    #
    # print("数据备份成功" )


if __name__ == '__main__':
    main()