# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: mongo_study.py
@datetime: 2020/4/15 13:58
"""

import pymongo
import json
from bson import ObjectId

class MongoManage:

    def __init__(self, host, port, username, password, database):
        self.host = host
        self.port = port


    def connect(self):
        pass

def print_msg(result):
    print type(result)
    print result

def new_col(db):
    mycol = db.classinfo
    data = {"classname": u"四4班", "nums": 48,
            "students": [{"s_name": "张三", "height": 1.45}, {"s_name": "李四", "height": 1.32}]}
    result = mycol.insert(data)
    print_msg(result)

def get_cols(db):
    collist = db.collection_names()
    print collist

def insert_many(db):
    users = [{"name":"张三", "age":33}, {"name":"李四", "age":45}]
    result = db.test.insert_many(users)
    print_msg(result)

def insert_data(db,data,col="test02"):
    if isinstance(data,list):
        pass

def main():
    try:
        # 121.37.249.234
        # client = pymongo.MongoClient(host="106.52.59.98", port=24000, username="base-py2", password="nash1234@",
        #                              connect=False)
        client = pymongo.MongoClient(host="192.168.0.134", port=24002, username="base-py2", password="Nash1234@", connect=False)
        #client = pymongo.MongoClient(host="dev.qumogu.com", port=27027, username="qumogu", password="Qumogu@1604", connect=False)
        # client = pymongo.MongoClient(host="dev.qumogu.com", port=27027, username="bruce.db", password="bruce.db@1532", connect=False)
        # client = pymongo.MongoClient("mongodb://bruce.db:bruce.db@1532@dev.qumogu.com:27027")
        #client = pymongo.MongoClient("mongodb://qumogu:Qumogu@160@dev.qumogu.com:27027")

        db = client["testb"]

        doc = db["users"]
        result = doc.find_one({})
        # result = doc.find_one({"_id": ObjectId("5ebe3fe07d962000011a4e50")},{"verify_info":1,"status":1, "catalogue_id":1})
        print(result)


        #in_data = json.dumps(data)

        #print in_data
        # data = [1,2,3]
        # if isinstance(data, list):
        #     print "ok"

        # data2 = []


        # result = db.nash_test.insert_one({'username':'jack','password':'jack123'})
        # print (result.inserted_id)
        #insert_many(db)
        # result = doc.update_one({"_id":ObjectId("5ebd102be3f4e652f067e109")},
        #                              {"$set":{"age":27}, "$addToSet":{'list_num':{"$each":data2}}})
        # result = doc.update_one({"_id": ObjectId("5ebd102be3f4e652f067e109")},
        #                         {"$set": {"age": 27}, "$addToSet": {'list_num': {"$each": data2}}})
        # result = doc.update_one({"_id": ObjectId("5ebd102be3f4e652f067e109")},
        #                          {"$unset": {"list_numqqq": 5}})
        # print(result.modified_count)




    except Exception as e:
        print "连接错误"
        print e
"""
    user1 = test02.find_one({"name":"may"}, {"name":1})
    user2 = test02.find_one({"name":"lily"}, {"_id":0})
    id1 = user1.get("_id")
    age = user2.get("age")

    print user1
    print id1
    print age
"""


if __name__ == '__main__':
    main()