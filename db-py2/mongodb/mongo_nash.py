# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: mongo_nash.py
@datetime: 2020/4/21 11:59
"""
import pymongo
from bson import ObjectId
import random

def get_data(doc_banner):
    data = doc_banner.find()
    result = []

    for i in data:
        result.append({
            "id": str(i.get("_id")),
            "catalogue_id": i.get("catalogue_id"),
            "serial": i.get("serial")
        })

    print(result)

def get_data2(doc_banner):
    data = doc_banner.find({"catalogue_id": "5e980b621a8707000ffb585d", "is_delete": 0, "serial": {"$gt": 0}})
    result = []

    for i in data:
        result.append({
            "id": str(i.get("_id")),
            "serial": i.get("serial")
        })
    result = sorted(result, key=lambda x: x.get("serial"), reverse=False)
    print(result)

def get_one(doc_banner ,catalogue_id, banner_id) :
    banner = doc_banner.find_one({'_id': ObjectId(banner_id), "catalogue_id": catalogue_id})

    print(banner)


def update_one(doc_banner,catalogue_id, banner_id, serial):
    result = doc_banner.update_one({'_id': ObjectId(banner_id), "catalogue_id": catalogue_id, "is_delete": 0},
                         {"$set": {'serial': serial}})
    print("更新成功"+str(result))

def update_serial(doc_banner,catalogue_id, banner_id, serial):

    """
    更新位置顺序
    :param catalogue_id:
    :param banner_id:
    :param serial:
    :return:
    """

    banner = doc_banner.find_one({'_id': ObjectId(banner_id), "catalogue_id": catalogue_id, "is_delete": 0},
                                     {"serial": 1})
    origin_serial = banner.get("serial")

    # 如果位置一样，不操作mongodb
    if serial == origin_serial:
        print ("位置一样")


    if serial > origin_serial:
        print ("位置往后移动")
        data = doc_banner.find(
            {"catalogue_id": catalogue_id, "is_delete": 0, "serial": {"$gt": origin_serial, "$lte": serial}},
            {"serial": 1})
        for banner in data:
            doc_banner.update_one({'_id': banner["_id"]}, {"$set": {"serial": banner["serial"] - 1}})
    else:
        print ("位置往前移动")
        data = doc_banner.find(
            {"catalogue_id": catalogue_id, "is_delete": 0, "serial": {"$gte": serial, "$lt": origin_serial}},
            {"serial": 1})
        for banner in data:
            doc_banner.update_one({'_id': banner["_id"]}, {"$set": {"serial": banner["serial"] + 1}})

    doc_banner.update_one({'_id': ObjectId(banner_id), "catalogue_id": catalogue_id, "is_delete": 0},
                          {"$set": {'serial': serial}})
    print ("位置更新成功")

def get_random_name(num):
    txt = "abcdefghijklmnopqrstuvwxyz0123456789_"
    #words = set(txt)
    chars = []
    for i in range(num):
        r_num = random.randint(0,36)
        chars.append(txt[r_num])
    name = "".join(chars)
    return name



def main():
    # 连接本地测试环境，192.168.0.134
    client = pymongo.MongoClient(host="192.168.0.134", port=24002, username="base-py2", password="Nash1234@",
                                connect=False)

    #doc_bak = client["tuxi2"].content_bak

    # get_data2(doc_banner)
    # get_one(doc_banner, "5e980b621a8707000ffb585d", "5e8302cc7d5694008e278ed8")

    #update_serial(doc_banner, "5e980b621a8707000ffb585d", "5e9e95f4db706b001c49de84", 1)

    #update_one(doc_banner, "5e980b621a8707000ffb585d" , "5e9e9cbcdb706b001c49de85", 9)

    #get_data2(doc_banner)
    #data = doc.find_one({"_id": ObjectId("5e980b621a8707000ffb585d")})

    # or_list = [{item: {"$regex": "天气",
    #                    "$options": 'i'}} for item in ["title", "description"]]
    #"tag_ids": ["5ea4f6996f9f8905d13a5db0","5ea4ef526f9f8905b356f58c"]
    #tag_id = "5ea4ef526f9f8905b356f58c"


    # search_info = {"$and": [{'status': "active", 'catalogue_id': "5e980b621a8707000ffb585d"},
    #                         {"$or": {"tag_ids":{"$elemMatch": tag_id}}}]}
    #search_info = {'status': "active", 'catalogue_id': '5e980b621a8707000ffb585d', 'tag_ids': tag_id}
    #search_info = {'status': "active", 'catalogue_id': '5e980b621a8707000ffb585d', 'tag_ids': {'$elemMatch': {tag_id}}}

    #search_info = {"$and": [{'status': "active"}, {'catalogue_id': "5e980b621a8707000ffb585d"}], "$or": or_list}

    # data = doc.find(search_info)
    # for i in data:
    #     print(i)
    #     print("-"*50)

    #cont = doc.find_one({"_id": ObjectId("5ea16f7d75de720001dc3425")}, {"tag_ids": 1})
    # cursor = doc.find({"verify_info.verify_uids": "5e8302cc7d5694008e278ed8"}, {"title": 1})
    # print(cursor)
    #
    # if cursor:
    #
    #     for i in cursor:
    #         print("{}-----{}".format(i.get("title"),str(i.get("_id"))))
    #         #print()
    # else:
    #     print("没有信息")
    # i = doc.find_one({})
    # print(i)
    #
    # i["is_delete"]=1
    # result = doc_bak.insert_one(i)
    # print(result.inserted_id)

    doc = client["tuxi2"].bruce

    #cursor = doc.find({"verify_info.verify_uids": "5e8302cc7d5694008e278ed7"}, {"status":1,"verify_info":1})
    #cursor = doc.find({"columns.column_id": ObjectId("5ec61c3d47c671000f738e95")}, {"columns.$.column_name": 1})
    #cursor = doc.find()
    #
    match = {"$match":{"final_exam":{"$gte":80}}}
    group = {"$group":{"_id":{"class_num":"$class_num"}, 'mid_exam_avg':{"$avg":"$mid_exam"}}}
    aggs = [match,group]
    cursor = doc.aggregate(aggs)
    print cursor

    for i in cursor:
        print("{}:{}".format(i.key(), i.value()))




def main3():
    # 连接Nash test环境，test02.howdata.cn
    client = pymongo.MongoClient(host="139.196.75.52", port=24000, username="base-py2", password="Nash1234@",
                                 connect=False)
    doc = client["tuxi2"].report_gather
    data = doc.find_one({'_id': ObjectId("5e1becd686e9f80001af2092")})

    print (data)

def main4():
    # 连接开发测试环境
    client = pymongo.MongoClient(host="dev.qumogu.com", port=27027, username="bruce.db", password="bruce.db@1532",
                                 authSource="bruce_dev", connect=False)
    doc = client["bruce_dev"].test

    data = doc.find_one()
    print (data)
    print (type(data))

    student = {"name":"timi", "age":3}

    data = doc.find_one({"_id":ObjectId("5e9676a78e418ebaabe143d7")}, {"name": 1})
    print (data)

    result = doc.insert_one(student)
    print (result)
    print (result.inserted_id)
    #"_id":ObjectId("5e9676a78e418ebaabe143d9")

    # update = doc.update_one({"_id":ObjectId("5e9676a78e418ebaabe143d7")}, {"$set":{"age": 24}})
    # print update.acknowledged
    # print update.matched_count
    # print update.modified_count
    #
    # data = doc.find_one()
    # print data
    #
    # result = []
    # for i in data:
    #     #result.append({"id":str(i.get("_id")),"name":i.get("name"),"age":i.get("age")})
    #     print i
    #
    #
    #
    # print result

def main5():
    # 连接本地测试环境，192.168.0.134
    client = pymongo.MongoClient(host="192.168.0.134", port=24002, username="base-py2", password="Nash1234@",
                                 connect=False)
    #doc = client["tuxi2"].content
    brcue = client["tuxi2"].bruce

    # students = []
    #
    #
    # for i in range(50):
    #     student = {
    #         "username": get_random_name(random.randint(4,10)),
    #         "stu_id": "10001{}".format(str(i)),
    #         "homework": [random.randint(3,10) for i in range(10)],
    #         "mid_exam": random.randint(60,100),
    #         "final_exam": random.randint(60,100),
    #         "addition": random.randint(1,6),
    #         "description": "我是第{}个学生，我们班级是最棒的！".format(str(i))
    #     }
    #     students.append(student)
    #
    # result = brcue.insert_many(students)
    # print(result.inserted_ids)

    # cursor = brcue.find({})
    # cursor = brcue.find({"username":{"$regex": "b", "$options": "i"}})
    # # s_id = student["_id"]
    # # print(str(s_id))
    # i = 0
    # for student in cursor:
    #     s_id = student["_id"]
    #     result = brcue.update_one({"_id":s_id}, {"$push":{"friends":"a"+str(i)}})
    #
    #     # result = brcue.update_one({"_id": s_id}, {"$unset": {"friends": 1}})
    #     print(result.modified_count)
    #     student = brcue.find_one({"_id":s_id}, {"username": 1, "friends":1})
    #     print(student)
    #     i += 1

    cursor = brcue.find({})
    for student in cursor:
        s_id = student["_id"]
        brcue.update_one({"_id":s_id}, {"$set":{"class_num":"三（{}）班".format(str(random.randint(1,7)))}})


if '__main__' == __name__:
    main()