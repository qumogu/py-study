# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: nash_aggregate1.py
@datetime: 2020/5/22 18:39
"""
from __future__ import unicode_literals
import pymongo
from datetime import datetime, timedelta
import urllib
from bson import ObjectId
import pickle


#mongodb://mongouser:******@10.0.0.14:27017,10.0.0.16:27017,10.0.0.42:27017/admin?authSource=admin&replicaSet=cmgo-ca0i1dpp_0
#10.0.0.16:27017
def main():
    # 连接本地测试环境，192.168.0.134  5f6c65e7e3f4e6892c0d7a14.txt
    # client = pymongo.MongoClient(host="192.168.0.243", port=24002, username="base-py2", password="Nash234@",
    #                              connect=True)
    mg_uri = "mongodb://{}:{}@192.168.0.243:24002/admin".format(urllib.quote("base-py2"), urllib.quote("Nash1234@"))
    mg_client = pymongo.MongoClient(mg_uri, connect=True)

    doc = mg_client["howdata"].test2
    print("ok")
    doc.create_index([("md5",-1)])
    print("end")
    # cursor = doc.find({"version":"b1d74423296ce7d59e4645f40f6d7df9"})
    #
    # items = []
    #
    # for i in cursor:
    #     #print (i)
    #     items.append(i)
    #
    # print("一共%d条记录" % (len(items)))
    # print("-*-"*30)

    # filename = "db/" + str(ObjectId()) + ".txt"
    # with open(filename, 'wb') as f:
    #     pickle.dump(items, f)
    #
    # print("数据备份成功" )


if __name__ == '__main__':
    main()