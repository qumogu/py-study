# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: nash_aggregate1.py
@datetime: 2020/5/22 18:39
"""

import pymongo
from bson import ObjectId
import datetime
from dateutil import parser



def main():
    # 连接本地测试环境，192.168.0.134
    client = pymongo.MongoClient(host="192.168.0.155", port=24002, username="base-py2", password="Nash1234@",
                                 connect=False)
    #doc = client["tuxi2"].content
    doc = client["tuxi2"].item

    # group= {"$group":{"_id":"$itemClass","count":{"$sum":1}}}
    #
    # cursor = doc.aggregate([group])

    sql = {"itemClass":"gif"}
    cursor = doc.find({"itemClass":"gif"},{"itemVer":1,"historyVersionDetails":1})


    item_ids = []
    success_ids =[]
    fail_ids = []
    for i in cursor:
        print (i)
        item_ver = i.get("itemVer", 0)
        imgsrc = i["historyVersionDetails"][item_ver]["itemData"]["data"]["imgSrc"]
        print(imgsrc)
        update_field = "historyVersionDetails.{}.itemData.data.imgSrc".format(item_ver)
        # new_src = imgsrc.replace("http://local.banber.com/","http://www.banber.com/")
        new_src = imgsrc.replace("http://www.banber.com/", "http://local.banber.com/")
        print(new_src)
        try:
            doc.update_one({"_id":i["_id"]}, {"$set":{update_field:new_src}})
            success_ids.append(str(i["_id"]))
        except Exception as e:
            fail_ids.append(str(i["_id"]))

        item_ids.append(str(i["_id"]))

    print("一共%d条记录" % len(item_ids))
    print("成功%d条记录" % len(success_ids))
    print("失败%d条记录" % len(fail_ids))



if __name__ == '__main__':
    main()