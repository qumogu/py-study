# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: nash_aggregate1.py
@datetime: 2020/5/22 18:39
"""

import pymongo
from bson import ObjectId
import datetime
from dateutil import parser



def main():
    # 连接公司测试环境，192.168.0.155
    # #now_time = datetime.datetime.now()
    # select_time = parser.parse("2020-06-16").date()
    # print(select_time)
    # print(type(select_time))
    # #cursor = doc.find({"createTime":{"$gte":select_time}},{"_id":0,"createTime":1,"itemVer":1})
    # cursor = doc.find({"createTime": select_time}, {"_id": 0, "createTime": 1, "itemVer": 1})
    # j = 0
    # for i in cursor:
    #     print (i)
    #     j += 1
    #
    # print("一共%d条记录" % j)


    # project = {"$project":{"read":"$readGroup","day":{"$substr":["$createTime",0,10]}}}
    # match = {"$match":{"day":"2020-06-15"}}
    client = pymongo.MongoClient(host="192.168.0.155", port=24002, username="base-py2", password="Nash1234@",
                                 connect=False)

    doc = client["tuxi2"].item
    select_time = parser.parse("2020-06-10")
    print(select_time)

    project = {"$project": {"class":"$itemSubClass","itemSubClass":1, "itemVer": 1,"_id":0,
                            "date": {"$substr": ["$createTime", 0, 10]}}}
    match1 = {"$match": {"createTime":{"$gte":select_time},"itemSubClass":{"$exists":True}}}
    #group = {"$group": {"_id": "$date", "item_count": {"$sum": 1}}}
    group = {"$group": {"_id": {"date":"$date","item_class":"$class"}, "item_count": {"$sum": 1}}}
    match2 = {"$match":{"item_count":{"$gte":6}}}
    sort = {"$sort": {"_id": -1}}
    limit = {"$limit": 30}
    cursor = doc.aggregate([match1,project,group,match2,limit,sort])
    j = 0
    for i in cursor:
        print (i)
        j += 1

    print("一共%d条记录" % j)



if __name__ == '__main__':
    main()