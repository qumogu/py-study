# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: nash_aggregate1.py
@datetime: 2020/5/22 18:39
"""

import pymongo
from bson import ObjectId
import random

def main():
    client = pymongo.MongoClient(host="192.168.0.243", port=24002, username="base-py2", password="Nash1234@",
                                    connect=False)
    doc = client["howdata"].howdata2

    #group = {"$group":{"_id":{"version":"$version"}, 'count':{"$sum":1}}}
    group = {"$group": {"_id": {"version": "$version"}}}


    aggs = [group]
    cursor = doc.aggregate(aggs)
    #print(len(cursor))

    j = 0
    for i in cursor:
        print(i)
        j += 1

    print(j)

if __name__ == '__main__':
    main()