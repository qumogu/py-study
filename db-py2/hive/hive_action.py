# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: hive_action.py
@datetime: 2020/4/2 19:21
"""


class HiveManager(object):
    """Hive数据库的操作基类"""

    def __init__(self, host, port, username, password, db, charset):
        pass

    def __del__(self):
        pass

    def get_cursor(self):
        pass

    def del_cursor(self):
        pass

    def connect(self):
        pass

    @timelimited(60 * 20)
    def check_table_is_can_read(self, table_name):
        pass

    def get_column_info(self, table_name):
        pass

    def get_data(self, table_name, offset, limit, column_name_list, all=True):
        pass

    def get_table_data(self, table_name, offset=1, limit=100, column_name_list=None):
        pass

    def get_table_count(self, table_name):
        pass

    def get_db_table_offset(self, table_name, offset, limit):
        pass

    def get_all_tables(self):
        pass

    #####################################

    def get_db_table_uptime(self, table_name):
        pass

    def get_db_table_count(self, table_name):
        pass

    def get_line_size(self, table_name):
        pass

    def get_primary_key(self, table_name):
        pass