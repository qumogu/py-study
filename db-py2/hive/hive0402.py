# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: hive0402.py
@datetime: 2020/4/2 19:44
"""

from pyhive import hive
import sys
import time_study

class HiveManager(object):
    """Hive数据库的操作基类"""

    def __init__(self, host, port, username, password, database="default", auth='CUSTOM'):
        self.host = host
        self.port = port
        self.username = username
        self.password = password
        self.database = database
        self.auth = auth
        self.error = None
        self.client = self.connect()
        self.cursor = self.client.cursor() if self.client else None


    def __del__(self):
        if self.cursor:
            try:
                self.cursor.close()
            except Exception:
                pass

        if self.client:
            self.client.close()


    def get_cursor(self):
        self.cursor = self.client.cursor() if self.client else None


    def del_cursor(self):
        if self.cursor:
            try:
                self.cursor.close()
            except Exception:
                pass


    def connect(self):
        """
        连接数据库
        :return:
        """
        client = None
        try:
            client = hive.Connection(host=self.host,
                             port=self.port,
                             username=self.username,
                             password=self.password,
                             database=self.database,
                             auth=self.auth)

        except Exception as e:
            # if isinstance(e, OperationalError):
            #     if e[0] == 1045:
            #         # 用户名或密码错误密码
            #         self.error = Error.DB_CONNECT_ERROR_USERNAEM_OR_PASSWD_ERROR
            #     elif e[0] == 2003:
            #         # 主机或端口错误
            #         self.error = Error.DB_CONNECT_ERROR_IP_OR_PORT_ERROR
            #     elif e[0] in [1044, 1049]:
            #         # 数据库不存在或没有访问权限
            #         self.error = Error.DB_NOT_EXIST_OR_NOT_PERMISION
            # elif isinstance(e, InternalError):
            #     if e[0] == 1049:
            #         # 数据库不存在或没有访问权限
            #         self.error = Error.DB_NOT_EXIST_OR_NOT_PERMISION
            # client = None
            print(e)
        finally:
            return client

    def get_all(self, sql):
        self.cursor.execute(sql)
        data = self.cursor.fetchall()

        return data

    def get_one(self, sql):
        self.cursor.execute(sql)
        data = self.cursor.fetchone()

        return data

    def get_many(self, sql ,size):
        self.cursor.execute(sql)
        data = self.cursor.fetchmany(size)

        return data




def main():
    data = sys.argv
    sql = data[1]
    myhive = HiveManager(host='49.233.91.43',
                       port=10000,
                       auth='CUSTOM',
                       database='default',
                       username='snpt',
                       password='snpt2020@test02')
    #sql = "select t3451a2d8c42e4d3c9f80ab42ee16ca2c_id from 0054b0395aa840968bd1b82ca02cf9eb limit 10"
    time1 = time_study.time()
    try:
        data = myhive.get_tmpdata(sql)
        print (data)
    except Exception as e:
        print(e)
    time2 = time_study.time()
    wait_time = time2-time1

    print ("数据查询耗时:%.4f" % wait_time)




if __name__ == '__main__':
    main()