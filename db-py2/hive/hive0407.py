# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: hive0402.py
@datetime: 2020/4/2 19:44
"""

from pyhive import hive
import sys
import time_study

class HiveManager(object):
    """Hive数据库的操作基类"""

    def __init__(self, host, port, username, password, database="default", auth='CUSTOM'):
        self.host = host
        self.port = port
        self.username = username
        self.password = password
        self.database = database
        self.auth = auth
        self.error = None
        self.client = self.connect()
        self.cursor = self.client.cursor() if self.client else None


    def __del__(self):
        if self.cursor:
            try:
                self.cursor.close()
            except Exception:
                pass

        if self.client:
            self.client.close()


    def get_cursor(self):
        self.cursor = self.client.cursor() if self.client else None


    def del_cursor(self):
        if self.cursor:
            try:
                self.cursor.close()
            except Exception:
                pass


    def connect(self):
        """
        连接数据库
        :return:
        """
        client = None
        try:
            client = hive.Connection(host=self.host,
                             port=self.port,
                             username=self.username,
                             password=self.password,
                             database=self.database,
                             auth=self.auth)

        except Exception as e:
            # if isinstance(e, OperationalError):
            #     if e[0] == 1045:
            #         # 用户名或密码错误密码
            #         self.error = Error.DB_CONNECT_ERROR_USERNAEM_OR_PASSWD_ERROR
            #     elif e[0] == 2003:
            #         # 主机或端口错误
            #         self.error = Error.DB_CONNECT_ERROR_IP_OR_PORT_ERROR
            #     elif e[0] in [1044, 1049]:
            #         # 数据库不存在或没有访问权限
            #         self.error = Error.DB_NOT_EXIST_OR_NOT_PERMISION
            # elif isinstance(e, InternalError):
            #     if e[0] == 1049:
            #         # 数据库不存在或没有访问权限
            #         self.error = Error.DB_NOT_EXIST_OR_NOT_PERMISION
            # client = None
            print(e)
        finally:
            return client

    def get_all(self, sql):
        self.cursor.execute(sql)
        data = self.cursor.fetchall()

        return data

    def get_one(self, sql):
        self.cursor.execute(sql)
        data = self.cursor.fetchone()

        return data

    def get_many(self, sql ,size):
        self.cursor.execute(sql)
        data = self.cursor.fetchmany(size)

        return data


    def get_all_tables(self):
        """
                获取所有表名
                :return:
                """

        result = list()
        sql = "show tables"
        self.cursor.execute(sql)
        data = self.cursor.fetchall()
        for item in data:
            result.append(item[0])
        return result


    def get_table_count(self, table_name):
        """获取表的数量"""

        self.cursor.execute("select count(1) from {}".format(table_name))
        total = self.cursor.fetchone()
        if total:
            total = total[0]
        return total

    def get_lowversion_data(self, sql, offset, limit):
        self.cursor.execute(sql)
        data = self.cursor.fetchall()
        startid=(offset-1)*limit
        data2 = data[startid:]
        return data2

    def get_table_data(self, table_name, offset=1, limit=100, column_name_list=None):

        if column_name_list:
            column_name = ['`{}`'.format(i) for i in column_name_list]
            column_name = ','.join(column_name)
        else:
            column_name = '*'

        print(column_name)
        if not offset:
            sql = """select {column_name}
                     from `{table_name}`
                     limit {limit}
                  """.format(column_name=column_name,
                             table_name=table_name,
                             limit=limit)
            self.cursor.execute(sql)
            data = self.cursor.fetchall()
        else:
            sql = " select {} from `{}` limit {}".format(column_name, table_name, int(offset) * int(limit))
            print(sql)
            data = self.get_lowversion_data(sql, int(offset), int(limit))

        return data

    def get_column_info(self, table_name):
        """
                获取字段信息
                :param table_name: 表名
                :return:
                """
        # 获取字段
        column_name_list = []
        column_type_list = []
        #sql = "show columns from {} from {}".format(table_name, self.database)
        sql = "describe {} ".format(table_name)
        print(sql)
        self.cursor.execute(sql)

        data = self.cursor.fetchall()
        #print (data)
        for item in data:
            #print (item[0])
            column_name_list.append(item[0])
            column_type_list.append(item[1])

        return column_name_list, column_type_list

    def get_one_data(self, table_name):

        """获取一条数据，用于测试是否可读"""
        sql = "select * from {} limit 1".format(table_name)
        self.cursor.execute(sql)
        data = self.cursor.fetchone()

        return data

def show_data(data):
    print (type(data))
    for item in data:
        print (item)


def show_createtable(data):
    print (type(data))
    i = 0
    while i <5:
        #print (item)
        txt = data[i][0]
        t1 = txt.split()
        print (t1)
        i += 1

def show_createtable2(data):
    print (type(data))

    for item in data:
        #print (item)
        txt = item[0]
        t1 = txt.split()
        if t1[0] == "CREATE":
            continue
        if t1[0] == "ROW":
            break
        print (t1[0])
        print(t1[1].rstrip(',').rstrip(')'))




def main():
    data = sys.argv
    sql = data[1]
    if len(data) > 2:
        action = data[2]
    else:
        action = "1"
    myhive = HiveManager(host='49.233.91.43',
                       port=10000,
                       auth='CUSTOM',
                       database='default',
                       username='snpt',
                       password='snpt2020@test02')
    #sql = "select t3451a2d8c42e4d3c9f80ab42ee16ca2c_id from 0054b0395aa840968bd1b82ca02cf9eb limit 10"
    time1 = time_study.time()
    data = None
    try:
        if action == "1":
            data = myhive.get_all(sql)
        elif action == "2":
            data = myhive.get_one(sql)
        else:
            # sql = "select t3451a2d8c42e4d3c9f80ab42ee16ca2c_id from 0054b0395aa840968bd1b82ca02cf9eb limit 10"
            # data = myhive.get_all(sql)
            #d1 = myhive.get_all(sql)
            #d2 = myhive.get_lowversion_data(2,5)
            #myhive.get_lowversion_data(sql,action)
            #data = '测试'
            table_name = "0054b0395aa840968bd1b82ca02cf9eb"
            #column_name_list= ['t3451a2d8c42e4d3c9f80ab42ee16ca2c_id']
            data = myhive.get_one_data(table_name)

        #print (data)
        show_data(data)
        #show_createtable2(data)
        #print (data[0])
        #print (data[1])
    except Exception as e:
        print(e)
    time2 = time_study.time()
    wait_time = time2-time1

    print ("数据查询耗时:%.4f" % wait_time)




if __name__ == '__main__':
    main()