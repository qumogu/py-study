# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: hive4.py
@datetime: 2020/4/2 18:33
"""

import pyhs2

with pyhs2.connect(host='49.233.91.43',
                   port=10000,
                   authMechanism="PLAIN",
                   user='snpt',
                   password='snpt2020@test02',
                   database='default') as conn:
    with conn.cursor() as cur:
        #Show databases
        print cur.getDatabases()

        #Execute query
        sql = "select t3451a2d8c42e4d3c9f80ab42ee16ca2c_id from 0054b0395aa840968bd1b82ca02cf9eb limit 10"
        cur.execute(sql)

        #Return column info from query
        print cur.getSchema()

        #Fetch table results
        for i in cur.fetch():
            print(i)