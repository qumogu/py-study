# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: hive2.py
@datetime: 2020/4/2 17:11
"""
#from pyhive import hive
from impala.dbapi import connect

sql = "select t3451a2d8c42e4d3c9f80ab42ee16ca2c_id from 0054b0395aa840968bd1b82ca02cf9eb limit 10"

"""
host='49.233.91.43'
port=10000
auth='CUSTOM'
database='default'
username='snpt'
password='snpt2020@test02'
"""
conn = connect(host='49.233.91.43', port=10000, database='default', user='snpt', password='snpt2020@test02')

cursor = conn.cursor()
cursor.execute(sql)

print(cursor.fetchall())