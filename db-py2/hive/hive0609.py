# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: hive0414.py
@datetime: 2020/4/14 15:22
"""
import time_study

from pyhive import hive

conn = None
client = None
data = None

try:
    conn = hive.Connection(host="10.0.2.7", port=10000, username="sntp", password="snpt2020@test02", database="default", auth="CUSTOM")
    client = conn.cursor()
except Exception as e:
    print "连接出错"
    print e.message

#sql = "select t3451a2d8c42e4d3c9f80ab42ee16ca2c_id from 0054b0395aa840968bd1b82ca02cf9eb limit 10"
sql = "select count(1) from 5658314f0f844110bcb7409948e1c69c"

if client:
    try:
        print("hive执行开始")
        start_time = time_study.time()
        client.execute(sql)
        data = client.fetchone()
        end_time = time_study.time()
    except Exception as e:
        print "连接出错"
        print e.message


print "结果"
print data
print("系统耗时：%.4f" %(end_time-start_time))