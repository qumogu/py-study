# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: first_hive.py
@datetime: 2020/4/2 14:41
"""
from pyhive import hive

conn = hive.Connection(host='49.233.91.43',
                       port=10000,
                       auth='CUSTOM',
                       database='default',
                       username='snpt',
                       password='snpt2020@test02')
sql = "select t3451a2d8c42e4d3c9f80ab42ee16ca2c_id from 0054b0395aa840968bd1b82ca02cf9eb limit 10"
cursor = conn.cursor()
cursor.execute(sql)
cursor.fetchmany(20)

print(cursor.fetchall())