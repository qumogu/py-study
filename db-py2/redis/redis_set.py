# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: redis_study.py
@datetime: 2020/4/13 15:36
"""
from redis import StrictRedis
import redis


import hashlib
import json
import time
import random
mycache = StrictRedis(host="192.168.0.243", port=6379, db=8, password="Nash1234@")

key = "test_set"
key2 = "test_set2"

# mycache.sadd(key,5,12,100)
# mycache.sadd(key2,5,18,100)

print(mycache.smembers(key))
print(mycache.smembers(key2))

print("集合的差集")
print(mycache.sdiff(key,key2))

print("集合的交集")
print(mycache.sinter(key,key2))

print("集合的并集")
print(mycache.sunion(key,key2))

# print("集合移动成员")
# print(mycache.smove(key,key2,"a"))
# print("集合删除成员")
# print(mycache.smembers(key2))
print(mycache.srem(key, "c"))
print(mycache.smembers(key))


# data = mycache.smembers(key)
#
# data = mycache.sscan(key)
#
# data = mycache.sscan_iter(key)
#
# print(data)
# for i in data:
#     print(i)
#
# print(mycache.sscan(key))

# print(type(data))

# a = set([10,2,2,4,6,4])
# a.add(3)
# print(a)
# a.add(2)
# print(a)

# members = list(data)
# print(members)
# # members.append("g")
# members.sort()
# print(members)

