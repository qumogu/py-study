# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: redis_study.py
@datetime: 2020/4/13 15:36
"""
from redis import StrictRedis
from redis.sentinel import Sentinel
from redis.client import Redis
import time
"""
42.194.173.62
119.29.166.51
119.29.79.71
"""
sentinel_list = [("119.29.166.51", "26379"),
                 ("119.29.79.71", "26379"),
                 ("42.194.173.62", "26379"),]


mySentinel = Sentinel(sentinel_list)


redis_master = mySentinel.master_for('mymaster', db=9, password="hophrFbAtLtdzF8zWf9m")
redis_salve = mySentinel.slave_for('mymaster', db=9, password="hophrFbAtLtdzF8zWf9m")
#mycache1 = StrictRedis(host="192.168.0.243", port=6379, db=9, password="Nash1234@")
#print(mycache1.get("test5"))
print("test02-----")

# print(redis_master.info().get(""))
master = mySentinel.discover_master('mymaster')

print(master)
print(master[0])
print (master[1])
print(mySentinel.discover_slaves('mymaster'))

print (type(redis_master))
#print (type(mycache1))

if isinstance(redis_master, Redis):
    print("ok----")
start_time = time.time()

redis_master.set("test3", 1)
print(redis_master.get("test3"))
end_time = time.time()

print(end_time-start_time)
test = redis_salve.get("test3")
print(test)
end_time = time.time()

print(end_time-start_time)

# 手动主从分离
#
#mycache1 = StrictRedis(host="192.168.0.243", port=6379, db=9, password="Nash1234@")
#
# mycache2 = StrictRedis(host="192.168.0.243", port=6380, db=9, password="Nash1234@")
#
# print(mycache2.get("test02"))
#
# mycache1.setex("test2",150,"2ww22")
#
# print(mycache2.get("test2"))


# 使用前判断
# import time
#
# start_time = time.time()
# mycache = StrictRedis(host="192.168.0.243", port=6378, db=9, password="Nash1234@")
#
# try:
#     #print(mycache.ping())
#     mycache.set("test_connect","test02,connect")
# except:
#     mycache = StrictRedis(host="192.168.0.243", port=6380, db=9, password="Nash1234@")
# print(mycache.get("test02"))
#
# end_time = time.time()
#
# print(end_time-start_time)
#
#def Redis_Con(Redis_ip, port):
    # Redis_sk=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    # Redis_Result=Redis_sk.connect_ex((Redis_ip, port))
    # if Redis_Result == 0:
    #     return True
    # else:
    #     return False
# # mycache2.setex("test2",150,"2ww22")
# #
# # print(mycache2.get("test2"))
# start_time = time.time()
# if Redis_Con("192.168.0.243", 6378):
#     mycache = StrictRedis(host="192.168.0.243", port=6378, db=9, password="Nash1234@")
# else:
#     mycache = StrictRedis(host="192.168.0.243", port=6380, db=9, password="Nash1234@")
#
# end_time = time.time()

# print(end_time-start_time)
#
# print(mycache.get("test02"))

