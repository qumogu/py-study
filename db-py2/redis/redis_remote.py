# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: redis_remote.py
@datetime: 2020/5/9 16:22
"""

from redis import ConnectionPool, StrictRedis, Connection, Redis

pool = ConnectionPool(host="dev.qumogu.com", port=16389, db=2, password="Qumogu@1604")

conn = StrictRedis(connection_pool=pool)

# key = "python:user"
# # value = "张三李四，多大第三方"
# value = "jack"
# r_conn.set(key, value, ex=150, nx=True)
#
# print(r_conn.get(key))
# conn.cluster()
#
# res = conn.keys()
#
# print(res)
#
# res2 = conn.scan(0, u"*user*", 20)
# print(res2)
#
# res = conn.execute_command("cluster nodes")
#
# res = conn.execute_command("cluster keyslot message:5ea58db3f76f2900014ae9a6")
#
# res = conn.execute_command("cluster countkeysinslot 8306")
#
# res = conn.execute_command("cluster getkeysinslot 8306 message:5ea58db3f76f2900014ae9a6")



try:
    print("1")
    conn.execute_command('multi')
    print("2")
    conn.set('id', 1)
    print("3")
    conn.set('name', 'brown')
    conn.set('gender', 'Male')
    print("4")
    res = conn.execute_command('exec')
    print(res)
    print(conn.mget(["id","name"]))
    conn.incr("cluster")
    conn.hexists()
except Exception as e:
    print("error")
    print(e.message)