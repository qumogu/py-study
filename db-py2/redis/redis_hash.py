# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: redis_study.py
@datetime: 2020/4/13 15:36
"""
from redis import StrictRedis
import redis


import hashlib
import json
import time
import random
mycache = StrictRedis(host="192.168.0.243", port=6379, db=8, password="Nash1234@")

key = "test_hash"
# mycache.delete(key)

# mycache.hmset(key, {"1003":38, "1004":48})


if not mycache.hexists(key, "1005"):
    mycache.hset(key, "1005", 55)

print(mycache.hget(key, "1005"))
mycache.hincrby(key,"1005", 2)
print(mycache.hget(key, "1005"))
print(mycache.hmget(key,["1001","1002"]))
mycache.hdel(key, "1005")
print(mycache.hgetall(key))
print("个数{}".format(mycache.hlen(key)))
print("所有key{}".format(mycache.hkeys(key)))
print("所有值{}".format(mycache.hvals(key)))
mycache.hmset(key, {"1001":1001, "1002":1002})
cursor1, data1 = mycache.hscan(key, cursor=2, match="100*", count=2)
# cursor2, data2 = mycache.hscan(key, cursor=cursor1, match=None, count=None)
print(data1)
