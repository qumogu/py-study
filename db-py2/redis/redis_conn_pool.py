# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: redis_study.py
@datetime: 2020/4/13 15:36
"""
from redis import StrictRedis,ConnectionPool

import string
import random
import time
if __name__ == '__main__':

    mycache = StrictRedis(host="192.168.0.243", port=6379, db=5, password="Nash1234@")

    for i in range(10):
        msg = random.sample(string.ascii_letters+string.digits, 8)
        msg = "".join(msg)
        mycache.publish("bruce", msg)
        print("发送信息：{}".format(msg))
        time.sleep(2)

    mycache.publish("bruce", "exit")

    for i in range(10):
        msg = random.sample(string.ascii_letters+string.digits, 8)
        msg = "".join(msg)
        mycache.publish("bruce", msg)
        print("发送信息：{}".format(msg))
        time.sleep(2)
    






