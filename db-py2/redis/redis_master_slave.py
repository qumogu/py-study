# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: redis_study.py
@datetime: 2020/4/13 15:36
"""
from redis import StrictRedis
from redis.sentinel import Sentinel

import socket

def Redis_Con(Redis_ip, port):
    Redis_sk=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    Redis_Result=Redis_sk.connect_ex((Redis_ip, port))
    if Redis_Result == 0:
        return True
    else:
        return False

#
# sentinel_list = [
# 			("192.168.0.243", "6379"),
# 			("192.168.0.243", "6380"),
# 		]
#
# mySentinel = Sentinel(sentinel_list)
#
# redis_master = mySentinel.master_for('my_master', db=9, password="Nash1234@")
# redis_salve = mySentinel.slave_for('my_master', db=9, password="Nash1234@")
#
# redis_master.set("test2","2222")
# test02 = redis_salve.get("test02")
# print(test02)

# 手动主从分离
#
# mycache1 = StrictRedis(host="192.168.0.243", port=6379, db=9, password="Nash1234@")
#
# mycache2 = StrictRedis(host="192.168.0.243", port=6380, db=9, password="Nash1234@")
#
# print(mycache2.get("test02"))
#
# mycache1.setex("test2",150,"2ww22")
#
# print(mycache2.get("test2"))


# 使用前判断
import time

start_time = time.time()
mycache = StrictRedis(host="192.168.0.243", port=6378, db=9, password="Nash1234@")

try:
    #print(mycache.ping())
    mycache.set("test_connect","test02,connect")
except:
    mycache = StrictRedis(host="192.168.0.243", port=6380, db=9, password="Nash1234@")
print(mycache.get("test02"))

end_time = time.time()

print(end_time-start_time)


# mycache2.setex("test2",150,"2ww22")
#
# print(mycache2.get("test2"))
start_time = time.time()
if Redis_Con("192.168.0.243", 6378):
    mycache = StrictRedis(host="192.168.0.243", port=6378, db=9, password="Nash1234@")
else:
    mycache = StrictRedis(host="192.168.0.243", port=6380, db=9, password="Nash1234@")

end_time = time.time()

print(end_time-start_time)

print(mycache.get("test02"))

