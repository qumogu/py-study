# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: redis_cluster.py
@datetime: 2020/5/11 11:05
"""
import sys
reload(sys)
sys.setdefaultencoding('utf8')


import redis

HOST = "10.0.0.41"
PORT = 6379
NUM = 10000
PASSWORD = "nash1234@"
DATABASE = 1


pool = redis.ConnectionPool(host=HOST, port=PORT, db=DATABASE, password=PASSWORD, max_connections=NUM)
#conn = redis.Redis(host=HOST, port=PORT, db=3, password=PASSWORD)
conn = redis.Redis(connection_pool=pool)


try:
    print("1")
    conn.exec_command('multi')
    print("2")
    conn.set('id', 1)
    print("3")
    conn.set('name', 'brown')
    conn.set('gender', 'Male')
    print("4")
    conn.execute_command('exec')
    print("ok")
    conn.k

    print(conn.mget(["id","name"]))
except Exception as e:
    print("error")
    print(e.message)