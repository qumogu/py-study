# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: redis_study.py
@datetime: 2020/4/13 15:36
"""
from redis import StrictRedis
import time
from redis.sentinel import Sentinel


class CustRedisExection(Exception):
    def __init__(self, err="redis主服务器无法连通"):
        Exception.__init__(self, err)



class TestRedis(object):

    def __init__(self, master_info, slave_info):
        self.master = self.get_master(master_info)

        self.slave = self.get_slave(slave_info)
    def get_slave(self, slave_info):
        redis_conn = StrictRedis(host=slave_info.get("host"),
                                 port=slave_info.get("port"),
                                 db=slave_info.get("db"),
                                 password=slave_info.get("password"))
        try:
            redis_conn.ping()
            return redis_conn
        except:
            return None

    def get_master(self, master_info):
        redis_conn = StrictRedis(host=master_info.get("host", "192.168.0.243"),
                                 port=master_info.get("port", 6379),
                                 db=master_info.get("db", 9),
                                 password=master_info.get("password", "Nash1234@"))
        try:
            redis_conn.ping()
            return redis_conn
        except:
            return None

    def set(self, key, value):
        if self.master:
            self.master.set(key, value)

    def get(self,key):
        if self.slave:
            print("slave")
            return self.slave.get(key)
        elif self.master:
            print("master")
            return self.master.get(key)
        else:
            return None


class SentinelTestRedis(TestRedis):

    def __init__(self,sentinel_list,db=0, password=None):
        self.master, self.slave= self.get_sentinel(sentinel_list,db, password)

    def get_sentinel(self, sentinel_list, db, password):
        mySentinel = Sentinel(sentinel_list)

        redis_master = mySentinel.master_for('mymaster', db=db, password="Nash1234@")
        redis_salve = mySentinel.slave_for('mymaster', db=db, password="Nash1234@")

        return redis_master, redis_master


class CustRedis(object):

    def __init__(self, master_info, slave_info):

        self.master_info = master_info
        self.slave_info = slave_info

    def get_slave(self):

        redis_conn = StrictRedis(host=self.slave_info.get("host"),
                                 port=self.slave_info.get("port"),
                                 db=self.slave_info.get("db"),
                                 password=self.slave_info.get("password"))
        try:
            redis_conn.ping()
            return redis_conn
        except:
            return None

    def get_master(self):

        redis_conn = StrictRedis(host=self.master_info.get("host", "192.168.0.243"),
                                 port=self.master_info.get("port", 6379),
                                 db=self.master_info.get("db", 9),
                                 password=self.master_info.get("password", "Nash1234@"))
        try:
            redis_conn.ping()
            return redis_conn
        except:
            return None

    def set(self, key, value):

        conn = self.get_master()

        if conn:
            conn.set(key, value)
        else:
            raise CustRedisExection

    def get(self, key):
        conn = self.get_slave()
        if conn:
            print("slave")
            return conn.get(key)
        else:
            conn = self.get_master()
            if conn:
                print("master")
                return conn.get(key)
            else:
                raise CustRedisExection

class SentinelRedis(CustRedis):

    def __init__(self,sentinel_list, sentinel_name, db=0, password=None):
        self.sentinel_list = sentinel_list
        self.sentinel_name = sentinel_name
        self.db = db
        self.password = password

    def get_master(self):
        mySentinel = Sentinel(self.sentinel_list)

        redis_conn = mySentinel.master_for(self.sentinel_name,
                                             db=self.db,
                                             password=self.password)
        try:
            redis_conn.ping()
            return redis_conn
        except:
            return None

    def get_slave(self):
        mySentinel = Sentinel(self.sentinel_list)
        # redis_conn = mySentinel.master_for(self.sentinel_name,
        #                                    db=self.db,
        #                                    password=self.password)
        redis_conn = mySentinel.slave_for(self.sentinel_name,
                                           db=self.db,
                                           password=self.password)
        try:
            redis_conn.ping()
            return redis_conn
        except:
            return None



if __name__ == '__main__':
    # mycache1 = StrictRedis(host="192.168.0.243", port=6379, db=9, password="Nash1234@")
    #
    # mycache2 = StrictRedis(host="192.168.0.243", port=6380, db=9, password="Nash1234@")
    #
    # print(mycache2.get("test02"))
    #
    # mycache1.setex("test2", 150, "2ww22")
    #
    # print(mycache2.get("test2"))

    master_info = {"host":"192.168.0.243", "port":6379, "db":9, "password":"Nash1234@"}
    slave_info = {"host": "192.168.0.243", "port": 6380, "db": 9, "password": "Nash1234@"}
    #
    # mycache = TestRedis(master_info, slave_info)
    # mycache.set("test3","333333")
    # print(mycache.get("test3"))

    # sentinel_list = [("192.168.0.243", "26379"), ]
    # sen_redis = SentinelRedis(sentinel_list,"mymaster", 9, "Nash1234@")
    #
    # #sen_redis.set("test6","66666222")
    # print(sen_redis.get("test6"))

    mycache = CustRedis(master_info, slave_info)

    try:
        mycache.set("test7", "7777")
    except CustRedisExection as e:
        print(e.message)

# 使用前判断


# start_time = time.time()
# mycache = StrictRedis(host="192.168.0.243", port=6378, db=9, password="Nash1234@")
#
# try:
#     #print(mycache.ping())
#     mycache.set("test_connect","test02,connect")
# except:
#     mycache = StrictRedis(host="192.168.0.243", port=6380, db=9, password="Nash1234@")
# print(mycache.get("test02"))
#
# end_time = time.time()
#
# print(end_time-start_time)


