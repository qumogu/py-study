# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: redis_study.py
@datetime: 2020/4/13 15:36
"""
from redis import StrictRedis

import hashlib
import json
import time
import random
mycache = StrictRedis(host="192.168.0.243", port=6379, db=8, password="Nash1234@")

key = "test_list"
mycache.delete(key)
# print(mycache.lrange(key, 0, -1))
# print(mycache.llen(key))
pl = mycache.pipeline()
i = 0
while True:

    num = random.randint(1, 99)
    pl.lpush(key, num)

    i += 1
    if i > 4:
        pl.execute()
        print("共有{}个信息，信息详情：{}".format(mycache.llen(key), mycache.lrange(key, 0, -1)))
        time.sleep(6)
        i = 1

# print(mycache.lrange(key, 0, -1))
#
# time.sleep(5)
#
# for i in range(4):
#     print(mycache.lrange(key, 0, -1))
#     time.sleep(3)
#     print(mycache.rpop(key))


