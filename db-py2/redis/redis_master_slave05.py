# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: redis_study.py
@datetime: 2020/4/13 15:36
"""
from redis import StrictRedis
from redis.sentinel import Sentinel

class A(object):

    def get(self):
        print("aaaaa")

    def set(self):
        print("aaaa")

    def get_msg(self,msg):
        print(msg)

class B(object):

    def get(self):
        print("bbbb")

    # def set(self):
    #     print("bbbb")
    #
    # def get_msg(self,msg):
    #     print(msg)

class W(object):
    def __init__(self, master, slave):
        self.master = master
        self.slave = slave

    def __getattr__(self, item):

        if item in ["get","hget"]:
            return getattr(self.slave, item)
        else:
            return getattr(self.master, item)


a = A()
b = B()

w = W(a,b)

w.get()
w.set()
w.get_msg("test02")

