# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: redis_study.py
@datetime: 2020/4/13 15:36
"""
from redis import StrictRedis

import hashlib
import json

mycache = StrictRedis(host="192.168.0.243", port=6379, db=6, password="Nash1234@")

data = [{"name":"jack", "phone":"13811112222", "age":18}, {"name":"张三", "phone":"13733334444", "age":22}, {"name":"李四", "phone":"13955556666", "age":45}]
data2 = [1,2,3]

data2 = json.dumps(data)
print data2

md5_str = hashlib.md5("user-data").hexdigest()

key = "user_cache:{}".format(md5_str)

mycache.set(key, data2)

data3 = mycache.get(key)

print data3

data4 = json.loads(data3)

print data4[0].get("name")


