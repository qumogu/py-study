# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: redis_study.py
@datetime: 2020/4/13 15:36
"""
from redis import StrictRedis,ConnectionPool
import socket
import time
from datetime import datetime

def msg_handle(msg):
    print(msg)
    print("***sleep 5s *****")
    time.sleep(5)

if __name__ == '__main__':

    pool = ConnectionPool(host="192.168.0.243", port=6379, db=15, password="Nash1234@", max_connections=2000)

    mycache = StrictRedis(connection_pool=pool, socket_keepalive=True)
    redis_info = mycache.info()
    print("total_connections_received:%d" % redis_info.get('total_connections_received'))
    print("connected_clients:%d" % redis_info.get('connected_clients'))
    print("total_commands_processed:%d" % redis_info.get('total_commands_processed'))

    print("{}，开始".format(datetime.now()))
    start_time = time.time()
    i_list = [10000, 30000, 50000, 80000]

    temps = 'ksljdfldksjfklsldkfjkdsjflkdsjflkdsklskdjfljewl'
    for i in range(1, 100000):
        mycache.set(temps + str(i), temps + temps + temps)
        if i in i_list:
            end_time = time.time()
            print("{},完成第{}次,耗时{}秒".format(datetime.now(), i, end_time - start_time))
        mycache.get(temps + str(i - 1))
    end_time = time.time()
    print("{}，结束".format(datetime.now()))
    print("一共耗时{}秒".format(end_time - start_time))

    redis_info = mycache.info()
    print("total_connections_received:%d" % redis_info.get('total_connections_received'))
    print("connected_clients:%d" % redis_info.get('connected_clients'))
    print("total_commands_processed:%d" % redis_info.get('total_commands_processed'))