# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: redis_study.py
@datetime: 2020/4/13 15:36
"""
from redis import StrictRedis
import redis


import hashlib
import json
import time
import random
mycache = StrictRedis(host="192.168.0.243", port=6379, db=8, password="Nash1234@")

key = "test_zset"
# mycache.delete(key)

print(mycache.zrange(key, 0, -1))
print(mycache.zscore(key, "1001"))
mycache.zadd(key, "1011",88)
# print(mycache.zrangebyscore(key,min=0, max=30,  start=0, num=-1))

# pl = mycache.pipeline()
# users = {
#     "1009": 100,
#     "1010": -10,
# }
# pl.zadd(key,users)
# # pl.zrem(key,"1002", "1001")
#
# pl.zincrby(key, 2, "1001")
#
# pl.execute()

# print(mycache.zscore(key, "1001"))

print(mycache.zcount(key, '-inf', 50))

print(mycache.zrange(key, 0, -1))
print(mycache.zrank(key, "1001"))

print("zcard元素个数")
print(mycache.zcard(key))

print("zrangbysorce元素个数")
print(mycache.zrangebyscore(key, 50, '+inf'))

for i in mycache.zrangebyscore(key, 50, '+inf'):
    print(mycache.zscore(key, i))

# print(mycache.zpopmax(key, 0))
# i = 0
# while True:
#
#     num = random.randint(1, 99)
#     p
#
#     i += 1
#     if i > 4:
#         pl.execute()
#         print("共有{}个信息，信息详情：{}".format(mycache.llen(key), mycache.lrange(key, 0, -1)))
#         time.sleep(6)
#         i = 1

# print(mycache.lrange(key, 0, -1))
#
# time.sleep(5)
#
# for i in range(4):
#     print(mycache.lrange(key, 0, -1))
#     time.sleep(3)
#     print(mycache.rpop(key))


