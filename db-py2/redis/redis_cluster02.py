# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: redis_cluster.py
@datetime: 2020/5/11 11:05
"""


import redis
from rediscluster import RedisCluster

HOST = "10.0.0.41"
PORT = 6379
NUM = 10000
PASSWORD = "nash1234@"
DATABASE = 1

#startup_nodes = [{"host":HOST, "port":PORT}]

#rc = RedisCluster(startup_nodes=startup_nodes,decode_responses=True, password=PASSWORD)
rc = RedisCluster(host=HOST,port=PORT, password=PASSWORD)

rc.set("cluster","test02")

print(rc.get("cluster"))

nodes = rc.cluster_nodes()
print(type(nodes))
keys = nodes[0].keys
print(keys)
for n in nodes:
    n_str = ""
    for k in keys:
        if k == "slots":
            continue
        n_str = n_str + "{}:{},".format(k, n.get(k))
    print(n_str)