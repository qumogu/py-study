# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: redis_study.py
@datetime: 2020/4/13 15:36
"""
from redis import StrictRedis

import hashlib
import json

mycache = StrictRedis(host="192.168.0.243", port=6379, db=5, password="Nash1234@")

ret_data = {
    "errcode": 0,
    "errmsg": "SUCCESS",
    "data": {
        "pageNo": 1,
        "pageSize": 18,
        "pageTotal": 1,
        "total": 18,
        "pageData": [
            {
                "id": "803714210889596928",
                "parentId": None,
                "regionCode": "533301000000",
                "orgCode": "803714210927345664",
                "orgName": "泸水市教育局",
                "shortName": "泸水市教育局",
                "typeCode": "official",
                "status": 1,
                "orgClassCode": "Agency",
                "orgCNPath": "国家/国家/云南省/怒江傈僳族自治州/泸水市/泸水市教育局",
                "orgType": "04"
            },
            {
                "id": "803714168980111360",
                "parentId": None,
                "regionCode": "533301101000",
                "orgCode": "803714169005277184",
                "orgName": "六库镇水利站",
                "shortName": "六库镇水利站",
                "typeCode": "official",
                "status": 1,
                "orgClassCode": "Agency",
                "orgCNPath": "国家/国家/云南省/怒江傈僳族自治州/泸水市/六库镇/六库镇水利站",
                "orgType": "05"
            },
            {
                "id": "803714504339881984",
                "parentId": None,
                "regionCode": "530326000000",
                "orgCode": "803714504356659200",
                "orgName": "会泽县运营中心",
                "shortName": "会泽县运营中心",
                "typeCode": "official",
                "status": 1,
                "orgClassCode": "Agency",
                "orgCNPath": "国家/国家/云南省/曲靖市/会泽县/会泽县运营中心",
                "orgType": "03"
            },
            {
                "id": "803714378481401856",
                "parentId": None,
                "regionCode": "530300000000",
                "orgCode": "803714378498179072",
                "orgName": "曲靖市指挥中心",
                "shortName": "曲靖市指挥中心",
                "typeCode": "official",
                "status": 1,
                "orgClassCode": "Agency",
                "orgCNPath": "国家/国家/云南省/曲靖市/曲靖市指挥中心",
                "orgType": "02"
            },
            {
                "id": "803714336538361856",
                "parentId": None,
                "regionCode": "530000000000",
                "orgCode": "803714336571916288",
                "orgName": "省指挥中心",
                "shortName": "省指挥中心",
                "typeCode": "official",
                "status": 1,
                "orgClassCode": "Agency",
                "orgCNPath": "国家/国家/云南省/省指挥中心",
                "orgType": "01"
            },
            {
                "id": "803714336542556160",
                "parentId": None,
                "regionCode": "533300000000",
                "orgCode": "803714336576110592",
                "orgName": "怒江傈僳族自治州指挥中心",
                "shortName": "怒江傈僳族自治州指挥中心",
                "typeCode": "official",
                "status": 1,
                "orgClassCode": "Agency",
                "orgCNPath": "国家/国家/云南省/怒江傈僳族自治州/怒江傈僳族自治州指挥中心",
                "orgType": "02"
            },
            {
                "id": "803717524045824000",
                "parentId": None,
                "regionCode": "803716098301231104",
                "orgCode": "803717524050018304",
                "orgName": "野马村村委会",
                "shortName": "野马村村委会",
                "typeCode": "official",
                "status": 1,
                "orgClassCode": "Agency",
                "orgCNPath": "国家/国家/云南省/曲靖市/会泽县/待补镇/野马村/野马村村委会",
                "orgType": None
            },
            {
                "id": "803714462526865408",
                "parentId": None,
                "regionCode": "530326213000",
                "orgCode": "803714462543642624",
                "orgName": "田坝乡机构",
                "shortName": "田坝乡机构",
                "typeCode": "official",
                "status": 1,
                "orgClassCode": "Agency",
                "orgCNPath": "国家/国家/云南省/曲靖市/会泽县/田坝乡/田坝乡机构",
                "orgType": "05"
            },
            {
                "id": "803714588506980352",
                "parentId": None,
                "regionCode": "530326108000",
                "orgCode": "803714588523757568",
                "orgName": "待补镇政府",
                "shortName": "待补镇政府",
                "typeCode": "official",
                "status": 1,
                "orgClassCode": "Agency",
                "orgCNPath": "国家/国家/云南省/曲靖市/会泽县/待补镇/待补镇政府",
                "orgType": "05"
            },
            {
                "id": "740258769950236672",
                "parentId": None,
                "regionCode": "100000000000",
                "orgCode": "1",
                "orgName": "省发展和改革委员会",
                "shortName": "实施机构",
                "typeCode": "official",
                "status": 1,
                "orgClassCode": "MofDep",
                "orgCNPath": "国家/省发展和改革委员会",
                "orgType": None
            },
            {
                "id": "803714630118670336",
                "parentId": None,
                "regionCode": "530326000000",
                "orgCode": "803714630135447552",
                "orgName": "会泽县民政局",
                "shortName": "会泽县民政局",
                "typeCode": "official",
                "status": 1,
                "orgClassCode": "Agency",
                "orgCNPath": "国家/国家/云南省/曲靖市/会泽县/会泽县民政局",
                "orgType": "04"
            },
            {
                "id": "803714210893791232",
                "parentId": None,
                "regionCode": "533301101000",
                "orgCode": "803714210931539968",
                "orgName": "六库镇镇政府",
                "shortName": "六库镇镇政府",
                "typeCode": "official",
                "status": 1,
                "orgClassCode": "Agency",
                "orgCNPath": "国家/国家/云南省/怒江傈僳族自治州/泸水市/六库镇/六库镇镇政府",
                "orgType": "05"
            },
            {
                "id": "803714713958612992",
                "parentId": None,
                "regionCode": "530000000000",
                "orgCode": "803714713971195904",
                "orgName": "省民政厅",
                "shortName": "省民政厅",
                "typeCode": "official",
                "status": 1,
                "orgClassCode": "Agency",
                "orgCNPath": "国家/国家/云南省/省民政厅",
                "orgType": None
            },
            {
                "id": "803714252723585024",
                "parentId": None,
                "regionCode": "533301000000",
                "orgCode": "803714252757139456",
                "orgName": "泸水市民政局",
                "shortName": "泸水市民政局",
                "typeCode": "official",
                "status": 1,
                "orgClassCode": "Agency",
                "orgCNPath": "国家/国家/云南省/怒江傈僳族自治州/泸水市/泸水市民政局",
                "orgType": "04"
            },
            {
                "id": "803714923476680704",
                "parentId": None,
                "regionCode": "100000000000",
                "orgCode": "803714923497652224",
                "orgName": "开发商",
                "shortName": "开发商",
                "typeCode": "official",
                "status": 1,
                "orgClassCode": "Agency",
                "orgCNPath": "国家/开发商",
                "orgType": None
            },
            {
                "id": "803714672007184384",
                "parentId": None,
                "regionCode": "530300000000",
                "orgCode": "803714672023961600",
                "orgName": "曲靖市民政局",
                "shortName": "曲靖市民政局",
                "typeCode": "official",
                "status": 1,
                "orgClassCode": "Agency",
                "orgCNPath": "国家/国家/云南省/曲靖市/曲靖市民政局",
                "orgType": None
            },
            {
                "id": "803714252727779328",
                "parentId": None,
                "regionCode": "533301000000",
                "orgCode": "803714252761333760",
                "orgName": "泸水市水利局",
                "shortName": "泸水市水利局",
                "typeCode": "official",
                "status": 1,
                "orgClassCode": "Agency",
                "orgCNPath": "国家/国家/云南省/怒江傈僳族自治州/泸水市/泸水市水利局",
                "orgType": "04"
            },
            {
                "id": "803714294603710464",
                "parentId": None,
                "regionCode": "533301000000",
                "orgCode": "803714294620487680",
                "orgName": "泸水市运营中心",
                "shortName": "泸水市运营中心",
                "typeCode": "official",
                "status": 1,
                "orgClassCode": "Agency",
                "orgCNPath": "国家/国家/云南省/怒江傈僳族自治州/泸水市/泸水市运营中心",
                "orgType": "03"
            }
        ]
    }
}

orgs = ret_data.get("data").get("pageData")

for org in orgs:
    org_type = org.get()

