# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: redis_study.py
@datetime: 2020/4/13 15:36
"""
from redis import StrictRedis

import time
from datetime import datetime
if __name__ == '__main__':

    mycache = StrictRedis(host="192.168.0.243", port=6379, db=5, password="Nash1234@")

    pubsub = mycache.pubsub()

    pubsub.subscribe("test02")
    pubsub.psubscribe(['channel1', "channel2"])
    print("启动")
    while True:
        msg = pubsub.get_message()
        if msg:
            print(msg)
            if msg.get("data") == "exit":
                print("{} 退出".format(datetime.now().strftime("%Y-%m-%d %H:%M:%S")))
                is_over = True
                break
        else:
            print("***sleep 5s *****")
            time.sleep(5)


