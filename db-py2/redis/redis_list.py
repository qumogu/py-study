# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: redis_study.py
@datetime: 2020/4/13 15:36
"""
from redis import StrictRedis

import hashlib
import json
import time
mycache = StrictRedis(host="192.168.0.243", port=6379, db=8, password="Nash1234@")

key = "test_list"
key2 = "test_list2"

data = [1,2]
mycache.lpush(key, *data)

mycache.rpush(key2, *[3,5,7,8])

# mycache.rpushx(key, 100)
mycache.lre
print(mycache.lrange(key,0,-1))
print(mycache.lrange(key2,0,-1))
# print("原有多少个对象")
# mycache.llen(key)

# print("在2面前加5")
# mycache.linsert(key,'before', 2, 5)

# print("在3面前加7")
# mycache.linsert(key,'after', 3, 7)

# print("对index位置重新设置为value")
# mycache.lset(key,2, 8)

# print("从后到前（逆序），删除1个2")
# mycache.lrem(key, -1, 2)

# print("从左踢出第一个值，返回它的值")
# print(mycache.lpop(key))

# print("从左到右，删除索引1-5外所有值")
# mycache.ltrim(key, 1, 5)

# print("去索引2的值")
# print(mycache.lindex(key, 2))

# print("一次移除多个列表")
# while True:
#     mycache.blpop([key2, key], timeout=2)
#     print(mycache.lrange(key, 0, -1))
#     print(mycache.lrange(key2, 0, -1))

# print("移动 元素从一个列表移动到另外一个列表")
# mycache.rpoplpush(key, key2)
# while True:
#     print("移动 元素从一个列表移动到另外一个列表")
#     mycache.brpoplpush(key,key2)
#     print(mycache.lrange(key, 0, -1))
#     print(mycache.lrange(key2, 0, -1))
#     if mycache.llen(key) == 0:
#         break

print(mycache.lrange(key, 0, -1))
print(mycache.lrange(key2, 0, -1))
