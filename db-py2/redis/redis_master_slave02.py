# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: redis_study.py
@datetime: 2020/4/13 15:36
"""
from redis import StrictRedis
from redis.sentinel import Sentinel



sentinel_list = [
			("192.168.0.243", "26379"),
		]

mySentinel = Sentinel(sentinel_list)

redis_master = mySentinel.master_for('mymaster', db=9, password="Nash1234@")
redis_salve = mySentinel.slave_for('mymaster', db=9, password="Nash1234@")

redis_master.set("test3","333")
# test02 = redis_salve.get("test02")
print(redis_master.get("test3"))
print(redis_salve.get("test02"))

# 手动主从分离
#
# mycache1 = StrictRedis(host="192.168.0.243", port=6379, db=9, password="Nash1234@")
#
# mycache2 = StrictRedis(host="192.168.0.243", port=6380, db=9, password="Nash1234@")
#
# print(mycache2.get("test02"))
#
# mycache1.setex("test2",150,"2ww22")
#
# print(mycache2.get("test2"))







