# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: redis_study.py
@datetime: 2020/4/13 15:36
"""
from redis import StrictRedis

import string
import random
import time
if __name__ == '__main__':

    mycache = StrictRedis(host="192.168.0.243", port=6379, db=5, password="Nash1234@")
    channels = ["test02", "channel1", "channel2"]

    j = 1
    for i in range(10):
        msg = random.sample(string.ascii_letters+string.digits, 8)
        msg = "".join(msg)
        mycache.publish(channels[random.randint(0,2)], "the {} msg:{}".format(j, msg))
        print("发送第{}个信息：{}".format(j, msg))
        time.sleep(1)
        j += 1

    # mycache.publish("test02", "exit")

    for i in range(10):
        msg = random.sample(string.ascii_letters+string.digits, 4)
        msg = "".join(msg)
        mycache.publish(channels[random.randint(0,2)], "the {} msg:{}".format(j, msg))
        print("发送第{}个信息：{}".format(j, msg))
        time.sleep(2)
        j += 1

    mycache.publish("test02", "exit")







