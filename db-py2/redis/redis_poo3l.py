# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: redis_study.py
@datetime: 2020/4/13 15:36
"""
from redis import StrictRedis,ConnectionPool
import socket
import time
from datetime import datetime

def msg_handle(msg):
    print(msg)
    print("***sleep 5s *****")
    time.sleep(5)

if __name__ == '__main__':

    pool = ConnectionPool(host="192.168.0.243", port=6379, db=15, password="Nash1234@", max_connections=100)

    mycache = StrictRedis(connection_pool=pool,socket_keepalive=True)

    mycache.flushdb()
    redis_info = mycache.info()
    print("total_connections_received:%d" % redis_info.get('total_connections_received'))
    print("connected_clients:%d" % redis_info.get('connected_clients'))
    print("total_commands_processed:%d" % redis_info.get('total_commands_processed'))
