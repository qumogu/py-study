# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: redis_study.py
@datetime: 2020/4/13 15:36
"""
from redis import StrictRedis

import hashlib
import json
import time
mycache = StrictRedis(host="192.168.0.243", port=6379, db=8, password="Nash1234@")

key = "test_list"

i = 0
while True:
    print(mycache.lrange(key, 0, -1))

    data = mycache.rpop(key)
    print(data)
    if not data:
        if i > 10:
            print("程序结束")
            break

        i += 1
        print("第{}次休息5秒钟".format(i))
        time.sleep(5)

    time.sleep(2)
