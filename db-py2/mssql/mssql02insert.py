# -*- coding: utf-8 -*- 
"""
@author: LanMin
@contact: lanmin@itssoft.net
@file: mssql02insert.py
@datetime: 2020/8/24 16:55
"""
from __future__ import unicode_literals
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

import pymssql
from pymssql import connect


host = "139.196.109.22"
port = 1433
username = "sa"
password = "Banber!@34"
db = "test02"

conn = connect(server=host, port=port, user=username, password=password,database=db,charset="utf8", as_dict=True) #, autocommit=True

cursor = conn.cursor()
sql = "insert into student(id,username,age,gender) values (17,'test11',11,1);"
#sql = "update student set username='test10abc' where id=16"
try:
    rs = cursor.execute(sql)
    print(cursor.rowcount)
    conn.commit()
    rs = cursor.fetchone()
    print(rs)
    print(cursor.lastrowid)
except Exception as e:
    print(e)
    conn.rollback()

# sql = "select id from student where username='test11'"
#
# cursor.execute(sql)
# rs = cursor.fetchone()
# print(rs)
# sql = "select ident_current('student')"
# rs = cursor.execute(sql)
# print(rs)


#rs = conn.commit()
# print(rs)
# print(
# cols = cursor.description
# for i in rs:
#     print(i)
#
# print(cols)

conn.close()
