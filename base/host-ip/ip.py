import socket
import uuid
import psutil

def ip():
    host = socket.gethostname()
    print(host)
    name = socket.getfqdn(host)
    print(name)
    myaddr = socket.gethostbyname(name)
    print(myaddr)

    for ip in socket.gethostbyname_ex(host)[2]:
        print(ip)


def GetMAC():
    r""" 针对单网卡"""
    # for item in uuid.getnode()
    #     print(item)
    print(uuid.getnode())
    addr = hex(uuid.getnode())[2:].upper()
    print("----"+addr)
    mac = '-'.join(addr[i:i+2] for i in range(0, len(addr),2))
    print(mac)

def ip2():
    """
    查询本机ip
    :return: ip string
    """
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(('8.8.8.8', 80))
        ip = s.getsockname()[0]
        for i in s.getsockname():
            print(i)
    finally:
        s.close()
        print(ip)
        return ip

def printNetInfo():
    """
    打印多网卡mac和ip
    :return:
    """
    dic =psutil.net_if_addrs()
    for adapter in dic:
        snicList = dic[adapter]
        mac = '无MAC地址'
        ip4 = '无ipv4地址'
        ip6 = '无ipv6地址'
        for snic in snicList:
            if snic.family.name in {'AF_LINK', 'AF_PACKET'}:
                mac = snic.address
            elif snic.family.name == 'AF_INET':
                ipv4 = snic.address
            elif snic.family.name == 'AF_INET6':
                ipv6 = snic.address

        print('%s,%s,%s,%s' % (adapter, mac, ipv4, ipv6))


if __name__ == '__main__':
    ip()
    print("---"*10)
    GetMAC()
    print("---" * 10)
    printNetInfo()

