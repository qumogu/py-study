import json
import random
import time

import psutil
from paho.mqtt import client as mqtt_client

def to_M(n):
    '''将B转为M'''
    u = 1024*1024
    m = round(n/u, 2)
    return m



def get_info():
    cpu_percent = psutil.cpu_percent(interval=1)
    cpu_count = psutil.cpu_count()
    sys_loadavg = [round(x / cpu_count*100, 2) for x in psutil.getloadavg()]
    mem = psutil.virtual_memory()
    mem_total, mem_free = to_M(mem.total), to_M(mem.free)
    mem_percent = mem.percent
    host_info = {
        'cpu_percent': cpu_percent,
        'cpu_count' : cpu_count,
        'sys_loadavg': sys_loadavg,
        'mem_total': mem_total,
        'mem_percent': mem_percent,
        'men_free': mem_free
    }
    return json.dumps(host_info)

def on_connect(_client, _userdata, flags, rc):
    '''连接回调函数'''
    if rc == 0:
        print("connected to MQTT ok!")
    else:
        print("failed to connect, return cod %d \n", rc)


def create_client(ip,port, client_base):
    num = random.randint(0,1000)
    client_id = f'{client_base}-{num}'
    client = mqtt_client.Client(client_id)
    client.on_connect = on_connect
    client.connect(ip, port, 60)
    return client

def publish(client,topic):
    while True:
        time.sleep(5)
        msg = get_info()
        result = client.publish(topic, msg)
        status = result[0]
        if status == 0:
            print(f"send `{msg}` to topic `{topic}`")
        else:
            print(f"failed to send message to topic `{topic}`")

def publish():
    msg = get_info()
    topic = "python/study/pub"
    result = client.publish(topic, msg)
    status = result[0]
    if status == 0:
        print(f"send `{msg}` to topic `{topic}`")
    else:
        print(f"failed to send message to topic `{topic}`")

def on_message(_client,_userdata, msg):
    print(f"Received `{msg.payload.decode()}` from `{msg.topic}` topic")

def hander_test(_client,_userdata, msg):
    data = json.loads(str(msg.payload.decode('utf-8')))
    name = data.get("name")
    if name == None:
        print("no data")
        return
    if name == "may":
        print(f"`{name}` is master, age is `{data['age']}`,topic:`{msg.topic}`")
        publish()
    else:
        print(f"`{name}` is normal, age is `{data['age']}`,topic:`{msg.topic}`")



def subscribe(topic, callback):
    client.subscribe(topic)
    client.message_callback_add(topic, callback)

def run():
    subscribe("python/study/#", hander_test)
    client.loop_forever()


if __name__ == '__main__':
    client = create_client("172.29.100.41", 1883, "py-study")
    # run()
    publish()
    run()