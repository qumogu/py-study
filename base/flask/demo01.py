from flask import Flask


app = Flask(__name__)

@app.route('/')
def hello():
    return 'Hello flask 2.2.2'

@app.route('/user')
def user():
    return 'user info'

if __name__ == '__main__':
    app.run("0.0.0.0", 7788, debug=True)
